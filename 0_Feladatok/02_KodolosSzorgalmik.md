# Beadható kódolásos feladatok

* Elvárások:
	* Bármilyen programozási nyelvet, keretrendszert, stb. lehet használni
	* A nyelvben, vagy valamely libraryjéban levő típus- és algoritmusmegvalósításokat természetesen nem szabad használni
	* Dokumentáció csak abban az esetben kell, ha nem triviális a környezet felépítése, a program futtatása, vagy bármilyen a programmal kapcsolatban felmerülő kérdés magyarázatra vár
	* Nem elvárás, de üdvözlendő (és gyorsítja a javítást) a tesztek jelenléte (unit tesztek, tesztfájlok, szimulációs környezet, vagy akár csak egy lista, hogy milyen inputokkal próbáltad végig)
	* A kódolt típusok, algoritmusok minél inkább hasonlítsanak az órán vett változataikhoz
	* Emellett tartsuk be a választott nyelv konvencióit, tehát lehetőleg ne használjunk egy betűs változóneveket, "E2C" jellegű osztályneveket még akkor se, ha papíron ez volt a stílus
	* Természetesen tömböket 0-tól indexelünk
	* Az üres else-ágakat hagyjuk el, ha a nyelv szemantikája erre lehetőséget nyújt
	* A hatékonyabb algoritmus jobb algoritmus
	* A maximális elérhető pont: 5 pont
	* A pontozás szempontjai (sorrendben) a késés, a helyesség és teljesség, az órai megközelítéshez való értelemszerű hűség, hatékonyság, általánosság, olvashatóság
	* A beadott megoldásokat értékelem, és ha nem max. pontos, leírom a problémáimat, és visszaküldöm javításra. Hibátlanra időben való javítás esetén jár az 5 pont
	* Mindkét témakörből az egyik feladatot lehet kiválasztani, nem kell előre szólni, hogy melyiket
	* Beadási határidő: ameddig jegyet szeretnél kapni -1-2 nap

## Első adag

* 1.
	* Készítsük el statikus tömbre a buborékrendezés, összefésülő rendezés, beszúró rendezés és maximumkiválasztó rendezés algoritmusait úgy, hogy mindegyik számlálja az összehasonlítások és cserék számát
	* Versenyeztessük őket különféle inputokon
	* Működjön egészekre, törtekre, boolokra (false < true), karakterekre, stringekre (lexikografikusan)
* 2.
	* Készítsük el az "E1" típust a szokásos műveletekkel
		* next és key lekérése
		* next beállítása
		* Írjunk konstruktort is, a kulcsot csak ezzel adhassuk meg
	* A key típusa legyen int
	* Készítsük el a lista interfészét, valamint a statikus tömbös és láncolt megvalósítását is
	* Ne feledkezzünk el a destruktorról, ami a láncolt esetben bontsa le a listát
	* Írjuk meg a keresés, törlés, beszúrás műveleteit kétféleképpen is: legyen egy minél hatékonyabb rendezetlenséget feltételező és egy rendezettséget feltételező változata
	* Írjunk egy bool paraméterű változatot is a keresés, törlés, beszúrás metódusokhoz. A paraméter döntse el, hogy a rendezett vagy a rendezetlen fajtát hívjuk meg
	* Tartsuk számon a listáról, hogy rendezett-e, készítsünk egy paraméter nélküli változatot a fentiekből, ami a rendezettségtől függően hívja egyik vagy másik verziót
	* Írjuk meg az egyik rendezőalgoritmust is (ezt is tömbös és láncolt implementációra is)
	* Az összes megírt metódus a lista interfészt használja, tehát legyen független az implementációtól
	* +1 pontért: Készítsünk generikus implementációt. Ne "key" legyen benne, hanem egy pointer valamilyen "összehasonlítható" elemre, ahol persze a tartalmazott elem típusáról mást nem feltételezhetünk. Az összes művelet működjön, törléskor ténylegesen töröljük a tartalmazott elemet!
* 3.
	* Készítsünk H2CL interfészt, valamint tömbös és láncolt megvalósítást. Utóbbihoz használjuk az E2C típust
	* Implementáljuk a rendezett beszúrás algoritmusát
	* Írjuk meg a lista megfordítása metódust
	* Írjunk egy metódust, ami megmondja, hogy a lista rendezett-e, ha igen, növekvően vagy csökkenően (esetleg mindkettő)
	* Írjuk meg az unió, metszet, különbség metódusokat, amelyek mind növekvően, mind csökkenően rendezett listákra működnek - ha mindkét paraméterlista ugyanúgy vagy rendezve - és maguk is egy hasonlóképp rendezett listát állítanak elő. A rendezettség iránya megadható paraméterként, azt nem kell ellenőrizni. Ha több azonos értékű elem van, az unió a nagyobb előfordulást, a metszet a kisebb előfordulást, a különbség az előfordulások számának különbségét vegye. A három művelet megvalósítása során törekedjünk a kódújrafelhasználásra (lásd: származtatás, iterátor, függvénypointer)
* 4.
	* Készítsük el a Verem (Stack) interfészt, valamint a láncolt és statikus tömbös megvalósításait
	* Készítsük el a helyes zárójelezést felismerő és a zárójelpárok indexeit kiíró függvényt
		* A támogatott zárójelek listája és precedenciáik legyenek kívülről konfigurálhatóak
	* Készítsük el a névsorfordító algoritmust 2 veremmel, abban a verzióban, ami a házi feladat volt
		* Készítsünk olyan tömbös ábrázolást is, ahol 2 verem is ugyanazt a tömböt használja elölről és hátulról. Használjuk ehhez a feladathoz
* 5.
	* Készítsük el a Verem (Stack) interfészt, valamint a láncolt és statikus tömbös megvalósításait
	* Implementáljuk az infix módon megadott matematikai kifejezések lengyelformára hozó valamint a lengyelformát kiértékelő algoritmusát
		* Egyféle zárójelet használhassunk csak, de a használható műveletek listája, precedenciával, aritással, asszociativitással lehessen kívülről konfigurálható. Mindenképp legyen köztük az értékadás (aminek a bal operandusa csak balérték - azaz változó - lehet), és az egyenlőségvizsgálat is
		* Aritásból kétfélét: 1 és 2 paramétereset támogassunk (az egy paraméteres pl. az unáris mínusz, vagyis a mínusz előjel)
		* Literálok mellett változóneveket is fogadjon el a függvény, amik (kezdeti) értékei szintén kívülről jöjjenek
		* 5 pontért elég ha megmaradunk az intek körében (az se baj, ha az osztás szabálytalanul kerekít), +1 pontért készítsük el generikusan. Bármilyen numerikus típust adhassunk meg neki, készítsünk sajátot is, saját műveleti jelekkel, saját literálokkal, saját implementációkkal... (példa: a literálok betűk, az összeadás meg mondjuk adja vissza az ASCII kódok (modulo vett) összege által jelölt karaktert), használhatunk reflectiont

## Második adag

* 1.
	* Definiáld a Queue (Sor) interfészt (vagy tisztán virtuális függvényeket tartalmazó absztrakt osztályt à la C++, stb.) a 06_Sor/01_Interfesz.md fájlban foglalt műveletekkel + a length() metódussal
	* Készíts három féle implementációt hozzá: tömbös, végelemes láncolt, végelem nélküli+hossz attribútumos láncolt
	* Írd meg az unió és a metszet algoritmusokat, mely két rendezett sort vár és egy rendezett sort ad vissza
* 2.
	* Készítsünk bináris fa generikus adattípust aritmetikai (tömbös) ábrázolással
	* Írjuk meg rá a preorder, inorder, postorder, szintfolytonos bejárásokat
	* Írjuk meg a beszúrás, keresés, törlés műveleteket is
	* Származtasd le a Kifejezésfa típust, amivel egy infix módon megadott matematikai kifejezésből lengyelformát tudsz készíteni
* 3.
	* Valósítsd meg a Heap (Kupac) típust aritmetikai ábrázolással
	* Írj tetszőleges tömböt kupaccá alakító algoritmust, írd meg a beszúrást, a törlést, a keresést, a removeMax()-ot és a kupacrendezést
	* Ehhez a plusz anyagokban találsz infót
* 4.
	* Add meg a PrQueue (prioritásos sor) interfészt a pluszanyagokban megadott műveletekkel
	* Készíts rendezetlen tömbös, rendezett tömbös és kupacos implementációt
	* Írj egy algoritmust, ami egy adott elem prioritásának változása esetén aktualizálja a fát
* 5.
    * Készíts programot, amivel tetszőleges, rendezett kételemű halmaz fölötti gyűjteményekre le tudod futtatni a bináris radix előre és vissza algoritmusokat
    * A megoldás legyen generikus
    * Az input legyen paraméterként (akár fájlból) beolvasható
    * Ehhez a plusz anyagokban találsz infót