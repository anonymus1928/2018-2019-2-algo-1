# Algoritmusok és adatszerkezetek 1., második zh konzultáció

* Helye: D 1-820
* Ideje: 2019. 05. 13., 17.30-19.20

## 1. feladat - bináris fa rekonstruálása bejárásból

* Adott egy bináris fa inorder és postorder bejárásai. Rekonstruáljuk a fát!
	* inorder: B, I, E, D, G, F, A, C, H, J
	* postorder: E, I, G, D, F, B, H, J, C, A
* Megoldás:
	* Tudjuk, hogy a postorder utolsó eleme a gyökér. Azaz az A-t leírhatjuk, mint gyökér
	* Most a C-ről annyit tudunk, hogy biztosan A egyik gyereke, az inorderből tudjuk, hogy jobb gyereke, mert ott A után van (A jobb részfájában van)
	* Hasonlóan, menjünk végig a postorder listán visszafelé, és mindig a gyökértől kiindulva a már meglevő elemek inorderbeli elhelyezkedése szerint válasszuk a bal vagy jobb gyereket. Ha az nincs, pont oda kell beszúrni az adott node-ot
	* Preorder és inorder sorrend esetén ugyanez a megoldás, csak a preorderen kell előrefelé végigmenni
	* Preorder és postorder bejárásokból pedig nem lehet megcsinálni a feladatot
	* Mindenképp ellenőrizzük a bejárások alapján, hogy jó-e az elkészült fa

	```
	    A
	  /   \
	  B    C
	   \    \
	   F    J
	   /    /
	  D    H
	 / \
	I   G
	 \
	  E
	```

## 2. feladat - Levélszám

* Hány levele van egy bináris fának?
	* Rekurzívan
	* ADT-szinten (absztrakt fára, reprezentációfüggetlenül)
* Megoldás:
	```
	levélszám(t : BinTree) : N
	  HA t == ÜRES
	    return 0
	  KÜLÖNBEN
	    HA t.left() == t.right() == ÜRES // azaz levél
	      return 1
	    KÜLÖNBEN
	      return levélszám(t.left()) + levélszám(t.right()) // itt a 0-s eset is meghívhódhat, ha csak egy gyereke van
	```

## 3. feladat - Levélszám korláttal

* Hány levele van egy bináris fának a k. szintig?
	* Rekurzívan
	* ADT-szinten (absztrakt fára, reprezentációfüggetlenül)
* Megoldás:
	* Hasonló az előzőhöz, de a rekurzív függvényt most ki kell egészítenünk még két paraméterrel: hányadik szintig megyünk és most hányadikon vagyunk
	* Az utóbbi paraméter viszont nem kell hogy kívülről látsszon, ezért bevezetünk egy segédfüggvényt, ami azért felel, hogy a rekurzió beinduljon
	* Egyenértékű megoldást kapunk, ha csak a k-t "visszük" tovább, ekkor nem is kell a külön inicializáló függvény. Ekkor k értékét minden körben csökkentjük (ami nem baj, mivel nem referencia szerint adtuk át), és azt ellenőrizzük, negatív lett-e

	```
	levélszám(t : BinTree, k : N) : N
	  return levélszám(t, k, 0)
	```  

	```
	levélszám(t : Bintree, k : N, szint : N) : N
	  HA szint > k vagy t == ÜRES
	    return 0
	  KÜLÖNBEN
	    HA t.left() == t.right() == ÜRES
	      return 1
		KÜLÖNBEN
	      return levélszám(t.left(), k, szint+1) + levélszám(t.right(), k, szint+1)	
	```

## 4. feladat - Maximumkiválasztás

* Rekurzívan
* ADT-szinten
* Előfeltétel: t != ÜRES
* Megoldás:
	* A fő hívásnál nem kell foglalkoznunk az üres fa esetével, de a rekurzív hívásoknál lehetne üres részfa, ezért ezeket még azelőtt "megfogjuk", hogy a rekurzió odaérne, azért ilyen a kód
	* Szebbé tehetnénk, ha úgy tekintenénk, hogy az üres részfa maximuma valami extremális érték (mínusz végtelen), és ekkor gyakorlatilag a kilépő és a rekurzív eset maradna csak az algoritmusban

	```
	maxKulcs(t : BinTree) : T
	  HA t.left() == ÜRES és t.right() == ÜRES
	    return t.key()
	  KÜLÖNBEN
	    HA t.left() != ÜRES és t.right() == ÜRES
	      return max {t.key(), maxKulcs(t.left())}
	    HA t.left() == ÜRES és t.right() != ÜRES
	      return max {t.key(), maxKulcs(t.right())}
	    HA t.left() != ÜRES és t.right() != ÜRES
	      return max {t.key(), maxKulcs(t.left()), maxKulcs(t.right())}
	```

## 5. feladat - Szintfolytonos bejárás

* Írjuk meg a szintfolytonos (vagy sorfolytonos) bejárást ADT-szinten ábrázolt fára
* Iteratívan
* Egy sor segítségével
* Megoldás:
	* Ha a fa nem üres, legalább egy csúcsa van
	* Ezt berakjuk egy sorba, majd addig haladunk, míg a sor ki nem ürül
	* Mindig kivesszük a sorból a legrégebb óta várakozó elemet, feldolgozzuk, és berakjuk a gyerekeit
	* Most, ha a gyökér bal részfáját dolgozzuk épp fel, a bal részfa gyerekei bekerülnek a sorba, de csak a gyökér feldolgozásakor bekerülő gyökér jobb részfája után kerülnek ki, ezzel valósul meg a szintfolytonos feldolgozás

	```
	szintfolytonosBejárás(t : BinTree)
	  HA t == ÜRES
	    SKIP
	  KÜLÖNBEN
	    s = Queue()
	    s.add(t)
	    AMÍG !s.isEmpty()
	      t = s.rem()
	      feldolgoz(t.key())
	      HA t.left() != ÜRES
	        s.add(t.left())
	      HA t.right() != ÜRES
	        s.add(t.right())
	```

## 6. feladat - Legkorábbi levél
* Határozzuk meg a legalacsonyabb (gyökérhez legközelebbi) szintet, ahol van levél
* Megoldás:
	* A szintfolytonos bejárás ötletéhez nyúlhatunk, hiszen a feladat a szintfolytonosan legkorábban megtalált levél szintjének meghatározása
	* A sorba most a csúcsok mellett a csúcs szintjét is berakjuk, hiszen ez direktben nem lekérdezhető
	* A ciklusfeltétel (!van) azért lehet ez, mert ha a van hamis, biztosan nem levél a legutóbb feldolgozott csúcs, és emiatt biztosan van gyereke. De ez a gyerek bekerült a sorba, így a következő ciklusmag-futtatáskor kivehetjük azt biztonsággal
	* A kód egyszerűsíthető lehet a "van" elhagyásával és a return parancs a ciklusmag megfelelő helyére illesztésével - azért írtuk meg mégis így, mert több programozási nyelven is elvárt, hogy minden fordítási időben kikövetkeztethető futási ágon legyen return utasítás. Mi tudjuk, hogy van levél, ezért a ciklusból nem jutnánk ki, de a fordító nem

	```
	legalacsonyabbLevél(t : BinTree) : Z
	  HA t == ÜRES
	    return -1
	  KÜLÖNBEN
	    van = hamis
	    s = Queue()
	    s.add((t,0))
	    AMÍG !van
	      (t,k) = s.rem()
	      van = t.left() == ÜRES és t.right() = ÜRES
	      HA !van
	        HA t.left() != ÜRES
	          s.add(t.left(), k+1)
	        HA t.right() != ÜRES
	          s.add(t.right(), k+1)
	    return k
	```

## 7. feladat - Preorder bejárás

* Írjuk meg a preorder bejárás algoritmusát
* Iteratívan
* Veremmel
* Megoldás:
	* Csak a sort kell veremre cserélni a szintfolytonosban, mivel itt épp az a lényeg, hogy ha egy úton elindultunk, az ő gyerekei élvezzék az elsőbbséget és a "jobb gyerekek" csak ezek után jöhessenek. Ezt támogatja a verem LIFO volta
	* Még egy módosítás: előbb a jobb, és utána a bal gyereket kell bepakolni, hiszen ekkor fog a bal hamarább kikerülni (lásd szövegmegfordítós feladatok)

	```
	preorderVeremmel(t : BinTree)
	  HA t == ÜRES
	    SKIP
	  KÜLÖNBEN
	    v = Stack()
	    v.push(t)
	    AMÍG !v.isEmpty()
	      t = v.pop()
	      feldolgoz(t.key())
	      HA t.right() != ÜRES
	        v.push(t.right())
	      HA t.left() != ÜRES
	        v.push(t.left())
	```

## 8. feladat - Inorder bejárás

* Írjuk meg az inorder bejárás algoritmusát
* Iteratívan (ciklussal)
* Veremmel
* Megoldás:
	* Mint tudjuk, a legbaloldalibb elemmel kell kezdenünk
	* Ezért a stratégia az lesz, hogy amíg van baloldali gyerek, berakjuk a verembe a látogatott csúcsot, és továbblépünk a bal gyerekre, hogy majd annak feldolgozása után üríthessük a vermet és dolgozhassuk fel az adott elemet, majd a jobb gyerekét
	* Az elágazás bal ága jelenti majd a balra menést és a verem gyűjtögetését, a jobb ága, pedig az elem feldolgozását és az eltérést a jobb gyereke felé, ahol amennyiben annak van bal részfája, megint csak a bal ágat fogjuk többször is meghívni
	* Addig megyünk, amíg van "jelenlegi" elem, illetve amíg vannak félretett elemek a veremben - üres fa esetén egyik sincs, tehát be se lépünk a ciklusba

	```
	inorderVeremmel(t : BinTree)
	  v = Stack()
	  AMÍG !v.isEmpty() vagy t != ÜRES
	    HA t != ÜRES
	      v.push(t)
	      t = t.left()
	    KÜLÖNBEN
	      t = v.pop()
	      feldolgoz(t.key())
	      t = t.right()
	```
