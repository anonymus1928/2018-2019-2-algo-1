# Info

* Minden "határidő"-t értsünk úgy, hogy a megadott nap 23:59 időpontjáig adható le a kész munka
* A megoldást a canvas felületen adjátok le (méltányolható okokból lehet kivétel, ekkor emailben kérem)
* A határidőn túl beadott megoldásokat is átnézem, canvasban leírom, milyen lett - tanulni lehet belőle -, de arra pontot már nem adok
* Amennyiben megcsúszok a félév végén, a globális határidő mindenre: 2019. 07. 05., 16:00
* Hacsak más nincs mondva, minden feladat 1 pontot ér

# 2019. 02. 20. - Második hét, második óra

## 01_BubbleSort

* Határozzuk meg a tanult buborékrendezés algoritmusra az mcsere<sub>bubbleSort</sub>, az Mcsere<sub>bubbleSort</sub> és az Acsere<sub>bubbleSort</sub> értékeket - mindhárom helyes: 1 pont
* Bizonyítsuk is be a fentieket (lehet "esszé-jellegű", de legyen pontos, alapos) - mindhárom helyes: 1 pont
* Határidő: 2019. 03. 20.

# 2019. 02. 27. - Harmadik hét, harmadik óra

## 02_Legendre

* Határozzuk meg a tanult Legendre-algoritmus alapján az mszorzás<sub>legendre</sub>(a, k), Mszorzás<sub>legendre</sub>(a, k) és Aszorzás<sub>legendre</sub>(a, k) értékeket
* Indokoljunk is meg a fentieket konkrét a-kkal, k-kkal, levezetve, hogy jött ki a válasz
* Az algoritmusban látható osztás nem számít szorzásnak, csak a "csillag"-gal jelölt műveletet számoljuk
* Határidő: 2019. 03. 20.

# 2019. 03. 06. - Negyedik hét, negyedik óra

## 03_ThetaEkvRel

* Bizonyítsuk be, hogy a Θ-viszony ekvivalenciareláció
    * reflexív
    * tranzitív
    * szimmetrikus
* Határidő: 2019. 05. 20.

## 04_OrdoOmegaEkvRel

* Vajon ekvivalenciareláció-e Ο és Ω? Döntsük el, és bizonyítsuk!
* Határidő: 2019. 05. 20.

# 2019. 03. 20. - Hatodik hét, hatodik óra

## 05_TorlesSL

* Írjuk meg a delete(&l : E1*, k : T) : L függvényt
    * l egy egyszerű láncolt lista (S1L), ami rendezett
    * Törölje a k kulcs első előfordulását tartalmazó E1-et
    * Térjen vissza egy boollal, ami igaz, ha sikerült a törlés (azaz volt k kulcsú elem)
    * Nyilván a törlést megelőzi egy keresés, ezt rátok bízom milyen módszerrel, hány pointerrel oldjátok meg
* Határidő: 2019. 05. 20.

# 2019. 03. 27. - Hetedik hét, hetedik óra

## 06_TorlesHL

* Írjuk meg a delete(f : E1*, k : T) : L függvényt
    * f egy fejelemes láncolt lista (H1L), ami rendezett
    * Törölje a k kulcs első előfordulását tartalmazó E1-et
    * Térjen vissza egy boollal, ami igaz, ha sikerült a törlés (azaz volt k kulcsú elem)
    * Nyilván a törlést megelőzi egy keresés, ezt rátok bízom milyen módszerrel, hány pointerrel oldjátok meg
* Határidő: 2019. 05. 20.

## 07_Beszuras2L

* Írjuk meg az insert(&l : E2*, q : E2*) függvényt
    * l egy fejelem nélküli, rendezett kétirányú lista (S2L)
* Tipp: Nem kell két pointer a kereséshez, hiszen van visszairányú mutatónk
    * Viszont azt sem szabad elfelejteni frissíteni!
* Határidő: 2019. 05. 20.

## 08_TorlesH2L

* Írjunk függvényt, ami egy kétirányú, fejelemes listából törli a megadott kulcsú elemet
* A rendezettség rátok bízva
* A visszatérési típus és annak értelmezése is
* Határidő: 2019. 05. 20.

## 09_InsertionSort

* Írjuk meg az insertionSort algoritmust egyszerű listára (S1L)
* Határidő: 2019. 05. 20.

# 2019. 04. 03. - Nyolcadik hét, nyolcadik óra

## 10_Metszet

* Írjuk meg az intersect(f1 : E1*, f2 : E1*) függvényt
    * Az input két rendezett HL (fejelemes lista)
    * Az f1-be az algoritmus futásának végére kerüljön az eredeti f1 és f2 metszete (halmaz értelemben)
    * f2 pedig ne változzon (immutability)
    * A műveletigény legyen lineáris
* Határidő: 2019. 05. 25.

# 2019. 04. 10. - Kilencedik hét, kilencedik óra

## 11_Zarojelezes

* Írjuk meg a parentheses(s : InStream) : L függvényt
    * A bemenet egy olyan input folyam, ami egy s eleme { '{', '}', '[', ']', '(', ')' }* szót reprezentál
    * Azaz egy szóban a három féle zárójel karakterei lehetnek
    * A zárójel-fajták között az alábbi precedenciasorrend van:
        * {}-ben minden lehet
        * []-ben csak [] és ()
        * ()-ben csak ()
    * Minden zárójel párban kell legyen
    * 3 db számlálóval, verem meg egyebek nélkül döntsük el, hogy helyesen van-e zárójelezve egy ilyen szöveg
* Határidő: 2019. 05. 25.

# 2019. 05. 06. - Tizenkettedik hét, első zh konzultáció

## 12_Rendezes2Veremmel

* Írjuk meg a konzultáción hallott (és az ottani fájlban részletezett) "Rendezés két veremmel" feladat algoritmusát
* Határidő: 2019. 05. 25.

## 13_Nevsorforditas

* Írjuk meg a konzultáción hallott "Névsorfordítás" algoritmust az alábbi változatban:
    * Input: vesszőkkel elválasztott, kettőskereszttel terminált, szavakat tartalmazó szöveg, mint stringstream
    * Output: ugyanilyen formátumban de fordított sorrendben
    * Példa: s := Andras,Bea,Csilla,..,Zeno# -> out := Zeno,...,Csilla,Bea,Andras#
    * Stratégia
        * Két veremmel
        * Bedobok mindent v1-be
        * Miután kész vagyok ezzel, végigmegyek v1 elemein és átpakolom v2-be, de úgy, hogy amikor elválasztójel jön, akkor mindig kiírok v2-ből
* Határidő: 2019. 05. 25.

# 2019. 05. 08. - Tizenkettedik hét, tizenegyedik óra

## 14_SorImplementacio

* Adj meg egy típust reprezentációval és műveletimplementációkkal, ami a Queue interfészt megvalósítja
    * A reprezentáció legyen egy nem végelemes lista, ill. tartalmazzon egy "hossz" attribútumot is
    * Most nem elvárás a feltétlen konstans műveletigény mindenre
* Határidő: 2019. 05. 26.

# 2019. 05. 09. - Tizenkettedik hét, tizenkettedik óra

## 15_LKKT

* Írjuk meg a legkisebb közös többszöröst kiszámoló programot
    * Bemenetként adott két szám prímtényezős felbontása egy-egy monoton növő sorban
    * Állítsuk elő egy harmadik sorba a két szám lkkt-jét
    * Összefuttatásos algoritmus legyen, de mindegy, hogy unió- vagy metszet-alapú
    * Mindegy, hogy s1 vagy s2 vagy mindkét paraméter immutable, de az egyik legalább legyen az
* Határidő: 2019. 05. 26.

## 16_Pascal

* Írjuk meg a pascal(n : N) : Queue fejlécű algoritmust
    * Adja vissza n pozitív egész függvényében a Pascal-háromszög n-edik sorát egy Queue-ként
    * Pl.: pascal(5) -> <1, 4, 6, 4, 1>
* Határidő: 2019. 05. 26.

## 17_ZarojelezesKifejezesfaval

* Írjunk algoritmust, ami egy láncoltan megadott kifejezésfát kapva kiír a konzolra egy annak megfelelő teljesen zárójelezett infix kifejezést

    ```
         +
        / \
       a   b
	```	
		
* Pl. ez legyen (a+b), ne ((a)+(b))
* Határidő: 2019. 05. 26.

# 2019. 05. 13. - Tizenharmadik hét, 2. zh konzultáció

## 18_FeltMaxFara

* Írjuk meg a feltételes maximumkeresést fára
    * Legyen adott egy béta : T -> L függvény, s csak azok között keressünk, amikre a béta igazat ad
    * Térjünk vissza egy boollal, ami legyen igaz, ha volt béta tulajdonságú elem, és hamis, ha nem (akár azért, mert üres volt a fa)
    * Egy paraméterbe írjuk be a maximumértéket, ha volt
    * Legyen a műveletigény lineáris
* Határidő: 2019. 05. 28.

## 19_LegkorabbiLevel

* Határozzuk meg a legmagasabban (gyökérhez legközelebb) levő levél szintjét
    * Ha nincs ilyen, térjünk vissza -1-gyel
    * Az input egy ADT-szinten adott bináris fa
    * Az algoritmus legyen rekurzív
* Határidő: 2019. 05. 28.

# 2019. 05. 15. - Tizenharmadik hét, tizenharmadik óra

## 20_BinarisKeresofaFelepites

* Írj struktogramot, ami az órán látott módon, az üres fából kiindulva egy számokat tartalmazó stream elemeit egyesével beszúrva felépít egy bináris keresőfát
* Határidő: 2019. 05. 31.

# Szorgalmi feladatok a pluszanyagokhoz

## 21_RakovetkezesKeresofaban

* Írj struktogramot, ami egy megadott csúcsra visszaadja annak a rendezés szerinti rákövetkezőjét (vagy ÜRES fát, ha nincs)
    * Az algoritmus legyen rekurzív!
* Határidő: 2019. 05. 31.

## 22_PrioritasosSorRemMax

* Írd meg kupacos implementációra a PrQueue::remMax() metódust, mely egy prioritásos sor maximumát veszi ki a sorból
* Határidő: 2019. 05. 31.
