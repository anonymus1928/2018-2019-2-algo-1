# ZH tematika

## zh1

* Műveletigény-elemzés
* Függvények aszimptotikus viszonya, definíciók, tételek, osztályok
* Láncolt listák típusai, műveletei, alkalmazásai
* Vermek interfésze, implementációi, alkalmazásai
* Lengyelformára hozás, kiértékelése

## zh2
* Sorok intefésze, implementációi, alkalmazásai
* Bináris fák interfésze, implementációi, nevezetes bejárásai, alkalmazásai

## Disclaimer
* A mintazh-kban előfordulhat, hogy a jelölések nem követik a mostani félév stílusát, illetve hogy olyan feladatok is szerepelnek bennük, ami az idei tematikában szándékosan vagy idő hiányában nem szerepelnek. A fenti lista az irányadó
* Az AVL-fa, B+-fa, tömörítés, mintaillesztés nem a félév része
* Néhány, a félév tematikáját jelentő témára nem maradt idő órán, ezeket a "09_Vizsgara" mappában találjátok (majd)
    * Bináris keresőfák interfésze, alkalmazásai
    * Kupacok interfésze, tömbös implementációja, tömb kupaccá alakítása, kupacrendezés
    * Prioritásos sorok interfésze, tömbös, kupacos reprezentácója
    * Lineáris rendezések (leszámláló, radixok, bináris radixok)
    * Hashelés (nyílt címzéses és láncolásos kulcsütközés-feloldás)