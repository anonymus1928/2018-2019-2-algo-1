# Algoritmusok és adatszerkezetek 1., első zh konzultáció

* Helye: D 00-623
* Ideje: 2019. 05. 06., 17.00 – 19.15 

## 1. feladat - Futásiidő-becslés

* Egymillió elemű tömbökre futtattunk összefuttatásos és buborékrendezéseket. Az összefuttatásos rendezéseink futásidejeinek átlaga 4 másodperc volt. Adjunk egy becslést, mennyi lehetett a buborékrendezéseké! (logaritmustáblázat mellékelve: log<sub>2</sub>(1000000) ~ 20)
* Megoldás:
	* Tudjuk, hogy nagyságrendileg n * log<sub>2</sub>(n) a mergesort műveletigénye, ami itt kb. 10<sup>6</sup> * 20 = 2 * 10<sup>7</sup> db művelet. Azt is tudjuk, hogy ez volt 4 másodperc
	* A buborékrendezés műveletigénye n<sup>2</sup>, azaz esetünkben (10<sup>6</sup>)<sup>2</sup> = 10<sup>12</sup>
	* Ha most elosztjuk AT<sub>BS</sub>(10<sup>6</sup>)-t AT<sub>MS</sub>(10<sup>6</sup>)-nal, azaz 10<sup>12</sup>-t 2 * 10<sup>7</sup>-nel, akkor megkapjuk tehát, hogy a bubble sort hányszor volt lassabb, mint a mergesort, s mivel az 4 másodperc volt, azt kapjuk így meg, hányszor 4 másodpercig futottak kb. a buborékrendezéseink: 10<sup>5</sup> / 2
	* Tehát 10<sup>5</sup> / 2 = 50000 * 4 másodperc az időigény, ami 200000 s, ami 3333 és 1/3 perc, ami 55,55 óra, ami 2,3 nap

## 2. feladat - Műveletigény-osztályok

* Osszuk be a megfelelő osztályba, majd állítsuk nagyságrend szerinti növekvő sorrendbe az alábbi műveletigényeket leíró függvényeket. Ha akadnak nagyságrendileg azonos osztályba tartozó függvények is, ezt külön jelöljük
	* sqrt(n) + 5
	* Átrak<sub>HanoiTornyai</sub>(n)
	* n * ln(n) + n<sup>1,1</sup>
	* n * ln(n) + n<sup>0,9</sup>
	* 10 * n<sup>3</sup> + 12 * n<sup>10/3</sup> – 130n + log(n)
* Megoldás:
	* A Hanoi tornyai feladatról tudjuk, hogy pontosan 2<sup>n</sup> - 1 átrakást jelent, ami miatt nagyságrendileg a Θ(2<sup>n</sup>) függvényosztályba tartozik
	* Az összes többi esetben a "szekvencia műveletigénye" tétel alapján a domináns tag műveletigényét kell néznünk
		* A gyökfüggvény meredekebben nő, mint a konstans (ami semennyire), így sqrt(n) + 5 eleme Θ(sqrt(n)), vagy más néven Θ(n<sup>1/2</sup>)
			* Látjuk, hogy tulajdonképpen ez egy hatványfüggvény. Azt kell megjegyeznünk, hogy a hatványfüggvények egymással nem ekvivalensek aszimptotikusan, mennél nagyobb a kitevő, annál erősebb a függvény. Az 1 alattiak ráadásul a lineárisnál is gyengébbek!
		* A következőnél az n<sup>1,1</sup> a meredekebb tag, és mint láttuk, a kitevő nem mindegy, ezért n * ln(n) + n<sup>1,1</sup> eleme Θ(n<sup>1,1</sup>)
		* Viszont az 1 alatti kitevő már gyengébb az n * log(n)-nél, ezért n * ln(n) + n<sup>0,9</sup> eleme Θ(n * log(n)), mivel a logaritmus alapja irreleváns
		* Végül itt is a legnagyobb kitevőjű tagot kell néznünk, ami trükkös módon nem az első: 12 * n<sup>10/3</sup>, de az együtthatókat elhagyhatjuk, ezért az utolsó kifejezés eleme Θ(n<sup>10/3</sup>)
	* A sorrend könnyedén előáll: sqrt(n) + 5 ≺ n * ln(n) + n<sup>0,9</sup> ≺ n * ln(n) + n<sup>1,1</sup> ≺ 10 * n<sup>3</sup> + 12 * n<sup>10/3</sup> – 130n + log(n) ≺ Átrak<sub>HanoiTornyai</sub>(n)
		* Mivel nincs Θ-viszony egyik pár között sem, a sorban mindegyik függvény eleme az őt követő függvényre vett kis ordó halmaznak, példa: sqrt(n) + 5 eleme ο(n * ln(n) + n<sup>0,9</sup>)
		* Ha valahol ekvivalencia lett volna, az a Θ-kapcsolatot jelentené
		* Amit tudni kellett a sorbarendezéshez:
			* A hatványfüggvényeknél amelyik nagyobb kitevő, az nagyobb nagyságrend
			* Az n * log(n) nagyobb, mint bármelyik 1 alatti kitevőjű hatványfüggvény, és kisebb, mint bármelyik 1 feletti kitevőjű (és egyébként nagyobb, mint az n<sup>1</sup>-en, azaz a lineáris függvény)
			* A 2<sup>n</sup> mindenkinél sokkal nagyobb

## 3. feladat - Műveletigény-osztály bizonyítás

* Bizonyítsuk be a definícióból, hogy 0,5 * n<sup>3</sup> - n + 15 eleme Ο(n<sup>3</sup>)
* Megoldás:
	* Az Ο(n<sup>3</sup>) /kiolvasva nagy ordó/ egy halmaz, amibe azon függvények tartoznak, amelyikeknek n<sup>3</sup> aszimptotikus felső korlátja
	* A halmaz definíciója az alábbi: Ο(n<sup>3</sup>) := { g : N->R | van c>0 konstans és n<sub>0</sub> >= 0 küszöbindex, hogy minden n >= n<sub>0</sub>-ra: g(n) <= c * n<sup>3</sup> }
	* Ha az a kérdés, hogy 0,5 * n<sup>3</sup> - n + 15 egy ilyen "g"-e, akkor csak annyi a dolgunk, mutassunk megfelelő c-t és n<sub>0</sub>-t
		* Írjuk fel az egyenlőtlenséget: 0,5 * n<sup>3</sup> - n + 15 <= c * n<sup>3</sup>
		* Osszunk le n<sup>3</sup>-nal, ami máris maga után vonja, hogy n<sub>0</sub> legalább 1 (hogy tudjunk osztani - n természetes szám, ne feledjük): 0,5 - (1/n<sup>2</sup>) + (15/n<sup>3</sup>) <= c
		* Itt láthatjuk, hogy a bal oldal második és harmadik tagja ahogy n-t növeljük (márpedig növeljük, lásd definíció) egyre csak csökkenni fog, míg az első tag és a c nyilván erre érzéketlen
			* Tehát a bal oldal egyre közelebb lesz 0,5-höz..., ez n=1-re még 0,5 - 1 + 15, azaz 14,5, de utána egyre kisebb lesz, mivel a 3. tag gyorsabban csökken abszolút értékben, mint a második!
			* Azaz, ha megválasztjuk n<sub>0</sub>-t a "legrosszabb" esetnek, ami az 1, és erre is tudunk adni egy jó c-t, akkor nyert ügyünk van
			* De a c-t már meg is állapítottuk: legyen 14,5, arra igaz az egyenlőtlenség
	* Egyébként a Θ-viszony (Théta) is igaz lenne, de ez egy másik bizonyítás. Abból viszont mind a Ω (nagy omega), mind az Ο következik, de a o (kis ordó) és a ω (kis omega) éppen hogy nem!
    * Ilyen feladatoknál lehetőség szerint mindig mondjunk konkrét n<sub>0</sub>-t és c-t (konstruktív bizonyítás), vagy adjunk írásban egyértelmű érveket arra, hogy "kell lennie" ilyen számoknak

## 4. feladat - Láncolt listás algoritmus

* Töröljük egy megadott kulcs utolsó előfordulását jelentő csúcsot egy kétirányú, nem fejelemes, nem ciklikus, nem rendezett listából! Térjünk vissza azzal, hogy sikeres volt-e a törlés
* Megoldás:
	* Végig kell mennünk egyesével a listán, és egy segédpointert minden alkalommal frissítenünk kell, amikor a megadott kulccsal rendelkező node-ot látogatunk. Ekkor ez a mutató a végén az utolsó találatra fog mutatni (vagy NULL-ra ha nem is volt)
	* Most mivel oda-vissza listánk van, nem kell se két pointer, se a dupla előrenézős módszer, hiszen bár szükségünk lesz a megtalált listaelem előzőjére a kiláncolás miatt, de azt le fogjuk tudni kérni belőle is
	* Mivel nem fejelemes a lista, várhatóan azt az esetet külön kell kezelnünk, amikor a lista első eleme a találat (hiszen neki nincs előzője)
	
    ```
    deleteLastOccurrence(&l : E2*, k : T) : L
    	t = NULL
    	p = l
    	AMÍG p != NULL
    		HA p->key == k
    			t = p
    		p = p->next
    	HA t == NULL
    		return false
    	KÜLÖNBEN
    		HA t == l
    			l = t->next
    			HA l != NULL
    				l->prev = NULL
    			delete t
    			return true
    		KÜLÖNBEN
    			t->prev->next = t->next
    			HA t->next != NULL
    				t->next->prev = t->prev
    			delete t
    			return true
    ```
	
	* l-t referencia szerint adjuk át, hiszen van olyan eset, amikor értéket adunk neki
	* Ha van találatunk, akkor kettéválunk aszerint, hogy ő az első elem-e vagy nem
	* Ha jobban megnézzük, a két ágban csak az a különbség, hogy l-nek adunk-e új értéket, vagy a találat előzőjének (nyilván t->prev létezik, ha t nem l)
	* A másik különbség nem is különbség: a t->next->prev-es értékadásnál a NULL nyugodtan helyettesíthető t->prevvel, ha t==l, mivel l-nek nincs előzője
	* Azaz a kód így egyszerűsíthető:
	
    ```
    deleteLastOccurrence(&l : E2*, k : T) : L
    	t = NULL
    	p = l
    	AMÍG p != NULL
    		HA p->key == k
    			t = p
    		p = p->next
    	HA t == NULL
    		return false
    	KÜLÖNBEN
    		HA t == l
    			l = t->next
    		KÜLÖNBEN
    			t->prev->next = t->next
    		HA t->next != NULL
    			t->next->prev = t->prev
    		delete t
    		return true
    ```
	
	* zh-n törekedjünk a minél rövidebb algoritmusra. Ami effektíve pontlevonást ér, ha az aszimptotikus műveletigény nagyobb, mint az elvárt. De ha csak konstansban tér el, netán kódismétlést tartalmaz, de a lépésszámra ez nem hat, az bár nem a legszebb megoldás, de a szempontunkból ugyanúgy hibátlan

## 5. feladat - Rendezés két veremmel

* Adott egy veremben néhány szám, ürítsük ki ezt a vermet, és egy másik verembe helyezzük át a számokat, de úgy, hogy rendezett sorrendben legyenek. A két vermen kívül más adatszerkezetet nem használhatunk, a szokásos verem interfész műveletei állnak csak rendelkezésünkre
* Megoldás:
	* Csak a lejátszást adtuk meg, a konkrét struktogram **házi feladat** volt
	* A vermet így fogom jelölni: [a, b, c. Ez azt jelenti, hogy a veremtető "c", alatta "b", és az alatt "a" van a veremben
	* Legyen a példabemenet az alábbi:
		* input = [25, 2, 12, 30, 4
	* Hozzunk létre egy újabb üres vermet, legyen a neve output, majd ezzel fogunk visszatérni
	* Vegyük le az input felső elemét (csak ehhez férünk hozzá eleve), majd helyezzük át:
		* input = [25, 2, 12, 30
		* output = [4
	* Ezt ismételgessük egészen addig, amíg vagy ki nem ürül az input (ekkor kész is vagyunk), vagy nem lesz igaz az az állítás, hogy van az outputnak eleme és ez a felső elem nagyobb, mint az input felső eleme:
		* input [25, 2, 12
		* output [4, 30
	* Azaz itt meg is állunk, a 30 nagyobb, mint a 12
	* Ilyenkor a 12-t kirakjuk egy ideiglenes változóba, és egészen addig, amíg létezik az output teteje és nagyobb, mint a kivett elem, visszarakosgatjuk az outputról az inputra az elemeket:
		* input [25, 2, 30 temp = 12
		* output [4
	* Amikor vagy kifogyott az output, vagy már nem nagyobb a teteje 12-nél, berakjuk:
		* input [25, 2, 30
		* output [4, 12
	* És folytatjuk az eddigi eljárást, amíg az input ki nem ürül:
		* input [25, 2
		* output [4, 12, 30
	* Most nagyobb az output teteje:
		* input [25, 30, 12, 4 temp = 2
		* output [
	* Kiürült, ezért berakhatjuk a 2-t:
		* input [25, 30, 12, 4
		* output [2
	* Most megint átrajuk a nagyobb elemeket:
		* input [25
		* output [2, 4, 12, 30
	* Most a 25 jönne, ami kisebb, mint a veremtető, ezért félretesszük, és az összes nagyobbat (egyedül a 30) visszarakjuk az inbe:
		* input [30 temp = 25
		* output [2, 4, 12
	* Mehet a 25, majd a 30 az outba:
		* input [
		* output [2, 4, 12, 25, 30
	* Kész

## 6. feladat - Palindrom 2 veremmel

* Döntsük el egy "stringstream" bemenetről (nem ismert előre a hossza), hogy egy palindrom szót ad-e. Használjunk verme(ke)t!
* Megoldás:
	* Először is nézzük meg, mi lenne, ha a bemenet tömbszerű lenne, azaz indexelhető és lekérdezhető méretű:
		
		```
		palindrom(s : String) : L
			i = 1 to alsóEgészRész(|s|/2)
				HA s_i != s_|s|-i+1
					return false
			return true
		```	
		
		* Páratlan méretű stringnél a középső elem önmagával kellene hogy egyenlő legyen, ezért ezt külön nem is vizsgáljuk
	* Vermes megoldásnál ha véletlen tudnánk az input méretét, akkor a következőt tennénk:
		* Beolvassuk a feléig a karaktereket egy verembe
		* Ha páratlan számú, eldobjuk a középsőt
		* És most, az inputon maradó második felét egyesével olvassuk be, minden soron következő elemet összehasonlítunk a veremtetővel (amit utána el is távolítunk), itt ugyebár a veremből pont fordított sorrendben fogjuk elővarázsolni a string első felét, ami épp megfelel szándékainknak
	* Ha nem ismerjük a méretet, akkor előbb egy verembe beolvassuk karakterenként, ezzel együtt számoljuk a beolvasott karaktereket, majd a fenti logika alapján a felét átmásoljuk egy másik verembe, megvizsgáljuk a méret paritását, majd a két verem elemeit sorban összehasonlítjuk:
		
		```
		palindrom(x : InStream) : L
			v = Stack()
			n = 0
			ch = x.read()
			AMÍG ch != EOF
				v.push(ch)
				n = n+1
				ch = x.read()
			w = Stack()
			i = 1 to alsóEgészRész(n/2)
				w.push(v.pop())
			HA 2 ∤ n
				v.pop()
			AMÍG !v.isEmpty()
				HA v.pop() != w.pop()
					return false
			return true
		```

    * Mivel n az input mérete, ezért az input feléig menő pop hívások - amiket nem előzött meg isEmpty hívás a helyességüket ellenőrizendő - szabályosak
    * Hasonlóan a végén, bár csak v nemürességét vizsgáljuk, de értelemszerűen ekkor w se lehet üres

## 7. feladat - Névsorfordítás 2 veremmel

* Írjunk algoritmust, ami egy vesszőkkel elválasztott, kettőskereszttel terminált, szavakat tartalmazó szöveget képes ugyanilyen formátumban, de fordított sorrendben visszaadni
* Megoldás:
	* Példa: s := Andras,Bea,Csilla,..,Zeno# -> out := Zeno,...,Csilla,Bea,Andras#
	* Karakterenként olvassuk, nem ismerjük a hosszát
	* A verem kiválóan alkalmas ilyen megfordítós feladatokra, de itt most az a feladat, hogy a vesszők mentén fordítva írjuk ki az elemeket, de a vesszőkön belül ne fordítsuk meg a sorrendet (de úgy is fogalmazhatok, hogy megfordíthatjuk, ha utána visszafordítjuk)
	* Ezért ennél a feladatnál két vermet fogunk használni
	* Két megoldási ötletet is elmondok, ebből az elsőt kidolgozom, a második **házi feladat**
		* Első megoldás: Bedobok mindent v1-be. De ekkor fordítva kerülne bele minden, ezért amikor elválasztó jön, akkor ami eddig volt v1-ben, azt v2-be rakom át, ami által újra megfordul a sorrend. Majd a végén v2-t kiírom
		* Második megoldás: Bedobok mindent v1-be, majd miután kész vagyok ezzel, végigmegyek v1 elemein és átpakolom v2-be, de úgy, hogy amikor elválasztójel jön, akkor mindig kiírok v2-ből (HF)

		```
		névsorFordítás2Veremmel(s : InStream, &out : OutStream)
			out = <>
			v1 = Stack()
			v2 = Stack()
			v2.push('#')
			ch = s.read()
			AMÍG ch != '#'
				AMÍG ch != '#' és ch != ','
					v1.push(ch)
					ch = s.read()
				AMÍG !v1.isEmpty()
					v2.push(v1.pop())
				HA ch == ','
					v2.push(',')
					ch = s.read()
			AMÍG !v2.isEmpty()
				out.print(v2.pop())
		```	
	
	* v2-be már a kezdetek kezdetén berakok egy #-et, hogy a végén az legyen az utolsó kiírandó elem
	* Elsőre nem biztos, hogy nyilvánvaló, miért ismételjük meg az AMÍG ch != '#' feltételt a belső ciklusban
		* A külső ciklus a teljes stringet nézi meg, míg azon belül a belső szavanként terminál. Az egyik (konkrétan az utolsó) szó végén viszont a # van, hát ezért
		* A HA ch ==',' feltétel is magyarázatra szorul: azt nézem itt, hogy a belső ciklus ugye nem azért ért véget, mert # volt, hanem mert vessző. Ha ez így van, akkor egyrészt majd be kell raknunk egy vesszőt a kiírandók közé (nyilván az utolsó szó után nem, mert akkor vesszővel kezdődne majd az output), és tovább is kell olvasnunk, hogy a következő szó elejével folytathassuk. Kettőskereszt esetében meg simán a következő körben terminálni fog a külső ciklus magától is
* Ha a tömbös veremimplementációt alkalmazzuk, lehet a két vermet egy és ugyanazon statikus méretű tömb elejeképp és végeképp megadni, mivel a két verem összméretének maximuma konstans

## 8. feladat - Lengyelformára hozás és kiértékelés

* Írjuk fel a lengyelformát egy verem segítségével kiértékelő függvényt, majd a megadott inputot a tanult módszerrel alakítsuk át RPN-re és utána értékeljük is ki a készített algoritmus segítségével! Input: s := ( ( ( 2 + 3 ) ^ 2 / 5 ) ^ 2 - 6 - 4 / 2 - 8 ) + ( 2 + 3 )
Megoldás:
	* A lengyelformát kiértékelő függvény hasonló elven működik, mint amit a teljesen zárójelezett kifejezések kiértékelésénél már megnéztünk - végül is, ugyanazt a feladatot oldjuk meg most is
	* Tehát ha operandust (változó vagy literál) olvasunk, berakjuk egy verembe, ha operátort (művelet), akkor pedig (élve azzal a feltevéssel, hogy csak binér műveleteink vannak) a verem két felső elemét összeműveleteljük majd az eredményt rakjuk be a verembe
	* A végén a végső eredmény fog a verem alján lapítani
	* Figyelem: amikor műveletet végzünk, vegyük figyelembe, hogy a jobb oldali paramétert vesszük ki előbb, tehát épp fordítva kell a paramétereket átadni a műveletnek, mint ahogy jöttek
	
		```
		kiértékelés(s : InStream) : Q
			v = Stack()
			ch = s.read()
			AMÍG ch != EOF
				HA ch eleme OPERANDUSOK
					v.push(ch)
				KÜLÖNBEN
					jobb = v.pop()
					v.push(kiértékel(v.pop() KONKAT ch KONKAT jobb))
				ch = s.read()
			return v.pop()
		```
		
	* Most az órán megismert módszerrel hozzuk lengyelformára ezt a kifejezést:
		* s := ( ( ( 2 + 3 ) ^ 2 / 5 ) ^ 2 - 6 - 4 / 2 - 8 ) + ( 2 + 3 )
	* Karakterenként olvasunk, minden körben kiírom az output és a verem állapotát
		* s - operandus, kiírjuk
			* v = [
			* out = s
		* := - operátor, berakjuk
			* v = [:=
			* out = s
		* ( - nyitó zárójel, berakjuk
			* v = [:= (
			* out = s
		* ( - nyitó zárójel, berakjuk
			* v = [:= ( (
			* out = s
		* ( - nyitó zárójel, berakjuk
			* v = [:= ( ( (
			* out = s
		* 2 - operandus, kiírjuk
			* v = [:= ( ( (
			* out = s 2
		* plusz - operátor, berakjuk
			* v = [:= ( ( ( +
			* out = s 2
		* 3 - operandus, kiírjuk
			* v = [:= ( ( ( +
			* out = s 2 3
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* v = [:= ( (
			* out = s 2 3 +
		* ^ - operátor, berakjuk
			* v = [:= ( ( ^
			* out = s 2 3 +
		* 2 - operandus, kiírjuk
			* v = [:= ( ( ^
			* out = s 2 3 + 2
		* / - operátor, kiírjuk a nagyobb precedenciájúakat, majd berakjuk
			* v = [:= ( ( /
			* out = s 2 3 + 2 ^
		* 5 - operandus, kiírjuk
			* v = [:= ( ( /
			* out = s 2 3 + 2 ^ 5
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* v = [:= (
			* out = s 2 3 + 2 ^ 5 /
		* ^ - operátor, berakjuk
			* v = [:= ( ^
			* out = s 2 3 + 2 ^ 5 /
		* 2 - operandus, kiírjuk
			* v = [:= ( ^
			* out = s 2 3 + 2 ^ 5 / 2
		* mínusz - operátor, kiírjuk a nagyobb precedenciájúakat, majd berakjuk
			* v = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^
		* 6 - operandus, kiírjuk
			* v = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6
		* mínusz - operátor, kiírjuk az egyenlő precedenciájúakat a balasszociativitás miatt, majd berakjuk
			* v = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 -
		* 4 - operandus, kiírjuk
			* v = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4
		* / - operátor, berakjuk
			* v = [:= ( - /
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4
		* 2 - operandus, kiírjuk
			* v = [:= ( - /
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2
		* mínusz - operátor, kiírjuk a nagyobb-egyenlő precedenciájúakat, majd berakjuk	
			* v = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / -
		* 8 - operandus, kiírjuk
			* v = [:= ( -
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* v = [:=
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 -
		* plusz - operátor, berakjuk
			* v = [:= +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 -
		* ( - nyitó zárójel, berakjuk
			* v = [:= + (
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 -
		* 2 - operandus, kiírjuk
			* v = [:= + (
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2
		* plusz - operátor, berakjuk
			* v = [:= + ( +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2
	    * 3 - operandus, kiírjuk
			* v = [:= + ( +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3
		* ) - csukó zárójel, kiírunk mindent a nyitó párjáig
			* v = [:= +
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3 +
		* Vége az inputnak, kiírok mindent a veremből
			* out = s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3 + + :=
	* Most pedig futtassuk le a feladat elején definiált függvényt:
	* Az előző rész outputja az input, karakterenként tudjuk olvasni: < s 2 3 + 2 ^ 5 / 2 ^ 6 - 4 2 / - 8 - 2 3 + + := >
		* s - operandus, berakjuk
			* v = [s
		* 2 - operandus, berakjuk
			* v = [s 2
		* 3 - operandus, berakjuk
			* v = [s 2 3
		* plusz - operátor, kivesszük, kiértékeljük a 2+3-at, berakjuk
			* v = [s 5
		* 2 - operandus, berakjuk
			* v = [s 5 2
		* ^ - operátor, kivesszük, kiértékeljük a 5^2-t, berakjuk
			* v = [s 25
		* 5 - operandus, berakjuk
			* v = [s 25 5
		* / - operátor, kivesszük, kiértékeljük a 25/5-öt, berakjuk
			* v = [s 5
		* 2 - operandus, berakjuk
			* v = [s 5 2
		* ^ - operátor, kivesszük, kiértékeljük a 5^2-t, berakjuk
			* v = [s 25
		* 6 - operandus, berakjuk
			* v = [s 25 6	
		* mínusz - operátor, kivesszük, kiértékeljük a 25-6-ot, berakjuk
			* v = [s 19
		* 4 - operandus, berakjuk
			* v = [s 19 4
		* 2 - operandus, berakjuk
			* v = [s 19 4 2
		* / - operátor, kivesszük, kiértékeljük a 4/2-t, berakjuk
			* v = [s 19 2
		* mínusz - operátor, kivesszük, kiértékeljük a 19-2-t, berakjuk
			* v = [s 17
		* 8 - operandus, berakjuk
			* v = [s 17 8		
		* mínusz - operátor, kivesszük, kiértékeljük a 17-8-at, berakjuk
			* v = [s 9
		* 2 - operandus, berakjuk
			* v = [s 9 2
		* 3 - operandus, berakjuk
			* v = [s 9 2 3
		* plusz - operátor, kivesszük, kiértékeljük a 2+3-at, berakjuk
			* v = [s 9 5
		* plusz - operátor, kivesszük, kiértékeljük a 9+5-öt, berakjuk
			* v = [s 14
		* := - operátor, kivesszük, kiértékeljük az s := 14-et, berakjuk
			* v = [14
		* S ezzel térünk vissza (persze mellékhatásként az s változó értéke is 14 lett)
	* Érdemes ilyenkor egyszer kiszámolni az infix változatot is fejben, hogy meggyőződjünk, jól csináltuk
