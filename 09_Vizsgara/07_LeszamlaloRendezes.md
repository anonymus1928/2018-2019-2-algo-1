# Leszámláló rendezés (Counting Sort)

## Elv

* Adott egy a : T[n] tömb, azaz egy a nevű T-ket tartalmazó n elemű tömb, amit 1-től n-ig indexelünk
* Ezt szeretnénk lineáris időben rendezni egy hasonló méretű és alakú b nevű tömbbe
* Azaz ez a rendezés nem helyben rendez, viszont stabil
* Feltétele, hogy legyen adott egy h : T → [0..k] olcsón kiszámítható, T-t egyenletesen elosztó leképezés, ahol k egy természetes szám és jóval kisebb, mint n (másképp fogalmazva: k ∈ Ο(n))
* h tulajdonképpen egy hash-függvény lesz (lásd ezután), aminek a dolga, hogy szortírozza az input elemeit a 0-tól k-ig indexelt, összesen tehát k+1 db edénybe
* Itt most a T a kulcsok halmaza, ezek a kulcsok bármik lehetnek, egész addig, amíg létezik ez a leképezés
* Ez az előfeltétel tulajdonképpen olyanná teszi T elemeit, mint az előadáson hallott nem számjegypozíciós edényrendezés inputja -- azaz nem elvárás, hogy a kulcs hierarchikus legyen, viszont azt elvárjuk, hogy diszkrét értékei legyenek egyenletes eloszlással
* Ekkor a következőt tehetjük:
    * Létrehozunk egy z segédtömböt, ami 0-tól van indexelve k-ig (azaz k+1 elemes) és természetes számokat tartalmaz (azaz z : N[k+1])
    * Elemeit 0-ra inicializáljuk
    * Végigolvassuk az inputot, mindegyik elemen rendre meghívjuk h-t, amit a függvény visszaad, a z annyiadik indexét eggyel megnöveljük - azaz megszámláljuk h melyik értékéből mennyi van
    * A végén a z tömb elemeit kumuláljuk, azaz sorban haladva a z minden eleméhez hozzáadjuk a megelőző elem értékét (avagy a z elemeiben a kumulálás előtti z elemeinek kezdőrészlet-összege lesz)
        * Nyilván ezt a ciklust elég 1-től (a második elemtől) indítani, mert a z[0] nem változik
    * Most a z-ben nem az lesz, hogy az egyes h értékek hányan vannak, hanem ha egymás mögé rakjuk őket rendezve, hányadik pozíciónál kezdődne az adott érték spektruma
    * Ha ez megvan, még egyszer végigmegyünk az elemeken, de ezúttal fordított sorrendben (a stabilitás megtartása miatt). Újra meghívjuk h-t minden elemre, megnézzük, a z annyiadik indexén milyen érték van: amit találunk b annyiadik indexére áthelyezzük a feldolgozás alatt álló elemet, majd a z megfelelő elemét csökkentjük, hogy legközelebb már a most beszúrt elem elé pakoljunk (na, ezért marad meg a stabilitás)
    * A végén a z-t persze eldobhatjuk
* Az a-n (azaz az [1..n] intervallumon) kétszer, a z-n (azaz a [0..k] intervallumon) egyszer megyünk végig, a h-kat 2n-szer számoljuk ki, de ez elemi műveletnek tekinthető, k pedig elhanyagolható n tükrében, így a rendezés ϴ(n)-es

## Lejátszás

* Legyen az input: a[n] = [12, 13, 00, 01, 32, 11]
* Ne zavarjon meg minket, hogy a elemei hierarchikus diszkrét elemű kulcsok, így a számjegypozíciós rendezésre is jók lennének, itt ezt nem használjuk ki, akár traktorok vagy playmate-ek is lehetnének a kulcsok...
* Legyen a h függvény, ami a (t,e) párhoz t-t rendel, vagy másképp, ami az e elemhez ⌊e/10⌋-et rendel, azaz ami visszaadja az első számjegyét (h(12) = 1, stb)
* Ez alapján erre az inputra k = 3 az ideális választás -- ezt persze paraméterként kapjuk, nem az inputból nézzük ki
* Az alábbi táblázat értelmezése:
    * A fejlécen kívüli sorai z indexei
    * Az oszlopok az idő múlását, azaz az algoritmus lépéseit jelölik (tehát minden oszlop z tartalmának egy pillanatképe)
    * Az INIT résznél hozzuk létre z-t csupa 0 értékkel
    * Az egyes számozott körökben az a i. eleme h szerinti képét, mint z-beli indexet számoljuk ki és növeljük a hozzá tartozó értéket -- az üresen maradó cellákat úgy értjük, hogy az értékük az maradt, ami az előző körben volt
    * A SUM oszlopban áttekintjük mi lett z állapota a számlálás után
    * A KUM oszlopban áttekintjük, mi lett z állapota a kumulálás után
    * Majd újra végigmegyünk az [1..n] intervallumon, ám ezúttal visszafelé
    * Közben építjük b-t -- ami az aktuális h(a[i])-edik eleme z-nek, annyiadik b-beli elemként felvesszük a[i]-t, majd csökkentjük ezt a z-beli értéket (a csökkentett értéket a táblázatba írjuk)
* Ne feledjük el B-t is írni a táblázat alatt

```
       INIT 1.  2.  3.  4.  5.  6.  SUM KUM 6.  5.  4.  3.  2.  1.
    0  0            1   2           2   2           1   0
    1  0    1   2               3   3   5   4               3   2
    2  0                            0   5
    3  0                    1       1   6       5

    b=[00, 01, 12, 13, 11, 32]
```

* Láthatjuk, b elemei rendezettek az első számjegy szerint, az "egyenlőnek" értékelt elemek között pedig a sorrend az a-beli eredeti sorrend -- azaz a rendezés stabil
* Az, ahogy említettem, ne zavarjon meg, hogy b-ben nem teljesen sorrendben vannak a számok -- a rendezési relációnk most ebben a példában csak az első helyiértéket vette figyelembe: a 13 és a 12 ekvivalensnek számít a hash szempontjából