# Kupac (nem 2Pac)

## Szemlélet

* A kupac (magyarul heap) egy iterált adatszerkezet
* Ábrázolás szintjén általában tömbös (okokat lásd később), de az absztrakciónk róla az, hogy ő egy speciális fa
	* Konkrétan: majdnem teljes, balra tömörített bináris fa, aminek minden csúcsára igaz a kupac-tulajdonság
		* Majdnem teljes: Ha az utolsó sorát elhagynánk, teljes fa lenne
			* A teljes fa azt jelenti, hogy minden belső csúcsnak szigorúan 2 gyereke van és minden levél egy szinten van, a fa alakja "háromszög"
			* A majdnem teljességet úgy is megfogalmazhatjuk, hogy minden levél az utolsó két szinten van
			* Még egy megfogalmazás: nincs "megkezdett" sora, amíg nem telt be minden afeletti sor
			* Vagy: a maximális teljes részfája maximum eggyel alacsonyabb, mint ő
			* A teljes fához képest maximum az utolsó szinten maradhatnak ki csúcsok
			* Ebből következik (de nem ekvivalens vele), hogy a mérete (csúcsok száma) nagyobb, mint 2<sup>magassága</sup>-1 és kisebb-egyenlő mint 2<sup>magassága+1</sup>-1
			* A majdnem teljes fára, ha az utolsó sort elhagyjuk, teljes fát kapunk (ezért van, hogy a 2<sup>magassága</sup>-1-nél nagyobb, ami az eggyel kisebb magasságú teljes fa magassága)
			* Ha lenne "kilógó" levél, a fa magassága eggyel nagyobb lenne, így a fenti egyenlőtlenségek sem stimmelnének
			* A definíciók és a következmények is megengedik, hogy a teljes fát is majdnem teljesnek vegyük, ez teljesen jó is nekünk, mert így minden n csúcsszámra létezik n csúcsú majdnem teljes fa, így a kupacság fenntartható csúcsok beszúrása és törlése esetén is
		* Balra tömörített: Az alsó sora ha nem teljes, az összes elem balról van feltöltve. Másképpen, ha az utolsó előtti sorban nincsenek minden csúcsnak gyerekei, ezek az üres helyek a lehető legjobboldalibb pozíciókba tömörülnek
		* Bináris: minden csúcsnak maximum két gyereke van
		* Fa. Tehát NEM kereső-/rendezőfa! Még akkor is, ha keresésre és főleg rendezésre használjuk
		* Kupac-tulajdonság: minden gyerek kulcsa kisebb-egyenlő, mint a szülőé (innen nyilvánvaló, hogy nem keresőfa, bár valamiféle a keresést segítő tendencia itt is van!)
			* Ebből következik, hogy minden részfa minden kulcsa kisebb-egyenlő, mint a részfa gyökeréé, valamint hogy a fa gyökere tartalmazza a maximumát, a globális minimum pedig levél, de nem feltétlen a legalsó sorban, hiszen egy csúcs részfái között már egymáshoz képest nincs semmilyen rendezettségi viszony
		* Néhány még felhasznált fogalom definíciója:
			* Részfa: önmaga, üres fa, és minden az egyik leszármazottjából, mint gyökércsúcsból kiinduló olyan részgráf, ami fa
			* Leszármazott: a gyerekség tranzitív lezártja. Azaz egy csúcs gyerekei, annak gyerekei, stb.
			* Valódi részfa: részfa, de nem üres és nem önmaga (a nem ürességet nem feltétlen várjuk el minden tárgyból, de itt most igen)

## Interfész

* A szokásos műveleteken kívül...
* max() - konstans műveletigény az elvárt
* removeMax() - log<sub>2</sub>(n) műveletigény az elvárás

## Reprezentáció

* Az aritmetikai - azaz tömbös - ábrázolás a menő, mégpedig a balra tömörítettség miatt (nincsenek felesleges, elharapódzó mennyiségben jelenlevő NULL-ok)
* A szokásos faműveleteket és a tömbös megvalósításukat ismerjük korábbról
* A max() csak visszaadja a tömb első elemét
* A removeMax()-ot a kupacrendezésnél, ill. a prioritásos soroknál ismertetem
* Láncolt reprezentációval is megvalósítható