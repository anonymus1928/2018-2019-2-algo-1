# Lineáris rendezések - Bevezetés

* A lineáris idejű rendezések nem kulcs-összehasonlítással operálnak, ezért egyrészt nem teljesül rájuk, hogy legjobb esetben is Θ(n*log(n))-es a műveletigényük; másrészt vannak bizonyos feltételek, amiknek meg kell felelnie az inputnak, hogy ilyen (hatékony) rendezések alkalmazhatók legyenek rá
* Fajtái
    * Leszámláló rendezés
        * Alkalmazható, ha az adatok olyanok, hogy viszonylag egyenletes eloszlással le tudom képezni őket valamilyen nem feltétlen egyedi "kulcsra" és ez a leképezés olcsó
        * Megnézem, hogy az egyes kulcsokból kumulálva mennyi van. Ez alapján tudom, hogy az eredménytömb hányadik indexétől kezdve kell feltöltenem az adott kulcsú elemeket, majd újra végig kell menni az inputon és a kigyűjtött indexek szerint megtenni a feltöltést
        * A rendezés stabil, azaz ha A és B azonos kulcsú, de A előbb volt eredetileg, mint B, akkor a rendezett gyűjteményben is előrébb lesz
        * A rendezés nem helyben rendez (kell hozzá egy újabb gyűjteményt allokálni)
    * Edényrendezés
        * Az inputot -- megint egyenletes eloszlást feltételezve -- szétszórjuk "edényekbe", mégpedig úgy, hogy átlagosan csak kevés jusson egy edénybe!
        * Az edények lehetnek, mondjuk egy tömb elemei, ahol a tömb indexe az az indexfüggvény, ami alapján a szétszórást megteszem
        * Ezeket az edényeket a már korábban megismert rendezések valamelyikével rendezzük (akár párhuzamosan) -- ennek a műveletigénye mindenhogyan alacsony, mivel kicsik az edények
        * Ha most a már rendezett edényeket a megfelelő sorrendben összefűzöm, meg is kapom a rendezett listát
        * Az összefűzés láncolt ábrázolással olcsó művelet
        * Az edényeket pedig pl. az indexfüggvényük alapján (ami mentén összevontuk a kulcsokat) könnyen tudom eleve rendezett módon számon tartani
        * Ez nem lesz stabil rendezés
        * Az edényrendezés speciális esetei a radix-rendezések, azaz a számjegypozíciós rendezések
            * Ezeken belül még megkülönböztetünk előre- és visszairányú radixot
            * Valamint külön felbontási szempont, hogy a számjegyek binárisok-e
            * Ezt részletezzük a továbbiakban