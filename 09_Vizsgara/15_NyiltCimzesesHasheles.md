# Hashelés - Nyílt címzéses kulcsütközés-feloldás

* A hashtábla mellett ez a másik lehetőség a kulcskollízió kezelésére
* A z tömb itt is 0-tól m-1-ig van indexelve, ám elemei most nem listák, hanem kulcs--státusz-párok. A kulcsok maguk az input. Ebből következően itt m>=n, de lehet viszonylag nagy is
* A tömbelemek státusz mezője az "s" szelektorral kérhető le (z[h(k)].s) és háromféle értéke lehet:
    * ü (azaz üres)
    * f (foglalt)
    * t (törölt)
* Az első kettő magáért beszél, a törölt státusz létét az algoritmusoknál fogom megindokolni
* A kereséshez bevezetjük a próbasorozat koncepcióját
    * Minden k kulcsra potenciális próbasorozatnak hívjuk azt a sorozatot, aminek elemei minden [0..m-1]-beli i-re h(k,i) alakú függvények, úgy, hogy ezekről a függvényekről az alábbiakat tudjuk:
        * A k mindnél ugyanaz (ez az a kulcs, ami a paramétere a h hash-függvénynek)
        * A h olcsón kiszámítható, adott k-adott i párra mindig ugyanazt adja
        * h értékkészlete a [0..m-1] intervallum
        * Szóval h egy hash-függvény
        * A sorozatban minden [0..m-1]-beli érték pontosan egyszer fordul elő valamely megfelelő h(k,i) képeként
        * Másképp fogalmazva a potenciális próbasorozat egy adott k-ra, olyan hash-függvények m hosszú sorozata, amik valamilyen sorrendben elő tudják állítani [0..m-1] összes elemét (de mindet csak egyszer) -- azaz a képük [0..m-1] egy permutációja
    * Mire jó ez? Képzeljünk el két különböző kulcsot amire az eddig bevált hash-függvényünk azonos értéket ad. Az egyiket be tudjuk illeszteni a hash-függvény által jelölt üres slotba, de a másiknak már nincs helye ott. Ekkor lekérjük a próbasorozat 2. függvényét, meghívjuk a második kulccsal és amit az ad, oda próbáljuk berakni. Ha ez foglalt, akkor továbbmegyünk a következő hash-függvényre. Mivel m helyünk van, legkésőbb az m. függvény be fog válni -- különben tele van a hash-tábla, ebben az esetben eleve meg se próbáljuk a beszúrást (érdemes a hash-tábla mellett eltárolni annak kitöltöttségi állapotát)
    * Tehát az eddigi h(k)-t h(0,k)-nak fogjuk hívni és összesen m hash-függvényünk lesz
    * De ezekből jó esetben (hiszen a hash-függvények egyenletesen osztanak el) csak nagyon keveset kell ténylegesen is meghívnunk k-ra, ezért a "potenciális" név. A potenciális próbasorozat azon kezdőszeletét, amit a próbálás során ténylegesen használtunk is, tényleges próbasorozatnak hívhatjuk
    * Így a beszúrás átlagos esetben (a hash-tulajdonság miatt) Θ(1), de legrosszabb esetben Θ(n) -- igazából elég valószínű, hogy nem fogjuk mind az m függvényt meghívni, mert a hash-tábla mérete a "legspórolósabb" esetben sem nagyobb, mint a bemenet, azaz m>=n, és nyilván ha nagyon pechesek vagyunk, akkor is az eddigi elhelyezett elemekbe fogunk "csak" beletrafálni, tehát maximum az n. próbára sikerül berakni az aktuális elemet (vagy eddigre derül ki, hogy ez esélytelen)
    * A beszúrás, keresés és a törlés (aminek a része egy keresés) is ugyanazzal a próbasorozattal dolgozik, azaz, ha a beszúrás során a 3. próbára sikerül valaminek a helyét megtalálni, akkor a keresés során 3-ra fogjuk őt megtalálni
* Nevezetes próbasorozatok -- egy példán keresztül
    * Legyen az inputunk a következő kulcssorozat: [18, 28, 36, 17, 62, 52, 49, 82]
    * Használjuk hashnek az ún. osztómódszert: h'(k) := k mod 11, ez [0..10]-ig 11 db slotot, s így m=11-et határoz meg, míg n=8
    * Azért a "vessző" a h nevében, mert simán h-val most a próbasorozatot, nem az egyik elemét -- ami saját jogán is hash-függvény -- jelöljük
    * Az alábbi táblázatban láthatjuk a h'(k) hash-függvény értékeit az inputokra:

```    
    k   h'(k)
    18  7
    28  6
    36  3
    17  6
    62  7
    52  8
    49  5
    82  5
```

* Ez alapján z az első három kör után így néz ki:

```
    i                   0   1   2   3   4   5   6   7   8   9   10
    z[i]                            36          28  18
    próbasorozat hossza -   -   -   1   -   -   1   1   -   -   -
```

* Hanem a 17-nél azt tapasztaljuk, a neki való 6. slot már betelt
* Ekkor a potenciális próbasorozat 2. (1-es indexű) elemét kell használnunk
* Erre 3 nevezetes stratégiát nézünk:
    * Lineáris próba
    * Négyzetes próba
    * Kettős hasítás

## Lineáris próbasorozat

* A lineáris próbasorozat függvényei az alábbi képlet alapján jönnek ki minden [0..m-1]-beli i-re: h(k,i) := (h'(k) + i) mod m
* Ez tehát azt jelenti, hogy veszünk egy hash-függvényt (ez legyen a példabeli h'), elsőre (i=0) megpróbáljuk azzal, majd ha nem sikerül, megnézzük eggyel eltolva balra. Egészen addig folytatjuk ezt, míg meg nem találjuk a helyét
* A 17-et i=2-re sikerült beszúrnunk, azaz a próbasorozat 3 hosszú volt (ti. 0, 1, 2)

```
    i                   0   1   2   3   4   5   6   7   8   9   10
    z[i]                            36          28  18  17  62  52
    próbasorozat hossza -   -   -   1   -   -   1   1   3   3   3
```

* Azt vehetjük észre a 62-es példáján, hogy noha csak a 2. 7-es hashű elem volt, de mégis harmadikra sikerült beraknunk, mert arrafelé feltorlódtak az értékek az előző kollízió miatt. Ennél is látványosabb az 52, itt a hash elvileg "egyedi" (a példát tekintve), de mégis ezt is csak a harmadikra szúrtuk be (mert, egy korábbi kollízió itt is gondot okozott). Ezt az önmagát erősítő kellemetlen jelenséget elsődleges csomósodásnak hívják, és természetesen a nem egyenletes, egymáshoz közeli értékekkel operáló próbasorozat az oka
* A maradék két elem is a helyére kerül:

```
    i                   0   1   2   3   4   5   6   7   8   9   10
    z[i]                82          36      49  28  18  17  62  52
    próbasorozat hossza	7   -   -   1   -   1   1   1   3   3   3
```

* A 82-nél is jól látható az elsődleges csomósodás minden káros hatása, miképp a modulós hasítófüggvény szerepe is (nem indexelünk ki)
* Ez a próbamódszer egyszerűen implementálható, és ha a kulcsütközés kockázata kicsi, használható

## Négyzetes próbasorozat

* A próbasorozat elemei minden [0..m-1]-beli i-re: h(k,i) := (h'(k) + c1*i + c2*i2) mod m, feltéve, hogy c1, c2 > 0, alkalmas konstansok
* Mivel itt is eltoljuk csak az eredeti hasht (h'(k)-t), ha két kulcsra az megegyezik, az egész sorozat megegyezik itt is, tehát csomósodás történik. Ezt másodlagos csomósodásnak mondjuk, és annyival jobb az elsődlegesnél, hogy jobban eloszlik, ezért az "ártatlan" elemeket kevésbé viszi bele a rosszba
* Ugyanakkor itt nehezebb megoldani, hogy minden index egyszer és csak egyszer szerepeljen a potenciális próbasorozatban, ebben van a c konstansoknak szerepe:
    * 2 hatvány m-ekre megfelelők a c1=c2=1/2 konstansok
        * Ez egy könnyen számolható sorozatot ad: a 0. elem h'(k), utána h'(k)+1, utána h'(k)+3, ami megegyezik az előző+2-vel, utána h'(k)+6, ami megegyezik az előző+3-mal..., azaz h(k,i) i>0 esetén nem más, mint h(k,i-1)+i, különben (i=0 esetben) pedig h'(k)
        * A rekurzív definíció azért is jó, mert könnyen és hatékonyan számítható (nem kell megismételni korábbi számításokat újra és újra)
    * A konkrét példánkban m nem 2 hatvány, ellenben prím. Prímekre is jók a fenti konstansok, de csak [0..(m-1)/2]-n biztosítják az eltérő értékeket, azaz ha a kitöltöttségi arány nagyobb, mint 50%, akkor már bizonytalan a dolog!
    * Erre a példára inkább egy másik stratégiát nézzünk meg. Ez az úgynevezett alternáló négyzetes próba
        * A most következő stratégia abban az esetben használható, ha m egy 4k+3 alakú prím (k egész szám)
        * Ekkor a próbasorozat így néz ki: h(k,i) := (h'(k) +előjel(i) * i2) mod m, ahol előjel(i) legyen +1, ha i páratlan és -1 ha páros. Azaz a próbasorozat prefixe: <h'(k); h'(k)+1 modulo m; h'(k)-4 modulo m; h'(k)+9 modulo m; ...>
* Az előző példa alternáló négyzetes próbasorozattal:

```
    i                   0   1   2   3   4   5   6   7   8   9   10
    z[i]                    82  17  36      49  28  18  62  52
    próbasorozat hossza	-   3   3   1   -   1   1   1   2   2   -
```

## Kettős hash

* A stratégiánk itt az, hogy két hash-függvényt is definiálunk, a próbasorozat elemeit pedig ezekre építve ekképp határozzuk meg: h(k,i) := (h'(k) + i*h'2(k)) mod m
* Ha h'2(k) és m relatív prímek, a próbasorozat lefedi az intervallumot
* Pl.: ha m kettő hatvány, akkor legyen h'2(k) értéke mindig páratlan
* Vagy ha m prím, akkor eleve jók vagyunk -- itt most az
* Kikötés továbbá, hogy a második hash-függvény minden k-ra szigorúan pozitív értéket vegyen fel (különben beállna azonos értékre)
* Legyen most mondjuk h'2(k)  := (k mod 6) + 1
* A +1 a pozitív értékek miatt
* Elég a második hasht lustán (tehát csak akkor, amikor szükség van rá) kiszámolni (utána mondjuk érdemes lehet elcache-elni)
* Az előző órai példa befejezése kettős hash esetén:

```
    i                   0   1   2   3   4   5   6   7   8   9   10
    z[i]                    17      36  82  49  28  18  52      62
    próbasorozat hossza -   2   -   1   3   1   1   1   1   -   2
```

* A lustán kiszámolt, felhasznált második hashek:

```
    k   h'(k)   h'2(k)
    18  7
    28  6
    36  3
    17  6       6
    62  7       3
    52  8
    49  5
    82  5       5
```

* Itt az a tapasztalat, hogy a csomósodás kevésbé figyelhető meg

## Algoritmusok

### Keresés

* Térjünk vissza a kulcsot tartalmazó slot indexével, vagy ha nincs, -1-gyel
* Megállapítjuk a hash-kód alapján az ideális slotot
    * Ha az foglalt
        * Megnézzük, az adott elem van-e ott, ha igen, meg is találtuk
        * Ha nem, továbblépünk
    * Ha nem foglalt: lehet törölt vagy üres
        * Itt van jelentősége annak, hogy törléskor nem üresre, hanem egy külön, törölt állapotra állítjuk a slot státuszát
        * Ugyanis képzeljük el az alábbi esetet:

        ```
            -   a   -   -
        ```
        
        * Tegyük fel, hogy h(a) == h(b) és a lineáris próbát használjuk, ekkor nyilván b-t így szúrjuk be:
        
        ```
            -   a   b   -
        ```

        * Tegyük fel, hogy c-re is ugyanezt adja a hash-kód, c-t nem szúrjuk be, de keressük. Elmegyünk a próbasorozat mentén az első üres mezőig, s mivel nem találtuk c-t a tele mezőkben, nincs okunk kételkedni abban, hogy c nincs a táblában
        * De ha most kitöröljük a-t, majd b-t keressük, akkor valahogy számításba kell venni, hogy b bár az a helyére szánta volna az ég, mégiscsak eggyel arrébb került, de minden olyan mezőt figyelembe kell venni a kereséskor, amit b beszúrásakor is figyelembe vettünk. Ezért a törölt elemeknél nem állunk meg, hanem átugorjuk őket
        * Az "átugrás" azt jelenti, hogy vesszük a következő elemet a potenciális próbasorozatból
    * A lenti keresés függvény a kettős hash próbasorozatára vonatkozik
    * a h'2() függvényhívásra tekintsünk úgy, hogy a h'2() függvénytörzsében magában van egy elágazás, ami csak első lekéréskor számolja ki konkrétan a hasht, utána már csak a cache-elt értéket adja vissza (így a műveletigény minimális -- arra számíthatunk, hogy a függvényt gyakran hívják meg)
    * Üres slot esetén pedig nem kell tovább keresnünk

    ```
    keresés(z : E1*[m], k : K) : Z
        próbák = 0
        i = h'(k)
        vége = false
        AMÍG nem vége
            HA z[i].s = f és z[i].kulcs == k
                return i
            próbák = próbák+1
            vége = z[i].s == ü v próbák >= m
            i = i + h'2(k) mod m
        return -1
    ```

    * Másik megoldás:

    ```
    keresés(z : E1*[m], k : K) : Z
        próbák = 0
        i = h'(k)
        AMÍG nem z[i].s == ü és próbák < m
            HA z[i].s == f és z[i].kulcs == k
                return i
            KÜLÖNBEN
                i = i + h'2(k) mod m
                próbák = próbák + 1
        return -1
    ```

* Minden kör végén a "próbák" változó a következő, még nem vizsgált próbasorozatbeli indexet tartalmazza, ezért van az, hogy ha már elérjük m-et, kiléphetünk, mert az azt jelenti, hogy most az m-1-es próbasorozattal végeztünk, ami az utolsó!
* A törölt slotok átugrása úgy van benne az algoritmusban, hogy ez a feltétel se a return ágon, se a vége értékadásában nincs, tehát nem vesszük az ilyen elemeket figyelembe, ha rajtuk múlik se returnölni, se véget érni nem fog az algoritmus

### Beszúrás

* A példában most lineáris próbára valósítom meg
* Sikeres beszúrás esetén térjen vissza a beszúrás [0..-m-1]-beli indexével; kulcsütközés esetén -1-gyel; betelt tábla esetén -2-vel

```
beszúr(z : E1*[m], k : K) : Z
    próbák = 0
    i = h'(k)
    talált = false
    ide = -1
    AMÍG próbák<m ÉS nem talált ÉS z[i].s != ü
        talált = z[i].s == f ÉS z[i].kulcs == k
        HA z[i].s == t ÉS ide == -1
            ide = i
        i = i+1 mod m
        próbák = próbák + 1
    HA talált
        return -1
    KÜLÖNBEN
        HA próbák == m ÉS ide == -1
            return -2
    KÜLÖNBEN
        HA ide == -1
            ide = i
        z[ide].kulcs = k
        z[ide].s = f
        return ide
```

* Az algoritmus ciklusa megkeresi az első üres helyet, s közben számlálja a próbák számát
* Ha menet közben törölt slotot talál, elmenti az első ilyet
* Ha menet közben megtalálja a keresett elemet, ezt a tényt is elmenti
* Találat esetén -1-gyel térünk vissza; a próbasorozat kimerítése esetén (ha a próbák eléri az m-et (tekintve, hogy 0-ról indult)) -2-vel
* Különben ha volt törölt elem, oda, különben az első üresre berakjuk a k-t
* Azért jó, ha az első töröltre, hogy később a keresés gyorsabb legyen -- viszont ehhez is mindenképp el kellett mennünk az első üresig, hogy tudjuk, benne volt-e már az adott elem
* Másik megoldás:

```
fv beszúr(T : E1*[m], k : K) : Z
    i = h'(k)
    próbák = 0
    AMÍG próbák<m ÉS z[i].s == f
        HA T[i].kulcs == k
            return -1
        KÜLÖNBEN
            i = i+1 mod m
            próbák = próbák+1
    HA próbák < m
        ide = i
    KÜLÖNBEN
        return -2
    AMÍG próbák<m ÉS z[i].s != ü
        HA z[i].s == f ÉS z[i].kulcs == k
            return -1
        KÜLÖNBEN
            i = i+1 mod m
            próbák = próbák+1
    z[ide] = k
    z[ide].s = f
    return ide
```

* Itt a ciklus a foglalt helyeken megy végig, s szól ha duplikáció van
* Amikor kilép, vagy túlléptük a próbasorozat kereteit (return -2) vagy megtalálta az első "nem foglalt" slotot
* Itt azt mondjuk, legyen ez az "ide" változó értéke, hogy majd ide szúrjuk be a k-t
* De nem lépünk tovább, mert megnézzük erre az elemre is, hogy törölt-e vagy üres -- utóbbi esetben kész is vagyunk
* Szóval most az első üres elemig keresünk -- előfordulhat, hogy foglaltat találunk újra, ekkor a duplikációt nézzük, de az első törölt (vagy üres -- de akkor a második ciklusba be se lépünk) elem indexe már megvan
* A végén csak beszúrjuk ide a k-t. Ha valami gebasz lett volna, már kiléptünk volna korábban

### Törlés

* Előbb megkeressük az adott elemet (ezt már ismerjük), s ha megtaláltuk, t-re állítjuk státuszát (fizikai törlés itt nem is kell)
* A törlés tehát t-re állítja az adott elemet. Ha ezt gyakran hívjuk, a keresés és a beszúrás lassú lehet. Ezért a hashtáblát néha újra szokás rendezni: létrehozunk egy új táblát (új mérettel akár, a megfelelő kitöltöttségi hányados eléréséhez), egyesével beszúrjuk az elemeket, és kidobjuk az eredeti táblát