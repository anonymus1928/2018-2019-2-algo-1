# Beszúrás keresőfába

* Adjuk meg a sima, kiegyensúlyozatlan bináris keresőfába való beszúrás iteratív algoritmusát, ADT szinten, amennyiben a paraméterünk már egy Node (nem csak a kulcs)
* Megoldás:
	* Maga a Node is ADT szinten egy fa
	* Referencia szerint adjuk át a fát, mert lehetséges hogy üres, ekkor magát az üres fát kell átírnunk a paraméterre
	* Az algoritmus első ciklusa meghatározza azt a szülőt, ami alá kerül majd be a p. Akkor érünk a fa aljára, ha az aktuális már ÜRES. Ekkor a szülő fogja annak a szülőjét tartalmazni
	* A szülő akkor és csak akkor ÜRES, ha a fa eleve ÜRES volt, mivel a szülő kezdetben ÜRES, az aktuális pedig a t, és csak akkor lépünk be (és maradunk benn) a ciklusba, amíg az aktuális nem ÜRES. És mivel a szülő mindig az aktuális előző értékét kapja, biztosan nem kaphat ÜRES értéket, csak akkor lehet az, ha sose kapott értéket az inicializálás óta
	* A másik speciális eset, ha a p kulcsa már szerepel a fában. Ekkor nem csinálunk semmit, hiszen kereső- és nem rendezőfa - hibát is jelezhetnénk akár
	* Láncolt ábrázolásnál ezek Node3*-ok lennének a szülő elérhetősége miatt
	
	```
	beszúr(&t : BinTree, p : BinTree)
	  szülő = ÜRES
	  aktuális = t
	  AMÍG aktuális != ÜRES és aktuális.key() != p.key()
	    szülő = aktuális
	    HA p.key() < aktuális.key()
	      aktuális = aktuális.left()
	    KÜLÖNBEN
	      aktuális = aktuális.right()
	  HA aktuális != ÜRES és aktuális.key() == p.key()
	    SKIP // már benne van
	  KÜLÖNBEN
	    HA szülő == ÜRES // üres fa
	      t = p
	    KÜLÖNBEN
	      p.parent() = szülő
	      HA p.key() < szülő.key()
	        szülő.left() = p
	      KÜLÖNBEN
	        szülő.right() = p
	```

# Rákövetkezés keresőfában

* Írjuk meg az adott csúcs esetén annak az inorder bejárás szerinti rákövetkezőjét visszaadó algoritmust bináris keresőfára
* Ez (bináris keresőfára) megegyezik a rendezés szerinti rákövetkezéssel
* Iteratív algoritmust írjunk
* Magát a Node-ot adjuk át, amiről feltehetjük, hogy egy helyes, a szülő csúcsokat is számon tartó bináris keresőfa egy csúcsa
* Ha nincs rákövetkező (mert a fa legnagyobb elemén hívtuk meg az algoritmust), térjünk vissza ÜRES-sel
* Megoldás:
	* Bal ág: ha nincs jobb gyerek, megnézem a szülőmnek melyik gyereke vagyok. Amíg a jobb voltam, addig biztos nagyobb voltam nála, azaz addig nem a szülő a rákövetkező. Amint a bal gyereke voltam, biztosan nagyobb nálam a szülő, de az ő jobb részfája már nagyobb nála is, tehát a legkisebb nálam nagyobb elem ez a szülő. Ha valahol ÜRES-et találtam, ki kell lépnem, mert ekkor úgy tűnik én voltam a legjobboldalibb elem
	* Jobb ág: ha van jobb gyerekem, elmegyek felé, hiszen nagyobb, mint én. De lehet, hogy ennek még van bal részfája, amiben szintén nálam nagyobb, de nála kisebb elemek vannak. Egész pontosan a bal részfa legbaloldalibb elemét keressük (ami szélső esetben a jobb oldali gyerekem)

	```
	következő(p : BinTree) : BinTree
	  HA p.right() == ÜRES
	    s = p.parent()
	    AMÍG s != ÜRES és p == s.right()
	      p = s
	      s = p.parent()
	    return s
	  KÜLÖNBEN
	    p = p.right()
	    AMÍG p.left() != ÜRES
	      p = p.left()
	    return p
	```
