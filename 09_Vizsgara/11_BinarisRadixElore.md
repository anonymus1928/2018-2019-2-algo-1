# Radix előre bináris számokra, tömbre

## Elv

* Legyen adott egy [1..n] indexelésű a tömb, mely n darab d jegyű kulcsot tartalmaz. Ezt szeretnénk helyben rendezni
* Tekintsük sorban az elemeket a legértékesebb helyiérték szerint. Elindulunk az alsó indexektől, ahol addig örülünk, amíg alacsony értékeket (azaz 0-kat találunk), eddig mondhatjuk azt, hogy az aktuális elem jó helyen van ott, ahol van: a két edény közül a 0-sban
* Hanem amikor megtaláljuk az első 1-est, akkor arra gondolunk, az bizony nincs jó helyen. Ekkor elindulunk a magas helyiértékektől, hogy megkeressük az első tájidegen elemet, az első 0-t. Ha ez megvan, egyszerűen kicseréljük a két kulcsot, majd mindkét indexet továbbléptetjük. Az alacsonyt felfelé, a magast lefelé
* Ha kell, tovább ismételjük az eljárást, egészen addig, amíg a két index össze nem ér -- ezzel meg is határoztuk a két edény határát!
* Ezek után rekurzívan meghívhatjuk mindkét edényre a második helyiértékre az előbbi eljárást
* Ez edényen belül fog létrehozni két újabb edényt (azaz alulról és felülről elindul két indexszel, és félúton találkozik)
* Ha ezek megvannak, akkor a harmadik helyiérték szerint következik az újabb kör -- most már 4 szálon megy a buli -- majd egészen az utolsó jegyig tart a rekurzió
* A végén, ha a virtuális edényeket sorban összeolvassuk -- azaz az a tömb elemeit felolvassuk -- megkaptuk a rendezett tömböt
* Az eljárás a bemenet (n) függvényében lineáris, egész pontosan d*n-es

## Példa lejátszás

* Legyen a = [101, 011, 111, 001, 010, 100, 001]
    * Tehát n=7 és d=3
* Első kör (i==1)
    * Az első elemről (101) máris elmondható, hogy rossz utcába tévedt, ezért őt cserélni fogjuk -- alulról az elsőt szintén tekinthetjük csúnya gazdasági bevándorlónak, ezért e kettőt kíméletlenül cseréljük, majd mindkét indexszel továbblépünk (dőlttel a cserélt elemek, vastaggal az indexek állása): a = [*001*, **011**, 111, 001, 010, **100**, *101*]
    * A 011 elsőjegyileg jó, de a következő, az 111 már nem, ezért itt megállunk. A magas indexeknél szintén, a 100 rendben van, de a 010 már afféle Englishman in New York, ezért ezeket cseréljük, majd mivel a két index összeér, végeztünk is az első körrel: a = [001, 011, *010*, 001 | ***111***, 100, 101]
* Afféle invariánsként látható, hogy elöl vannak a nullával és hátul az egyessel kezdődők
* Második kör (i==2)
    * Most előbb ugyanezt eljátsszuk a második jegy szerint az első 4 elemet magába foglaló első edénnyel: a = [001, *001* | **010**, *011* | 111, 100, 101]
    * Majd a második edénnyel is (igazából e kettő cselekedet mehet párhuzamosan is): a = [001, 001 | 010, 011 | *101*, 100 | ***111*** ]
* Harmadik kör (i==3)
    * Összesen 4 edényünk van, ebből egy 1 elemes, ami nyilvánvalóan kész van
    * A maradék háromból az elsőben ugyanaz a két elem van, ami nem okoz semmilyen gondot
    * A második véletlenül épp rendben van
    * A harmadik pedig cserére szorul: A = [ | 001, 001 | 010 | 011 | *100* | *101* | | 111]
* Összesen 8 edényünk van, ezeket összeolvasva a tömb rendezett