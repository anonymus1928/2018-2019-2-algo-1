# Hashelés - Bevezetés

## Célja, elve

* Kulcsolt adatok egy tárolási módja (programozási nyelvekben: asszociatív tömb, dictionary, map)
* Célja a "CRUD" műveletek gyors elvégzése
    * Beszúrás
    * Törlés
    * Keresés
* Tehát motivációja hasonló, mint pl. az AVL-fákénak
* Jelöljük n-nel a beszúrandó adatok számát
* Jelöljük z-vel magát a hasítótáblát: ez legyen egy tömb, aminek az indextartománya [0..m-1], azaz a mérete m
* Bevezetünk egy általában h-val jelölt hash-függvénynek nevezett leképezést, ami a tárolandó adatokat képezi le egyenletesen arra a zárt intervallumra, ami a hash-tábla értelmezési tartománya ([0..m-1]), és olcsó kiszámítani (ϴ(1)-nek tekintjük)
* A kulcsok halmazát majd K-val fogom jelölni, így h szignatúrája: h : K → [0..m-1]

## Jó hash code

* Mint írtam, a fő cél a gyors kiszámíthatóság és a lehetséges különböző értékek arányos szétszórása
* Ha az adat, amit hashelünk megváltozik, a hashnek nem szabad megváltoznia (persze, ha az adat csak a kulcs, akkor nyilván ha ez változik, borul minden..., ez esetben ez programozói hiba)
* Ezen felül arra is gondolni kell -- ez majd a nyílt címzés fejezetnél lesz lényeges --, ha úgynevezett próbasorozatot építünk a hash-kódra, a [0..m-1] intervallum minden eleme egyszer és csak egyszer jöjjön ki eredményként: hiszen, ha valamelyik kimarad, oda sose kerül semmi; ha pedig többször szerepel, a második vizsgálat felesleges lesz
* Ennek konkrétumait (pl. osztómódszer és szorzómódszer) lásd előadáson
* Én egy konkrét, programozási nyelvi példát hoztam, a Java nyelv String osztályának (FQN: java.util.String) int hashCode() metódusának implementációját
    * Előbb a két adattag:
    
    ```
    private int hash = 0;
    private final char value[];
    ```
    
    * Majd a kód:

    ```
    public int hashCode() {
        int h = hash;
        if (h == 0 && value.length > 0) {
            char val[] = value;
            for (int i = 0; i < value.length; i++) {
                h = 31 * h + val[i];
            }
            hash = h;
        }
        return h;
    }
    ```

* Ez nem más, mint a félév elejéről megismert Horner-sémás megoldás a polinom helyettesítési értékének kiszámítására
* A polinom a következő: val[0]*31n-1 + val[1]*31n-2 + val[n-2]*31 + val[n-1]
* A val egy char tömb, azaz a String hashCode()-ja a karakterek ASCII-kódjaiból és egy prímszám szorzatából épül fel
* Műveletigénye n db szorzás, és n db összeadás, ahol az n a String hossza (nullkarakter nincs :) )
* De mint láthatjuk, az egész metódus eleje lekérdezi, hogy a hashCode() úgy nulla-e, hogy a string nem üres..., üres stringnek 0 a hashCode-ja, ez a polinomból is következik, meg abból a tényből is, hogy az elmentett hash adattag nullára inicializálódik. Tehát ha a hashCode egyszer már ki lett számolva, nem hívja meg újra, ezt hívjuk cache-elésnek és értelemszerűen ismételt meghívásra konstanssá teszi a műveletigényt -- ez egy nagyon fontos optimalizáció
* Persze van kivétel, mert lehetséges olyan stringet írni, aminek a hashCode-ja 0, pedig nem üres. Ezt ilyenkor újraszámolja, de mindig ugyanaz az érték fog hozzá kijönni
* Ez a String osztály immutabilitása miatt van -- azaz ha egy String típusú változó egyszer létrejött, annak az értéke (belső reprezentációja) konstans marad (a value tömb final). Ez az oka annak is, hogy a hashCode-ot el szabad cache-elni. Nem kell attól félni, hogy a string meg fog változni
* Javában pl. s1, s2 String-változók esetében az s1.concat(s2) nem az s1 belső állapotát változtatja meg azzal, hogy hozzárakja az s2-t, hanem a két paraméter érintetlenül hagyása mellett létrehoz egy új Stringet amivel visszatér
* A Javában a hashCode() egy az Object osztályban definiált metódus, azaz minden objektumnak (ti. ezek "gyárilag" mind az Object leszármazottai) van ilyen metódusa
* A metódus felüldefiniálható -- sőt felüldefiniálandó --, amennyiben az adott típus a) rendelkezik valamilyen "kulcs"-szerepű adattaggal/adattagokkal/olcsó metódussal és ebből kifolyólag ezek alapján megírt boolean equals() metódussal, b) hash-alapú tárolóban szeretnénk ilyen objektumokat tárolni
* Az equals() metódus szintén az Objectben definiált és a Javában két objektum szemantikus azonosságát hivatott ellenőrizni -- azaz pl. két hallgató akkor azonos, ha a neptun-kódjuk azonos, még akkor is, ha az adott Student osztálynak fizikailag két különböző példányáról is beszélünk
* A hashCode() pedig többek között a Java HashMap adattárolójába való beszúráskor/kereséskor segít. A HashMap gyakorlatilag a következő órán részletezett hash-tábla javás implementációja, az interfésze pedig a bevezetőben említett asszociatív tömb, azaz kulcsolt értékeket képes tárolni. Mivel az úgynevezett slotokba a hashCode() alapján osztja el az elemeket, létfontosságú, hogy a hashCode() meg legyen írva és jól legyen megírva
* Ami még fontos, hogy ha egy osztály példányai bekerültek egy HashMapbe, azon attribútumok, amikre a hashCode() épít, ne változzanak, mert nem számolja újra a hashCode()-ot, hasonlóan a Stringéhez elcache-eli azt
* A konkrét kontraktus (szerződés) az equals() és a hashCode() között a JDK szerint: ha a nem null a-ra és b-re az a.equals(b) igaz, az a.hashCode() == b.hashCode()-ra; fordítva nem elvárás (nem is feltétlen lehet), de ha a hashtábla-alapú tárolókat hatékonyan szeretnénk használni, célszerű ha az is teljesül, hogy különböző objektumok hashCode()-jai különböznek

## Direkt címzés

* Ez az alapeset. A z egy tömb, elemei pointerek az adatokra. z mérete (ami ugye m) nagyobb-egyenlő, mint az input, de egyik se túl nagy
* Ebben az esetben a tömböt valóban tömbként használjuk. 0-tól m-1-ig indexeljük, az indexek a hash-kódok
* Azaz ha egy elemet be akarunk szúrni, nem a következő szabad helyre rakjuk, hanem a hash-kódja értékére, mint indexre
* A törlés hasonló: ezen az indexen levő elemet delete-eljük és/vagy nullra állítjuk
* Keresés: return z[h(p→key)]
* Műveletigények konstansok
* Annyival jobb, mint egy tömb, hogy bár itt is az elem indexét kell tudni a konstans eléréséhez, de itt az index nem egy hasra ütés alapján eldöntött szám, hanem következik az elemből magából. Kicsit mintha a neptun-kód nem egy random karaktersorozat lenne, hanem a régi EHA-kódhoz hasonlóan a nevedből és a képzésedből generáltatott volna ki (pl. nekem SZBRAGI, ami alapján azért könnyebb keresni)
    * Itt feltétel, hogy több hely legyen, mint inputadat (m>=n) és, hogy ne legyen két különböző elemnek azonos a hash-kódja, hiszen akkor felülírná a később beszúrt a korábbit
* Erre a problémára két megoldási javaslatunk van
    * A (valódi) hash-tábla és a
    * Nyílt címzéses módszer
* Mindkettőnél a lényeg, hogy a lekérdező, beszúró és törlő műveletek minél olcsóbban, átlagosan Θ(1) műveletigénnyel menjenek. Igaz, rossz hash-kód, illetve szerencsétlen eloszlású adatok esetében ez akár Θ(n)-re is felmehet (átlagos, és maximális műveletigény)