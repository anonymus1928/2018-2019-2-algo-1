# Tömb kupaccá alakítása

* Legyen adott egy tömb: [3, 7, 12, 9, 2, 6, 19, 17, 1, 25, 13, 10]
* Ő persze lehet egy fa aritmetikai reprezentációja, ahol az első szint a [3], a második a [7, 12], a harmadik a [9, 2, 6, 19], és a negyedik a [17, 1, 25, 13, 10, NULL, NULL, NULL]
* Azaz vizuálisan így:

	```
            3
          /   \
         7      12
        / \      / \
       9  2      6  19
      / \ / \    /
     17 1 25 13  10 
	```

* Ez nyilvánvalóan nem kupac, mivel pl. a 17 nagyobb, mint a 9
* Az átalakítás a következőkből áll: vegyük alulról az első olyan szintet, ahol vannak köztes csúcsok, és menjünk ezeken a köztes csúcsokon jobbról balra végig. Nézzük meg, a bal- és jobbgyerekek közül melyik a nagyobb (már ha léteznek), és ez a nagyobb nagyobb-e mint a csúcs kulcsa. Ha nagyobb, cseréljük. Ezt tegyük meg az összes csúcsra az "utolsó előtti" sorban

	```
            3
          /   \
         7      12
        / \     / \
      17  25   10  19
     / \ / \   /
    9  1 2  13 6 
	```

* Persze ez csak egy szemléltetés, a valóságban a tömb elemeit mozgattuk: [3, 7, 12, 17, 25, 10, 19, 9, 1, 2, 13, 6]
* Most már a legalsó kis cseresznye-alakú részfák kupacok!
* Ismételjük meg a fenti műveletsort az alulról a második köztes sorra, de úgy, hogy ha egy csúcsot cseréltünk az egyik gyerekére, akkor mivel a régi csúcsot nem "mérkőztettük meg" az új csúcs gyerekeivel, ezért azon az ágon, amivel cseréltük egészen a levelekig, vagy az első nála kisebb-egyenlő elem felbukkanásáig kell cserélgetnünk. Ezt hívják süllyesztésnek. Nyilván, ha egy csúcsot a bal gyerekkel cseréltük, akkor az új csúcs már maradhat a helyén, mivel nagyobb mint a régi csúcs és nagyobb is mint a jobb gyerek (lásd algoritmus). Ebből következik az is, hogy a jobb gyerek leszármazottaival se kell összevetni senkit, csak és kizárólag a trónról letaszított volt szülő-csúcsot kell süllyeszteni, hogy a kupac sajátos féligrendezettségét létrehozzuk. Ezért fontos mindig a nagyobbik gyerekkel cserélni
* Most tegyük ezt meg a 12-es részfájával:

	```
            3
          /   \
         7     19
        / \     / \
      17  25   10  12
     / \ / \   /
    9  1 2  13 6
	```

* Itt nem volt nagy tennivaló, mert a 19-nek nem volt már gyereke
* Következik a 12-essel fémjelzett csúcs:

	```
            3
          /   \
        25     19
        / \     / \
      17  13   10  12
     / \ / \   /
    9  1 2  7 6
	```

* Látható, hogy a 7-est mindig a nagyobb gyerekkel cserélve egészen a levélszintig süllyesztettük
* Így áll most a tömb: [3, 25, 19, 17, 13, 10, 12, 9, 1, 2, 7, 6]. Láthatjuk, hogy osonnak felfelé a nagy értékek, és egyúttal lefelé a kicsik. Azt is elmondhatjuk most, hogy biztosan a tömb első három eleme közül kerül ki a maximum. Most jön az utolsó kör, az igazság pillanata - itt a süllyesztések még egy szinttel hosszabbak lehetnek, de minden cserénél (már csak egy lesz) szigorúan csak egy utat járunk be, azaz a költsége ennek log<sub>2</sub>(n), ami ráadásul a majdnem teljesség miatt egy nagyon pici konstans szorzóval tud csak rendelkezni! Egy-egy ilyen lépés nagyon hatékony tehát

	```
            25
          /   \
        17     19
        / \     / \
      9   13   10  12
     / \ / \   /
    3  1 2  7 6 
	```

* Kész a kupac: [25, 17, 19, 9, 13, 10, 12, 3, 1, 2, 7, 6]
* Megjegyzés: néhány feladat nem úgy lesz megfogalmazva, hogy "alakítsuk kupaccá a következő tömböt", hanem "készítsünk kupacot az alábbi elemekből". Ekkor a beszúrás műveletet kell majd ismételgetnünk egy üres kupacból kiindulva (erre a prioritásos sor fejezetben mutatok példát)
