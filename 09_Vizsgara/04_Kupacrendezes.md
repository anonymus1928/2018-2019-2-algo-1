# Kupacrendezés (Heapsort)

* Használjuk az előzőekben bevezetett kupac adatszerkezetet egy rendezés megvalósításához
* Az inputunk egy tömb, amire egy balra tömörített faként tudunk nézni
* Első lépésben kupaccá alakítjuk (lásd előző fejezet)
* Tudjuk, hogy a maximumelem a fa gyökere (azaz a mögöttes tömb első eleme)
* Cseréljük ki ezt az elemet a fizikailag legutolsóval, majd csináljunk úgy, hogy ez az elem nem eleme már a kupacnak - ezt elérhetjük azzal, hogy bevezetünk egy plusz változót, ami kezdetben a tömb mérete, majd lépésről lépésre csökken: ez lesz a tömbön belül a kupac része, itt én egy függőleges vonallal fogom jelölni
* Legyen az input az előző rész példája, ami kupaccá alakítás után a következőképp néz ki:

	```
            25
          /   \
        17     19
        / \     / \
      9   13   10  12
     / \ / \   /
    3  1 2  7 6 
	```
* [25, 17, 19, 9, 13, 10, 12, 3, 1, 2, 7, 6]

* Csere után:

	```
            6
          /   \
        17     19
        / \     / \
      9   13   10  12
     / \ / \
    3  1 2  7
	```
* [6, 17, 19, 9, 13, 10, 12, 3, 1, 2, 7 | 25]
* Nyilván most elrontottuk a kupactulajdonságot, süllyesszük hát le a 6-ost!

	```
            19
          /   \
        17     12
        / \     / \
      9   13   10  6
     / \ / \
    3  1 2  7
	```
* [19, 17, 12, 9, 13, 10, 6, 3, 1, 2, 7 | 25]

* A maradék kupacnak érthető okokból a 19 a maximuma, ismételjük meg a fentieket
	* Cseréljük ki az utolsó elemmel
	* Csökkentsük a tömb kupac-kapacitását
	* Süllyesszük le az új gyökeret

	```
            17
          /   \
        13     12
        / \     / \
      9   7   10  6
     / \ /
    3  1 2
	```
* [17, 13, 12, 9, 7, 10, 6, 3, 1, 2 | 19, 25]
* Itt most a 7-es nem süllyedt teljesen az aljára, mert a bal gyerek (2) kisebb volt, a jobb gyerek pedig nem létezett
* Folytassuk:

	```
            13
           /  \
          9   12
         / \  / \
        3  7 10  6
       / \
      2   1
	```
* [13, 9, 12, 3, 7, 10, 6, 2, 1, | 17, 19, 25]
* Ezt persze addig kell ismételni, amíg n-1 elemet helyre raktunk, s így a maradék elem is végül a helyén van
* Láthatjuk, szép lassan előáll a tömb suffixában a rendezett sorrend, az algoritmus tehát addig teker, amíg nem lesz egy elemű a virtuális kupac. Ez kb. n csúcs, mindegyikre van két konstans műveletünk (csere és átméretezés), illetve a süllyesztés ami logos, így tehát az összköltség kb. n * log<sub>2</sub>(n)
