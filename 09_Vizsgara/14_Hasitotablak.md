# Hashelés - Hasítótáblák

## Célja, elve

* A direkt címzéses módszer során előforduló kulcsütközés gyógyírjaként, ha vannak azonos hashű elemek; vagy a túl nagy méretű nem kihasznált tömb kiváltására ha ugyan nincsenek azonos indexre kerülő elemek, de ennek ára a túl nagy tömb
* Tehát itt jellemzően K számossága sokkal nagyobb, mint m, és akár n is lehet nagyobb, mint m
* Értsük a különbséget: n az input mérete, |K| pedig az inputelemek lehetséges varianciája
* Kulcsütközés persze m>n esetén is lehet
* A z elemei egyszerű láncolt listák (EL) lesznek. Konkrétabban E1* típusú pointerek, ahol az E1 kulcs része K típusú, a next része értelemszerűen E1* típusú
* A hashtábla elemeit rekeszeknek, angolul slotoknak mondjuk
* Kezdetben a z elemei csupa nullpointerek
* Kulcsütközésnek (collision) azt a jelenséget nevezzük, amikor két különböző elem kulcsához ugyanaz a hashkód tartozik, azaz egy slotba valók
* Beszúrásnál kulcsütközés esetén a lista elejére szúrjuk az elemet (ezáltal a műveletigény legrosszabb esetben is konstans); keresésnél és törlésnél pedig az átlagos idő a listahossz (slotméret) függvénye, ami az összes adat osztva a slotok számával, azaz: α=n/m
* A slotok listái lehetnek rendezettek is, nagyságrendileg ez sokat nem segít, de legalább a beszúrás se lesz konstans :)
* Még egy lehetőség, hogy a beszúrás is Θ(α)-s legyen, ha ellenőrizzük a konkrét kulcsduplikációt, ahogy a Java is teszi
* Tisztázzuk: a "kulcsütközés" a hashek ütközését jelenti, ez a mostani pedig a kulcsok ütközését
* Az ilyet kezelhetjük hibajelzéssel, felülírással, helyben hagyással, de akár figyelmen kívül hagyással és elé/mögé beszúrással is
* Általában elmondható, hogy a hash-függvény egyenletesen oszt el, a listák hossza (α) egyenletes, és kicsi -- már ha n nem sokkal nagyobb, mint m
* Akárhogyanis: a műveletigények Θ(α)-sok, ami tehát megfelelő n-re, m-re inkább Θ(1)-nek tekinthető
* A Javában a megfelelője a Hashtable (szinkronizált -- azaz több szálon biztonságos, de lassú; se kulcsnak se értéknek nem fogadja el a nullt) vagy a HashMap (nem szálbiztos; egy null kulcsot és bármennyi null értéket tolerál)
    * Amúgy a Hashtable régi, hatékonyabb a ConcurrentHashMap. Egy másik alternatíva a Collections.synchronizedMap(Map)
    * Konkrétan nem teljesen azt csinálja a javás HashMap, mint mi papíron:
    * Valóban egy tömb van benne (mégpedig úgy, hogy m kötelezően kettő egy hatványa)
    * A tömb elemei "Entry"-k, ami egy rekord kulccsal, értékkel, next pointerrel és cache-elt final hashsel
    * Beszúráskor viszont nem teljesen az Objectben deklarált hashCode() metódus szerint keresi meg a megfelelő indexet, hiszen arra nincs semmilyen kikötésünk, hogy m-hez képest mekkora, hanem azon meghív egy második hash() függvényt, ami már mapspecifikus, és az képezi le a megfelelő intervallumra az eredeti függvényt
    * Ha megvan a keresett slot (null kulcs esetében a nulladik indexen levő), annak a láncolt listájában megkeresi, hogy pontosan ilyen kulcsú elem van-e már. Ha van, azt felülírja, és returnöl a régi value-jával [a key marad]. Ha nincs, akkor beszúrja az új elemet a listába (kulcs--érték-pár), és returnöl nullal
    * Ezért irtó fontos az equals() és a hashCode() konzisztenciája
    * Fontos szerepe van még az általunk α-val jelölt kitöltöttségi hányadosnak (load factor)
    * Ha ez túl kicsi lesz, vagy túl nagy (erre vannak predefinit konstansok, de akár állíthatók is) akkor egy rehashing nevű eljárás történik, azaz megváltozik a belső hash() metódus és egy nagyobb méretű tömbbe költözik az egész adatmennyiség
    * Tulajdonképpen a rehashing tekinthető egy kulcsütközés-kezelésnek is, a nyílt címzéses esetben a próbasorozatok elve hasonló

## Algoritmusok

### Keresés

* Meghívjuk a hash-függvényt a keresendő elemen, indexeléssel megkeressük a listát -- addig megyünk a listán, amíg az aktuális elem nem null és a kulcsa nem egyezik a keresendő kulccsal. A végén vagy null marad a lista maradék része (azaz nem volt meg az elem), vagy megálltunk a keresendő elemnél
    * Tehát, akár a Javában: előbb leszűrjük a lehetőségeket a hashCode() alapján, majd végignézzük lineárisan a maradék jelölteket az equals()-szal
* Itt nem szükséges előre nézni -- azaz vizsgálhatjuk mindig az "aktuális" elemet, nem annak a nextjét (lásd következő struktogram, ahol pedig úgy kell). Ennek az az oka, hogy a keresett elemre vagyunk csak kíváncsiak, az őt megelőző elemmel nem akarunk semmit se csinálni
* Ha tudjuk, hogy a lista rendezett, akkor ennek megfelelően alakíthatjuk az algoritmust: nem a "nem egyenlő" feltétel fennállásáig, hanem a "kisebb" feltétel fennállásáig megyünk
* Akármelyik stratégiát is alkalmazzuk: rakhatunk bele duplikáció-ellenőrzést, bár ezt inkább a beszúrásba kéne rakni, és akkor itt már biztosan nem kell ezzel számolnunk

## Beszúrás

* A mellékelt példa figyeli a kulcsduplikációt és rendezett listát feltételez, s persze a rendezés fenntartásával szúr be
* Más megoldás a lista elejére szúrás:
    * Készítek egy node-ot. A nextje legyen a lista első eleme (akár null); a kulcsa legyen a beszúrandó elem
    * Berakom a lista pointerére ezt
    * Ez mindenképp konstans műveletigény, persze a lista nem lesz rendezett
    * Ez nem ellenőrzi a duplikációt (meg lehet oldani azzal is, akkor az egész tetejére kerül egy keresés)
* De ennyi erővel akár szúrhatunk a lista végére is
    * Először kiszámoljuk a hash-függvényt, ezzel meg is van a céllista
    * 4 eset van
        * Az adott lista üres
        * A lista elejére kell szúrnunk
        * A lista "közepére" kell szúrnunk
        * A lista legvégére kell szúrnunk
    * Ez a kód szempontjából igazából csak két eset, a lista elejére szúrás (ami üres listánál is alkalmazható), illetve a lista közepére szúrás (ami akkor is értelmezhető, ha a következő elem null, azaz a lista végén vagyunk)
    * Viszont amint kész vagy az algoritmussal, nagyon érdemes ezzel a 4 esettel tesztelni, mert ez az összevonósdi csak akkor működik, ha ügyesek vagyunk, és hát általában nem vagyunk azok elsőre :)
    * Az üres lista/lista eleje eset annyit jelent, hogy z[i]-be (ami egy pointer változója) rakjuk a beszúrandó elemet
    * A másik eset során pedig azt tudjuk, hogy z[i] létezik és kisebb v. egyenlő, mint a k, ezért most z[i] nextjéról kérdezzük le ugyanezt, és addig léptetjük p-t, amíg erre az elemre is igaz ez a feltétel. Azt mindig tudjuk, hogy p-re igaz, hiszen p az előző körös p->next (illetve az első korban a z[i])
    * Amikor megállunk, akkor azt tudjuk, hogy p az utolsó elem, ami még kisebb-egyenlő, mint k (hiszen p->next vagy null vagy nagyobb), azaz most megvizsgáljuk, hogy egyenlő-e k-val (az pedig világos, hogy egyáltalán létezik). Ha az, akkor duplikáció volt (max. egy ilyen elem lesz, mert a rendszer invariánsa, hogy nincs duplikáció: azt már a beszúrás függvény korábbi hívása megtalálta volna!)
    * Ha pedig p kulcsa nem k, akkor mivel ő az utolsó, aki még kisebb, ezért az ő nextjére kell rakni k-t, és az új elem kulcsa pedig az eddigi p->next lesz (ami lehet null ~ lista végére szúrás)
    * Legyen az input a k kulcs (lehet olyan feladat is, hogy már az E1* az input, ez esetben nem kell new-zni azt); térjünk vissza a beszúrt slot indexével (azaz a hash-kóddal) vagy -1-gyel, ha duplikáció volt. Azt feltételezhetjük, hogy a hash-kód egy valid indexet ad

```
beszúr(z : E1*[m], k : K) : Z
    i = h(k)
    HA z[i] == null VAGY z[i]->key > k
        p = z[i]
        z[i] = new E1
        z[i]->key = k
        z[i]->next = p
    KÜLÖNBEN
        p = z[i]
        AMÍG p->next != null ÉS p->next->key <= k
            p = p->next
        HA p->key == k
            return -1
        KÜLÖNBEN
            r = p->next
            p->next = new E1
            p->next->key = k
            p->next->next = r
    return i
```

* Annak, hogy egy pointeren egy szelektort hívjunk (-> operátor), előfeltétele, hogy a pointer ne legyen null. Amikor azt mondom: "p == null VAGY valami", akkor a "valami" ágat csak az első feltétel nem teljesülésekor kell nézni (rövidzár, lusta kiértékelés), azaz ez egy őrfeltétel ehhez! Hasonlóan a "p != null ÉS valami" is így működik. Fontos ezért a vagyolt v. éselt kifejezésben a tagok sorrendje

* Törlés
    * (Az algoritmus házi feladat)
    * Az elve: meghívja a keresést, ha megtalálta az adott elemet, kimenti egy pointerrel, az előzőjének (ezért kell itt előrenézéssel keresni) a nextjét az ő nextjére állítja (ami persze akár null is lehet, de ez nem baj), majd delete-eli a kimentett elemet. A sorrend fontos, különben NullPointerException történhet vagy memóriaszivárgás (elvesztesz egy a heapen levő elemet úgy, hogy nincs rá pointer, de ő még létezik -- nem delete-elted)