# Radix Vissza

## Feladat

* Ugyanaz, mint az előző esetben

## Megoldás

* Az elv hasonló, de a kulcshierarchiában hátulról haladunk előre
* Itt nem fogunk újabb és újabb edényeket létrehozni, hanem ugyanazt a listát "rendezzük" újra és újra, míg a végén rendezett lesz (természetesen balról jobbra lexikografikusan)
* Ez egy "előrendezéses módszer"

## Példa

* Az input legyen ugyanaz, mint az előbb: <bac, cac, acb, cca, aab, bab, cab, cbc, caa, aac>
* A kulcsokat először a hátsó, majd sorban az egyre baloldalibb helyiérték szerint vizsgáljuk, de persze a lista elemein most is balról jobbra megyünk végig
    * Az első edények a harmadik betű alapján
        * a edény: <cca, caa>
        * b edény: <acb, aab, bab, cab>
        * c edény: <bac, cac, cbc, aac>
* Az edényeket nem hozzuk létre "fizikailag", csak az elemeket láncoljuk okosan. A listánk az edények összefűzése után így néz ki: <cca, caa, acb, aab, bab, cab, bac, cac, cbc, aac>
    * Most a második helyiérték szerint megyünk végig az átrendezett listán és soroljuk újra edényekbe tartalmát:
        * a edény: <caa, aab, bab, cab, bac, cac, aac>
        * b edény: <cbc>
        * c edény: <cca, acb>
* Azaz a virtuális edények alapján a lista most így néz ki: <caa, aab, bab, cab, bac, cac, aac, cbc, cca, acb>
    * Harmadik -- utolsó -- kör, az első jegy alapján:
        * a edény: <aab, aac, acb>
        * b edény: <bab, bac>
        * c edény: <caa, cab, cac, cbc, cca>
* Ezt összeolvasva elő is áll a megoldás: <aab, aac, acb, bab, bac, caa, cab, cac, cbc, cca>

## Algoritmus

* Érdemes láncolt listára megvalósítani, hiszen nem tudjuk előre a vödrök méretét, illetve ezeket minden kör végén össze is kell fésülni
* Teljes neve: "Radix edényrendezés a kulcshierarchiában jobbról balra haladva, egyszerű láncolt listára"
* Tegyük fel, hogy az alábbi függvények adottak és gyorsan kiszámíthatók:
    * edényszám(i : N) : N -- visszatér az i. helyiértékre fazonírozott edények számával
    * edénysorszám(k : T, i : N) : N -- visszatér azzal, hogy a k kulcsot az ő i. helyiértéke alapján hányadik edénybe kéne sorolni (itt egyébként nagyvonalúan néha a "hányadik" és a "melyik" szavakat felcserélve használjuk értelemszerűen, ami akkor nem teljesen pontos, ha pl. az edények betűkkel vannak "indexelve")
    * edényeketKészít(m : N, edényfejek : E1*[], edényvégek : E1*[]) -- m méretű NULL-tartalmú tömböket csinál
    * edényeketTöröl(edényfejek : E1*[], edényvégek : E1*[]) -- törli a tömböket
* Mit csinálunk?
    * Végigmegyünk a számjegyeken hátulról előre, meghatározzuk az edényszámot, végigmegyünk a kulcsokon, majd befűzzük a megfelelő helyre
    * Utána az edényeket összefűzzük (és megszüntetjük)
    * Mennyi lesz így a műveletigény? Könnyen lehetne négyzetes, hacsak nem tartjuk számon az edényvégeket is (nem kell az edényeken mindig végigmenni a végéhez fűzéshez -- mert az fontos hogy mindig a végéhez fűzzük, különben bukna a félig rendezettség)
    * d-vel jelöljük a kulcs hosszát (számjegyek számát)

```
radixVissza(&l : E1*, d : N)
  FOR i = d down to 1
    m = edényszám(i)
    edényeketKészít(m, edényfejek, edényvégek)
    p = l
    AMÍG p != NULL
      q = p
      p = p->next
      j = edénysorszám(q->key, i)
      befűz(q, j, edényfejek, edényvégek)
    összefűz(l, edényfejek, edényvégek)
    edényeketTöröl(edényfejek, edényvégek)
```

```
befűz(q : E1*, j : N, edényfejek : E1*[m], edényvégek : E1*[m])
  HA edényfejek[j] == NULL
    edényfejek[j] = q
  KÜLÖNBEN
    edényvégek[j] -> next = q
  edényvégek[j] = q
  q->next = NULL
```

```
összefűz(&l : E1*, edényfejek : E1*[m], edényvégek : E1*[m])
  l = NULL
  FOR i = m down to 1
    HA edényfejek[i] != NULL
      edényvégek[i] -> next = l
      l = edényfejek[i]
```

* Feltételezhetjük, hogy az inputhoz képest d és minden i eleme [1..d]-re az i. edényszám is viszonylag kicsi
* Az edényszám() függvény hívása Θ(d), hiszen d-szer hívjuk meg
* Az edényeketKészít() függvény Θ(d*m)-es, mivel d-szer hívjuk meg és m edényt készít
* Ezek tehát az inputhoz (méretét jelölje n) képest elhanyagolhatók!
* A belső ciklus utasításai összesen Θ(d*n) nagyságrendben futnak le -- a befűz() művelet sem rontja ezt le, mivel az edényvég segédtömb segítségével konstans műveletigényűvé tettük!
* Az összefűz() hívása Θ(d*m)-es, hiszen még az is a d-s ciklusban van, önmaga pedig egy m-es ciklus
* Az edényeketTöröl() pedig m edényt töröl összesen d-szer, tehát ez is Θ(d*m) komplexitású
* Összességében mivel d és m is kicsi n-hez képest a teljes algoritmus a Θ(n) műveletigény-osztályba tartozik