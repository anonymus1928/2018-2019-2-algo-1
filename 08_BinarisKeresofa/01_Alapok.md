# Bináris kereső- és rendezőfák

* Olyan bináris fák, amiknek minden csúcsára igaz: a bal részfájában kisebb kulcsúak vannak, a jobb részfájában nagyobbak
    * Vigyázat, az, hogy a bal gyereke kisebb kulcsú, a jobb nagyobb, nem elégséges definíció, ugyanis képzeljük el ezt a fát:
        ```
             3
            / \
           2   5
              / \
             1   6
        ```
    * Itt minden csúcsra igaz, hogy a közvetlen gyerekei nagyobbak, ill. kisebbek nála a megfelelő módon, de az 1-es, ami a 3-as jobb részfájában van, mégis kisebb nála
* Rendezőfa: lehet egyenlőség
* Keresőfa: nem lehet egyenlőség
    * Egyfajta halmazmegvalósítás (pl. a TreeSet a Javában)
    * Onnan könnyű megjegyezni, hogy míg rendezni lehet nem szigorúan monoton listát, de egyértelműen kulcsra keresni csak egyértelmű elemek esetén lehet

* Műveletek:
	* insert(x : T), insert(p : BinTree) - érték v. csúcs
	* search(x : T) : L (esetleg visszatérhetünk a csúccsal is)
	* delete(x : T)
	* min() : T - legbaloldalibb levél
	* max() : T - legjobboldalibb levél
	* next(p : BinTree) : BinTree - rákövetkezés rendezés, azaz inorder bejárás szerint (maxra ÜRES)
	* prev(p : BinTree) : BinTree - megelőző elem rendezés, azaz inorder bejárás szerint (minre ÜRES)

* Építsük fel a rendezőfát ebből: 10, 5, 20, 9, 25, 17, 3, 8, 15, 22
    * Egyszerűen ahogy jönnek az inputok, balra vagy jobbra pakoljuk az aktuálisan feldolgozott elemhez képest, illetve rekurzívan az összes eddigi elemhez képest balra vagy jobbra "lépünk" az első "üres helyig" menve
    * Ha most jönne egy újabb 10-es pl. és rendezőfáról beszélnénk, mindegy lenne, hogy balra vagy jobbra rakom
* Az inorder kiírással lehet a rendezett sort kiíratni (hiszen előbb a bal részfa, majd a gyökér, majd a jobb részfa következik, ami a definícióból adódóan a rendezésnek felel meg)
