# Hanoi tornyai

## Alapprobléma

* Adott 3 rúd, és n korong. A korongok mind különböző méretűek és az első rúdon helyezkednek el egymáson úgy, hogy a legnagyobb van legalul, és a torony minden emelete eggyel keskenyebb, mint az alatta levő. Azaz egyre feljebb haladva egyre kisebb korongok vannak
* A feladat, hogy átkerüljön az első rúdról a másodikra az n darab korong ugyanilyen sorrendben úgy, hogy a korongok átrakodása során a következő szabályokat kell betartanunk:
	* Egyszerre egy korongot helyezhetünk át, mindig a rúdon található legfelsőt
	* Egy korong vagy üres rúdra kerülhet, vagy egy olyan rúd tornyának tetejére, ahol az addigi tető korongja nagyobb, mint az áthelyezés alatt álló korong
	* Azaz "az egyre magasabb helyen egyre kisebb korong van" szabályt meg kell tartani. Ez egy invariáns
* Példa n=3-ra (vigyázat, a rudak száma mindig 3, a korongoké ettől függetlenül 3 ebben a konkrét példában)
	* Alapállapot:
		* Első rúd: <3,2,1>
		* Második rúd: <>
		* Harmadik rúd: <>
	* Célállapot:
		* Első rúd: <>
		* Második rúd: <3,2,1>
		* Harmadik rúd: <>
    * Balról jobbra = alulról felfelé; nagyobb szám = vastagabb korong

## A megoldás ötlete	

* Tegyük fel, hogy meg tudjuk oldani, hogy átkerüljön a felső n-1 korong az első rúdról bármelyikre az invariáns megtartásával. Ekkor pl. a harmadikra is át tudjuk tenni
* Ezután egy lépésben áttehetjük az első rúdról a legalsó korongot a másodikra, hiszen ekkor ez a korong a "tető", az egyetlen korong, a második pedig üres
* Majd mivel feltettük, hogy n-1 korongot át tudunk tenni, csak annyi a feladat, hogy áttegyük mind az n-1 korongot a 3-ról a 2-ra, amit megtehetünk, mert a 2-on a globális maximum van
* Persze, ha n-be behelyettesítjük az n-1-et, akkor hasonló elven kijön, hogy igazából n-2-re is meg tudjuk ezt oldani, stb.
* Azaz ez egy rekurzív eljárás. 3 lépésből áll, ahol a középső elemi, a másik kettő pedig egy rekurzív hívás, épp csak a paraméterezése tér el
* Nyilván n=1-re visszakapjuk magát a sima elemi átrakás műveletet, ezért a rekurzióból lesz kiszállás
* Példa n=3-ra:
	* Alapállapot: <3,2,1>, <>, <>
	* Rekurzió (n=2). Próbáljuk meg a felső kettőt félrerakni a harmadikra
	* Tehát ez az alfeladatunk: <2,1>, <>, <>; ami az eredeti feladat eggyel kisebb n-re, és kicsit más szereposztással a rudak között... ami miatt ez is rekurzívan oldandó meg, pakoljunk át n=1 elemet az első rúdról a másodikra. Azért a másodikra, mert amíg az eredeti feladatban a 2. rúd volt a cél és a 3. a segéd, itt most a 3. a cél, tehát a 2. a segéd
	* Ez nyilvánvalóan ennyi: <2>, <1>, <>; ami után könnyedén átrakható az alsó korong: <>, <1>, <2>; majd a befejező lépés egy újabb rekurzív hívás: rakjunk át n=1 korongot a segédrúdról (2.) a célrúdra (3.): <>, <>, <2,1>
	* Ez a fenti volt az n=3 eset első lépése csak az n-1=2 darab felső korongot nézve. Azaz a teljes feladat most így áll: <3>, <>, <2,1>
	* Most jöhet az elemi lépés: <>, <3>, <2,1>
	* Majd az újabb rekurzív hívás n=2-re és forrás: 3., cél: 2., segéd 1. szereposztással, amit itt nem részletezek, az eredmény ez: <>, <3,2,1>, <>

## Algoritmus	

* A fenti gondolatmenetet öntjük struktogramba (vagyis itt pszeudokódba)
* A rekurzív algoritmusoknál szokás, hogy a rekurzió kilépési feltétele a lehető legegyszerűbben megvalósítható eset legyen, amiről hihetnénk, hogy az n=1, de nem, mert még ennél is egyszerűbb a tök üres első rúdról átpakolni a korongokat a második rúdra, azaz az n=0 esetet nézni. Ennek a megoldása nemes egyszerűséggel, hogy nem csinálunk semmit
    * Kifejezetten tanácsos mindig a létező legmélyebbre leengedni a rekurziót, mert így elkerülhető a kódismétlés, aminek számtalan előnye van (clean code)

```
hanoiTornyai(n : N)
    hanoiTornyaiRekurzív(n, 1, 2, 3)
```

```
hanoiTornyaiRekurzív(n : N, i : N, j : N, k : N)
	HA n == 0
		SKIP
	KÜLÖNBEN
		hanoiTornyaiRekurzív(n-1, i, k, j)
		átrak(i, j)
		hanoiTornyaiRekurzív(n-1, k, j, i)
```

* A rekurzív algoritmusban n a korongok száma; i, j, k a három rúd. i az eredeti feladat "innen" rúdja, j az "ide" rúd, míg k a "segéd"
* Mivel i, j, k csak a rekurzióban levő haladáshoz kellett, készítettünk egy a láncreakció elindításáért felelős, egy soros függvényt, ami inicializálja a segédváltozókat
* A rekurzív algoritmus előbb megvizsgálja, a rekurzió alján vagyunk-e. Ha igen, akkor ez a rész kész is van, ha nem, akkor a korábban ismertetett elvek alapján félreteszi a felső n-1 elemet, átrakja az "innen" rúdról az "ide" rúdra a legalsó elemet, majd a tartalékrúdról visszahelyezi a célrúdra a korongokat
* Azért kellett az i, j, k változókat bevezetni, mert nem minden rekurzív szinten kell épp az 1-esről a 2-esre átpakolni, hanem a szerepek változnak

## Műveletigény

* A domináns művelet "az egy korong átpakolása egyik rúdról a másikra". Ennek számosságát szeretnénk meghatározni
* Ha nem látjuk elsőre a formulát, kezdjük el kis n-ekre lejátszani és/vagy az algoritmus alapján kitalálni a lépések számát:
	* átrakás<sub>hanoiTornyaiRekurzív</sub>(n) = ...
		* n=0 --> 0
		* n=1 --> 1
		* n=2 --> 3
		* n=3 --> 7
		* n=4 --> 15
		* stb.
	* Ami:
		* n=0 --> 0
		* n=1 --> 2*0 + 1
		* n=2 --> 2*1 + 1
		* n=3 --> 2*3 + 1
		* n=4 --> 2*7 + 1
		* stb.
	* Azaz "kétszer az átrakások száma eggyel kisebb n-re + 1", ami az algoritmus ismeretében nem meglepő
	* Nyilván ha rekurzív az algoritmus, egy rekurzív függvény adja meg a műveletigényt is
	* Felírható esetszétválasztásos függvényként: átrakás<sub>hanoiTornyaiRekurzív</sub>(n) = 0, ha n=0; 2*átrakás<sub>hanoiTornyaiRekurzív</sub>(n-1) + 1, különben
	* Ha zárt alakban szeretnénk felírni (márpedig később, amikor ezeket a függvényeket osztályokba akarjuk sorolni, úgy fogjuk akarni), megint csak azt tudom javasolni, hogy az alacsony n-ekre vett példákkal próbáljunk megsejteni valamilyen összefüggést
	* Gyakorlatilag majdnem duplázódnak az értékek, csak egyet mindig kivonunk...
	* Azaz átrakás<sub>hanoiTornyaiRekurzív</sub>(n) = 2<sup>n</sup>-1 (még 0-ra is; annál kisebbre pedig nem értelmes a dolog)
	* Persze ez csak egy tipp, bizonyítsuk be. Az esetszétválasztásos, rekurzív definícióból kiindulva természetesen teljes indukcióval bizonyítható:
		* átrakás<sub>hanoiTornyaiRekurzív</sub>(0) = tudjuk, hogy 0; a képlettel 2<sup>0</sup> - 1 = 0. Stimmel
		* Most tegyük fel, hogy egy tetszőleges n természetes számra már beláttuk, hogy átrakás<sub>hanoiTornyaiRekurzív</sub>(n) = 2<sup>n</sup> - 1
		* Azt kéne belátni, hogy átrakás<sub>hanoiTornyaiRekurzív</sub>(n+1) = 2<sup>n+1</sup>-1
		* Na de azt tudjuk a rekurzív definícióból, hogy átrakás<sub>hanoiTornyaiRekurzív</sub>(n+1) = 2*átrakás<sub>hanoiTornyaiRekurzív</sub>(n)+1 (ez az ág lesz érvényes, mert n+1 biztos nagyobb, mint 0)
		* Helyettesítsük be a feltételt: átrakás<sub>hanoiTornyaiRekurzív</sub>(n+1) = 2( 2<sup>n</sup> - 1 )+1 = 2*2<sup>n</sup> - 2 + 1 = 2<sup>n+1</sup> - 1, és ezt kellett belátni
	* Nyilván mátrakás<sub>hanoiTornyaiRekurzív</sub>(n) = Mátrakás<sub>hanoiTornyaiRekurzív</sub>(n) = Aátrakás<sub>hanoiTornyaiRekurzív</sub>(n) = átrakás<sub>hanoiTornyaiRekurzív</sub>(n), mivel itt csak az n az input és determinisztikus az algoritmus, tehát minden n-re egy féle futás lehetséges (általában "n" nem az egyetlen tényező, amitől számít az input, hanem az csak a mérete annak)
