# Adatszerkezetek

* Olyan adattípus, ami valamilyen belső reprezentációval más, egyszerűbb (nem feltétlen elemi) típusú elemeket tartalmaz, azokra épít
    * Ez általában hivatkozással vagy tartalmazással valósul meg (lásd asszociáció, aggregáció és kompozíció)
* A kontraszt miatt: a nem "adatszerkezet" típusok az ún. primitív vagy elemi típusok a következők:
    * Számok (N; Z; ...)
    * Igaz-hamis, azaz logikai értékek (L)
    * Bármilyen véges értékhalmaz elemei ({0,1,2,4}; {párduc, oroszlán, gorilla}; karakterek - Ch; ...)
    * Pointerek
    * A String és a tömb nem, ezek már adatszerkezetek!
* Sokszor elvárjuk, hogy a tartalmazott adatok azonos típusúak legyenek (iterált adatszerkezet)
    * Ez technikailag a típusos polimorfizmus miatt nem nagy megkötés
* Lineáris adatszerkezetnek hívjuk, ha a benne levő elemek valamiféle egyértelmű sorrendben vannak (nem feltétlen rendezett sorrendben, de sorrendben)
* Rendezhetőnek, ha a benne levő elemek között van rendezési reláció
* Rendezettnek, ha rendezhető, lineáris, és a természetes bejárási sorrendje rendezett
* Példák:
    * Az [1,2,3] tömb iterált, lineáris, rendezhető és rendezett
	* Az [1,3,2] tömb iterált, lineáris, rendezhető, de nem rendezett
	* Az {1, 2, elefánt} halmaz nem iterált, mert nem minden elem egyforma fajta, nem is lineáris, mert a {2, elefánt, 1} halmaz is ugyanaz a halmaz, és ezekből következően nem is rendezhető, s nem is rendezett
* Rekurzív adatszerkezetről beszélünk, ha az adatszerkezet önhivatkozó, a saját típusának megfelelő adattagja van
    * Például egy fa egy csúcseleme ilyen, ha hivatkozik a szülőjére és/vagy leszármazottjára
    * A fa megint csak nem lehet lineáris, habár iterált lehet
    * Sőt, rendezhető is lehet, ha pl. az elemek címkéi számok, amik felett van rendezési reláció
    * De a hagyományos értelemben nem lehet rendezett, noha a reprezentációjában lehet, hogy rendezett tömböt használunk és az is lehet, hogy egy rendezőalgoritmus megvalósításához használjuk
* Egyéb nevek: gyűjtemények, kollekciók, sokaságok, stb.
* Például: tömbök, listák, halmazok, fák, gráfok, mátrixok, vermek, sorok, elsőbbségi sorok, stb.
* Az adatszerkezet a rajta értelmezett elemi műveleteivel együtt teljes
    * Ezek algoritmusok, amivel az adatszerkezeten lehet manipulálni, bejárni, keresni, beszúrni, törölni, rendezni, indexelni, stb.
    * Sokszor igen egyszerűek
    * Nem minden adatszerkezet támogat minden műveletet, illetve komoly hatékonysági különbségek lehetnek, hiszen minden szerkezet másra és másra van kitalálva, optimalizálva
    * Tehát a "művelet" egy adatszerkezeten (illetve konkrétan annak egy elemén - this vagy self referencia) értelmezett algoritmus. Szoktuk még metódusnak, angolul methodnak hívni
    * A C++ nyelvben az "operator"-ok, azaz a műveleti jelek is ilyen műveletek, de pl az std::vector esetében a push_back() is egy művelet a mi értelmezésünkben
* Kb. az az adatszerkezet, amit C++-ban a class kulcsszóval vezetnénk be
    * De ez nem alapszabály, hiszen a félév során más tárgyból tanult Enor nem igazán adatszerkezet (bár adattagjai és műveletei is vannak), hanem egy algoritmus megadása OOP-eszközökkel, egy tevékenységobjektum
    * A C++ std::vector\<int\>-je már adatszerkezet (maga a vector típusparaméter nélkül csak egy sablontípus)
        * std::vector\<int\> - dinamikus helyfoglalású tömb (gyakorlatilag tömbös ábrázolású láncolt lista) vs. int[] - statikus helyfoglalású tömb
        * A kacsacsőrök közé írt típus az ún. generikus típusparaméter, a generikus programozás egy technikai eszköz lineáris adatszerkezetek megvalósításához
* A programozási gyakorlatban igen ritkán kell nevezetes adatszerkezeteket megírni, hiszen rengeteg generikus, már kész megoldás van ezekre, viszont jó tudni, hogy melyiket hogyan írták meg, melyik mire is való, mik a gyengeségeik és az erényeik - erről szól ez a két féléves tárgy
* Fontos elv az enkapszuláció: az adattagokat, azaz a belső reprezentációt szeretjük a felhasználó (aki többnyire egy kolléga, aki a Te adatszerkezetedet használja egy algoritmusban) elől elrejteni, hogy a használat helyén ne építsen, ne építhessen ezekre. Ennek a haszna kettős
    * Ha csak a publikus metódusok (az interfész) mentén lehet használni egy adatot, a pontos implementáció cserélhetővé válik, azaz nem lesz bedrótozva a kódba egy konkrét adattípus, csökken a függőség az egyes kódrészletek között, ami nagyobb projekteknél elemi igény a párhuzamos fejlesztés miatt
    * Amiről nem tudunk, azzal nem is kell foglalkoznunk, így mindig csak az adott problémával kell szembe néznünk. Ha az adatszerkezet kiadná a reprezentációját, voltaképpen nem lenne értelme az OOP-nek, ugyanolyan "minden mindennel összefügg" alakú kódunk lenne, mintha az egészet egy main() függvényben írtuk volna meg. A felelősségi körök mentén való kisebb részekre bontás viszont segíti az átláthatóságot
