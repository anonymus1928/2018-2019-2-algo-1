# Lépésszámmal kapcsolatos jelölések

* mT<sub>S</sub>(n) - Minimális, legjobb eset. A T-vel jelölt domináns művelet lefutási száma az S programban az n méretű inputok közül azon konkrét (egyik) bemenet esetén, ahol ez a szám a lehető legkisebb
	* Viszonylag nem fontos, mert nem a legjobb eset szokta a kihívást okozni
* MT<sub>S</sub>(n) - Maximális, legrosszabb eset. Az összes n méretű inputok közül annak a lépésszáma, ahol ez a lépésszám a legnagyobb
    * Fontos, ha a program felé vannak olyan elvárások, hogy a legzordabb körülmények között is hozzon egy elvárt gyorsaságot
* AT<sub>S</sub>(n) - Átlagos eset. Elvileg az összes lehetséges n méretű inputra adott lépésszámok átlagaként számítandó
    * Fontos, ha a program sokszor fut le, és arra vagyunk kíváncsiak, nagy vonalakban hozza-e az elvárt teljesítményt (még ha meg is engedünk egy-egy kiugró értéket)
    * Figyelem: ez nem mT<sub>S</sub>(n) és MT<sub>S</sub>(n) átlaga, hiszen lehet hogy a program 10 inputból 9-re semmit sem csinál, egyre pedig 10 percig fut. Ekkor AT<sub>S</sub>(n) nem 5 percnek megfelelő érték lesz
* T<sub>S</sub>(n) - Ha m és M azonos, akkor abból következően A is azonos. Ilyenkor el szoktuk hagyni a prefixet és csak szimplán így jelöljük

* A "T" (domináns művelet), az "S" (a vizsgálat alatt álló algoritmus) persze általában konkrét esetekkel helyettesítendő. Pl. a buborékredezés összes cseréjét jelölhetjük így: swap<sub>bubbleSort</sub>(n)
* Ha "T"-t írok domináns műveletnek az vagy azt jelenti, hogy általánosan fogalmazok, konkrét domináns művelet nélkül, vagy azt, hogy az összes alprogram-hívást és ciklusmag-lefutást tekintem domináns műveletnek
