# Infix forma átalakítása lengyelformára

* Nézzünk előbb két egyszerűbb példát, majd az algoritmust, végül egy összetettebb levezetést
* a + b * c - d
	* Vermet használunk, ha operandust látok csak leírom (hiszen azok sorrendje nem változik), ha operátort, akkor berakom a verembe
		* a-t leírom
		* \+ a verembe
		* b-t leírom
		* \* a verembe
		* c-t leírom
		* \- esetében mivel a veremtető nagyobb precedenciájú, ezért kiveszem azt s leírom; de még mindig van valami, ami ugyan "csak" azonos precedenciájú, de mivel balról jobbra köt, ezért ezt is kiveszem, leírom, majd mivel a verem már üres, berakhatom a mínusz jelet
		* d-t leírom
		* Mivel elfogyott a bemenet, mindent ami a veremben van, kiírom (-)
	* Eredmény: a b c * + d -
* Tehát megtanulhattuk azt, hogy az operandust leírom, a műveletet berakom, de előtte minden nála nagyobb, illetve balra kötő műveletek esetén minden vele azonos precedenciájú műveleti jelet is kiírok (értelemszerűen, ha nem üres a verem; ha üres, csak berakom)
* A következő példában zárójel is van: a * ( b + c / d ) ^ 2
	* a-t leírom
	* \* verembe
	* ( verembe - tekinthetünk rá úgy, mint egy "verem a veremben" megnyitására, vagy úgy, mint egy operátorra, ami mindenkinél erősebb
	* b-t leírom
	* \+ verembe, a nyitó zárójel "eltakarja" az előző műveleti jelet, ilyenkor úgy tekintsünk rá, mintha üres verembe raknánk az aktuális műveleti jelet (verem a veremben), vagy ha midenkinél gyengébb operátor lenne
	* c-t leírom
	* / verembe, mert nagyobb a precedenciája a veremtetőnél (+)
	* d-t leírom
	* ) triggereli a verem ürítését és kiírását egészen (-ig, mindenféle precedenciák és egyebek figyelembe vétele nélkül. A nyitó zárójel is kikerül, de azt nem írjuk ki (hát persze, hiszen erről szól a lengyelforma) és a csukó zárójel se kerül be, csak eldobjuk
	* ^ verembe, mivel erősebb, mint a veremtető, ami most a szorzás (de ez a jobbasszociativitás miatt akkor is bekerült volna előtte való kiírás nélkül, ha hatványjel van a verem tetején)
	* 2-t leírom
	* vége, elfogyott a bemenet, kikerül sorban ami a veremben van: a hatvány- majd a szorzás jel
	* Eredmény: a b c d / + 2 ^ *
* Az algoritmus előfeltétele a helyes (de nem feltétlen teljes) zárójelezettség, valamint a már a zárójeles résznél emlegetett "prec" függvény megléte
	
```
lengyelformáraHozás(in : InStream, &out : OutStream)
	v = Stack()
	ch = in.read()
	AMÍG ch != EOF
		HA ch == '('
			v.push(ch)
		KÜLÖNBEN HA ch eleme OPERANDUSOK
			out.print(ch)
		KÜLÖNBEN HA ch == ')'
			AMÍG v.top() != '('
				out.print(v.pop())
			v.pop()
		KÜLÖNBEN HA ch eleme OPERÁTOROK
			HA ch eleme BALASSZOCIATÍV
				AMÍG !v.isEmpty() és v.top() != '(' és prec(v.top()) >= prec(ch)
					out.print(v.pop())
				v.push(ch)
			KÜLÖNBEN HA ch eleme JOBBASSZOCIATÍV
				AMÍG !v.isEmpty() és v.top() != '(' és prec(v.top()) > prec(ch)
					out.print(v.pop())
				v.push(ch)
		in.read(ch)
	AMÍG !v.isEmpty()
		out.print(v.pop())
```

* A csukó zárójel esetében, egyrészt nyugodtan ellenőrizhetjük a v.top()-ot az üresség ellenőrzése nélkül, hiszen feltételezve a helyes inputot, kellett már beolvasott nyitó zárójelnek lennie, tehát a verem biztos nem üres. Az a plusz veremből-művelethívás pedig a nyitó zárójel eldobása
* A műveletek esetében a két belső ciklus között a nagyobb jel élessége a különbség: azt írjuk ki hamarabb, amelyiket hamarabb kell elvégezni, tehát jobbasszociatív esetben a jobb oldalit, ezért a már bennlevő baloldali jelet nem dobjuk ki ilyenkor, viszont mindenkit, aki nála erősebb, igen


## Példa a lengyelformára hozó algoritmus lejátszásra

* Legyen az input ez: y := ( b - 2 * h ^ 2 ^ 3 / e ) / j * a + 2 - 1 / w
* Olvassuk be a bemenetet karakterenként, alakítsuk lengyelformára, minden lépésben mutassuk az output és a verem tartalmát is
	* Beolvassuk: y
		* v = [
		* out = y
	* Beolvassuk: :=
		* v = [ :=
		* out = y
	* Beolvassuk: (
		* v = [ := (
		* out = y
	* Beolvassuk: b
		* v = [ := (
		* out = y b
	* Beolvassuk: -
		* v = [ := ( -
		* out = y b
	* Beolvassuk: 2
		* v = [ := ( -
		* out = y b	2
	* Beolvassuk: *
		* v = [ := ( - *
		* out = y b	2
	* Beolvassuk: h
		* v = [ := ( - *
		* out = y b	2 h
	* Beolvassuk: ^
		* v = [ := ( - * ^
		* out = y b	2 h
	* Beolvassuk: 2
		* v = [ := ( - * ^
		* out = y b	2 h 2
	* Beolvassuk: ^
		* v = [ := ( - * ^ ^
		* out = y b	2 h 2
	* Beolvassuk: 3
		* v = [ := ( - * ^ ^
		* out = y b	2 h 2 3
	* Beolvassuk: /
		* v = [ := ( - /
		* out = y b	2 h 2 3 ^ ^ *
	* Beolvassuk: e
		* v = [ := ( - /
		* out = y b	2 h 2 3 ^ ^ * e
	* Beolvassuk: )
		* v = [ :=
		* out = y b	2 h 2 3 ^ ^ * e / -
	* Beolvassuk: /
		* v = [ := /
		* out = y b	2 h 2 3 ^ ^ * e / -
	* Beolvassuk: j
		* v = [ := /
		* out = y b	2 h 2 3 ^ ^ * e / - j
	* Beolvassuk: *
		* v = [ := *
		* out = y b	2 h 2 3 ^ ^ * e / - j /
	* Beolvassuk: a
		* v = [ := *
		* out = y b	2 h 2 3 ^ ^ * e / - j / a
	* Beolvassuk: +
		* v = [ := +
		* out = y b	2 h 2 3 ^ ^ * e / - j / a *
	* Beolvassuk: 2
		* v = [ := +
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2
	* Beolvassuk: -
		* v = [ := -
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 +
	* Beolvassuk: 1
		* v = [ := -
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1
	* Beolvassuk: /
		* v = [ := - /
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1
	* Beolvassuk: w
		* v = [ := - /
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1 w
	* Vége, kiírjuk a vermet
		* v = [
		* out = y b	2 h 2 3 ^ ^ * e / - j / a * 2 + 1 w / - :=
