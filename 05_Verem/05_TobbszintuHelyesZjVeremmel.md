# Helyes zárójelezés

## Hierarchikus eset - veremmel

* Most nézzük a több szintű zárójelekkel rendelkező esetet
    * A legerősebb a "{ }" zárójel, ebbe kerülhet önmaga és a gyengébbek
    * A következő a "[ ]" zárójel, szintén önmaga és a gyengébbek mehetnek bele
    * Végül a "( )" típus, ebbe csak önmaga kerülhet
* Persze ez nem egy kőbe vésett dolog, nem "égetjük" bele a kódba a zárójelek karaktereit, ez az egész kívülről konfigurálható ideális esetben
* Vezessünk be egy prec(ch) függvényt, ami adjon vissza egy számot, ami leírja az adott ch-val jelölt zárójel erőségét. Ez minél nagyobb, annál erősebb a precedencia
		* Pl: prec('{') = prec('}') = 3; prec('(') = 1
	* Tegyük fel, hogy adott egy NYITÓ_ZÁRÓJEL és egy CSUKÓ_ZÁRÓJEL halmaz a megfelelő elemekkel, konstans lekérdezéssel
* Most a verembe mentsük el a zárójel karakterét is és az indexet is, mert mindkettőre szükség lesz, illetve a zárójel karakter helyett annak precedenciája is elég lenne, mivel még mindig csak a nyitókat rakjuk be
* A v.top()<sub>1</sub> jelentése legyen a veremtető első komponense, ami itt a zárójel karakter lesz
* A fenti algoritmusból kiindulva így lehet ezt a bonyolultabb feladatot megoldani:
```
helyesZjPrec(s : InStream, out& : OutStream) : L
	v = Stack() //itt a T a (Ch,N)-pár
	out = <>
	i = 1
	ch = s.read() // ch : Ch
	AMÍG ch != EOF
		HA ch eleme NYITÓ_ZÁRÓJEL
			HA v.isEmpty() v prec(v.top()₁) >= prec(ch)
				v.push(ch, i)
			KÜLÖNBEN
				return false
		KÜLÖNBEN HA ch eleme CSUKÓ_ZÁRÓJEL
			HA !v.isEmpty() és prec(v.top()₁) == prec(ch)
				out.print(v.pop()₂)
				out.print(i)
			KÜLÖNBEN
				return false
		ch = s.read()
		i = i+1
	return v.isEmpty()
```

* Ha nyitó zárójelet olvasunk, még automatice nem rakhatjuk a verembe, hanem csak ha vagy ez az első (nem már becsukott) zárójel, vagy ha nála nagyobb-egyenlő precedenciájú nyitott zárójelben áll
* A csukónál pedig csak a legutóbb megnyitott nyitó párja az elfogadható