# Helyes zárójelezés

## Veremmel

* Itt most a vermet, és a műveleteit adottnak tekintjük, nem kell (nem is szabad) az implementációjába belemenni, az interfészben megadott műveleteket használhatjuk
* Legyen adott egy "rendes" matematikai kifejezés, mindenféle egyéb karakterekkel (műveletek, paraméterek, ...)
* Döntsük el, helyesen van-e zárójelezve és írjuk ki a nyitó-csukó-párok indexeit a becsukás sorrendjében (formázás nem érdekes, és nem baj, ha egy helytelenül zárójelezett kifejezés első néhány zárójelét kiírjuk)
* Példa:
	* Bemenet: ( ( a + b ) / ( 2 - x ) ) ^ ( 5 * z )
	* Kimenet: Helyes; (2, 6) (8, 12) (1, 13) (15, 19)

```
helyesZj(s : InStream, &out : OutStream) : L
	v = Stack()
	out = <>
	i = 1
	ch = s.read() // ch : Ch segédváltozó
	AMÍG ch != EOF
		HA ch == ’(’
			v.push(i)
		KÜLÖNBEN HA ch == ’)’
			HA v.isEmpty()
				return false
			KÜLÖNBEN
				out.print(v.pop())
				out.print(i)
		ch = s.read()
		i = i+1
	return v.isEmpty()
```

* A függvény a helyességgel tér vissza, mellékhatásként kiír az átadott kimeneti folyamra (pl. konzolra)
* Előreolvasunk, karakterenként feldolgozunk
    * Direkt bemeneti folyam a bemenő paraméter, ezt nem lehet indexelni, nem lehet visszalépni már beolvasott elemre, és a hosszát se tudjuk lekérdezni. Csak elemenkénti feldolgozásra van lehetőség
* Ha nyitót olvasunk, berakjuk a verembe (kb. mint a számláló növelése)
* Ha csukót, kivesszük, ami a számláló csökkentését jelenti. Az, hogy a számláló 0, azzal ekvivalens, ha a verem üres (és minden zárójel be van csukva és mégis csukót olvasunk)
* Maga a verem azért kell, mert ki akarjuk írni a sorszámokat
* A végén akkor volt helyes, ha épp kiürült a verem (azaz, ha nem pozitív a számláló, nincs megnyitott, de nem bezárt zárójel)