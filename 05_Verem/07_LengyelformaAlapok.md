# Lengyelforma

* Egy műveletet leírhatunk úgy is, hogy a műveleti jel a két operandus között van (ez a megszokott alak), de akár másképp is:
	* a+b (infix forma)
	* +ab (prefix forma)
	* ab+ (postfix forma)
* A prefix alakot szokás "lengyelformának" (PN - Polish Notation, Jan Łukasiewicz után)
* A postfixes jelölés pedig a fordított lengyelforma (RPN - Reverse Polish Notation), ami amúgy ausztrál lenne :) (Charles Leonard Hamblin)
* Ezen két jelölésnek számtalan haszna van, de mivel utóbbit könnyebb kiértékelni, ezt fogjuk használni, és mostantól eleve ha a "lengyelforma" szót halljuk, a "fordított lengyelformára" gondolunk!

## Tulajdonságai

* Nincs benne zárójel (erre megy ki a játék eleve)
* Az operandusok egymáshoz képest fizikailag abban a sorrendben vannak leírva, mint az infix változatban
* Az operátorok a kiértékelés sorrendjében vannak leírva, ami eltérhet az eredeti sorrendtől a zárójelek, precedencia és az asszociativitás iránya miatt
* Az operátor mindig a két paramétere után van közvetlenül (persze a paraméterek lehetnek egy részkifejezés eredményei)
* Veremmel hozható létre az infix alakból
* Veremmel kiértékelhető 

## Bevezető példák

* a * b + c ~ a b * c +
	* Előbb az a * b -t számoljuk ki, majd az "a * b" + c -t
* a + b - c ~ a b + c -
	* Ez ugyanolyan lett, mivel bár a precedencia azonos, de az asszociativitás miatt balról jobbra haladunk
* a + b * c ~ a b c * +
	* A szorzás az erősebb, hiába van "jobbrább", ezért azt írjuk le előbb. A "b c *" lesz az összeadás baloldali operandusa
* a ^ b ^ c ~ a b c ^ ^
	* Itt az infix "második" hatványjele van elsőnek írva a postfix jelölésben, mivel a jobbasszociativitás miatt azt értékeljük ki hamarabb
* a := b := c ~ a b c := :=
	* Dettó
* ( a ^ b ) ^ c ~ a b ^ c ^
	* Itt a zárójelezés miatt marad az eredeti sorrend
* a * ( b + c ) ~ a b c + *
	* A zárójel miatt előbb a + értékelődik ki, ezért van előbb írva, aztán őt követi a szorzás, aminek a jobb paramétere az összeg!
