# Verem / Stack

* A verem egy LIFO (last in first out) generikus adatszerkezet, azaz "az jön ki belőle leghamarabb, ami a legkésőbb került bele", ilyenformán egy egyetemistára hasonlít a legjobban szombat este
* Iterált adatszerkezet, hiszen ugyanolyan (T-vel jelöljük majd) típusú adatokat tud tárolni. Az implementációtól függően lehet rekurzív, valamint, bár indexelést nem biztosítunk rá, de mégis egyértelmű sorrendben bejárható (még ha mellékhatásként ez ki is üríti), ezért lineáris adatszerkezet is
* Az alábbiakban ismertetem a verem interfész műveleteit, azaz ha egy adatszerkezet el akarja magáról mondani, hogy ő verem, akkor ezeket a metódusokat kell tudnia (ráadásul mindet konstans műveletigénnyel):
	* Stack() - A konstruktor. Új verem létrehozásakor fut le, üresre inicializálva azt
	* push(x : T) - A verem "tetejére" rakás
	* pop() : T vagy pop(x : &T) vagy pop() - A verem tetejének kivétele. Három lehetséges formában is elkészíthető. Az első rendes, jóliskolázott függvény lévén visszatér vele, a második egy eljárás, ami mellékhatásként tér vele vissza, míg a harmadik eldobja a veremtetőt, de nem adja vissza. Mindhárom tehát csökkenti a verem méretét! Előfeltételük, hogy a verem ne legyen üres. Fontos megjegyezni, hogy nem a verem tetejét jelentő pointerrel vagy "node"-dal térnek vissza, hiszen akkor az implementáció szökne ki, illetve eleve az interfészbe kellene foglalnunk implementációs megkötéseket
	* top() : T vagy top(x : &T) - Más néven "peek". Visszatér a felső elemmel (ha van - azaz megint a nemüresség az előfeltétel), anélkül hogy kiszedné a veremből
	* isEmpty() : L vagy isEmpty(&l : L) - Függvény-, vagy esetleg eljárásszerűen hívva a logikai paraméterbe adja meg a választ, ami megmondja üres-e a verem, azaz, igaz-e, hogy nincs veremtetője. Méretet lekérdezni nem lehet
	* isFull() : L, isFull(&l : L) - Kiegészítő függvény, statikus tömbös implementációnál lehet értelme. Láncolt lista esetében ugyanis elvileg nem telhet be a verem, mindig tudok új elemet foglalni a heap memórián. Persze a gyakorlatban minden memória betelik egyszer, de az már az operációs rendszer hatáskörébe tartozó probléma, míg a tömbös ábrázolás esetén nem
