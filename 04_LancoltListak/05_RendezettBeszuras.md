# Beszúrás rendezett listába
	
* Két szempont szerint lehet itt különböző megoldásokat adni
	* Az egyik, hogy mi a bemenet, egy kulcs-skalár, vagy már eleve egy kész pointer
		* A most következő első példa az első esetet mutatja, a második a másodikat - ott már nem hozunk létre E1-et, úgy tekintjük a key adattagjában a helyes érték van, a next pedig definiálatlan (azaz valahol kell lennie egy értékadásnak rá nézve, anélkül biztosan nem jó a program)
	* A másik pedig, hogy hány pointert akarunk használni
		* Az első példa két pointeres megoldás lesz, a második egy pointeres
* Miért is kellhet két pointer? Hát azért, mert az algoritmusunk két részből fog állni: előbb megkeressük az utolsó olyan elemet, aki kisebb, mint amit be akarunk szúrni (ez gyakorlatilag a keresés alapfeladata), majd beszúrjuk ez után az elem után
	* Na de, honnan tudjuk, hogy melyik elem az utolsó, aki még kisebb nála? Nyilván akkor derül ez ki, amikor beolvassuk az elsőt, aki már nem!
	* Ezért mindenképp valahogyan előre kell nézni egy listaelemmel. Erre van két stratégia: az egyik karbantartja az aktuális pointer mellett az "előző"-t is, és majd a végén ez után az előző után szúrja be a beszúrandót; míg a másik eleve az aktuális pointer rákövetkezőjét vizsgálja
		* Érezzük, hogy mindkét esetben külön történet az, ha az első elem elé szeretnénk beszúrni. Miért is? Mert az első elemnek nincs "előző" pointere, illetve mert ha eleve az első elem rákövetkezőjét vizsgáljuk, akkor az első elem kimarad
* A fentiek értelmében a két pointeres algoritmus a következőképp fog működni:
	* Létrehozzuk az új E1-et, a kulcsát már be is tudjuk állítani a paraméter alapján
	* Elindulunk p-vel l-től, és definiáljuk a prev (p előzője) pointert is, amit jobb híján NULL-ra inicializálunk (sőt, ennek a NULL értéknek mindjárt nagyon is komoly jelentése lesz)
	* Egyszerű while-ciklussal megkeressük az első olyan elemet, aki már nem kisebb, mint k. Nullchecket muszáj tennünk, hiszen az is lehet, hogy nincs ilyen elem (ekkor végigmegyünk a listán), sőt az is, hogy eleve üres a lista, p már új korában is NULL
	* prevet is karbantartjuk és p-t is növeljük
		* A prev = p értékadásnak meg kell előznie p növelését, hiszen utána már nem tudjuk azt visszavonni, nem tudjuk elérni p előzőjét
		* A p = p->next értékadás így ebben a formában teljes, a "p->next" természetesen nem elég, olyan az, mintha annyi írnánk hogy i+1 az i = i+1 vagy az ezt rövidítő ++i helyett (ez egy gyakori hiba!)
	* Ezen a ponton tehát p-ben ott van az az elem, ami elé kell szúrnunk
	* Mint korábban írtam, négy esetet kell néznünk. Kis gondolkodás után rájöhetünk, hogy egy megfelelő kétágú elágazással le tudjuk fedni mind a 4 esetet:
		* Ha most prev == NULL, akkor mi is történt? Nem léptünk be a ciklusba! Hiszen prev csak a legelején NULL (mondtam, hogy lesz jelentősége), és ezen kívül minden ciklusmag-futtatáskor értéket kap, mégpedig p előző értékére, de p akkor nem volt NULL a ciklusfeltétel alapján. Tehát prev akkor és csak akkor NULL, ha nem léptünk be a ciklusba, magyarán l==p
			* Ekkor mi a teendő? Mint az előző feladatban, l-nek kell értékül adni a beszúrandó elemet, majd ennek a nextjét kell beállítani l-re (akár NULL volt, akár nem)
			* Ha l NULL volt, akkor ezzel lefedtük az üres lista esetét, ha nem, akkor pedig a lista elejére szúrást - hiszen a lista vagy üres volt, vagy minden eleme (beleértve azt, amire l mutat) nagyobb-egyenlő mint a beszúrandó elem
        * Ha pedig prev nem NULL, akkor, az előző elem után kell szúrni, de ezzel elveszítenénk a listában p-t, ezért a q rákövetkezőjét ráállítjuk p-re
			* Ez az ág lefedi a lista "közepét" és a lista végét is, hiszen ha azért léptünk ki, mert p már NULL volt, akor is prev után kell q-t szúrnunk, és persze ez esetben az ő mutja NULL kell legyen, na de az épp p
        * Fontos, hogy mindkét ágon kapott értéket q->mut, hiszen mint mondtuk, amíg explicit nem adok meg neki valamit, addig nem hogy NULL, hanem egyenesen definiálatlan az értéke
    * Ezek után a kód:
		
		```	
        insert(&l : E1*, k : T)
            q = new E1
            q->key = k
            p = l
            prev = NULL
            AMÍG p != NULL és p->key < k
                prev = p
                p = p->next
            HA prev == NULL
                l = q
                q->next = p
            KÜLÖNBEN
                prev->next = q
                q->next = p
		```

* Most nézzük az egy pointeres esetet
	* Ezt speciel úgy fogom csinálni, hogy már eleve pointert kapunk paraméterként, de istenigazából ez a keresés szempontjából nem számít
	* Itt az elején külön megnézem az üres lista, és a "lista elejére szúrok" esetet
		* Tehát megnézem l NULL-e, vagy ha nem NULL (rövidzár!), a kulcsa nagyobb-e, mint a beszúrandó elemé
			* Ekkor megismétlem az előző példa ide vonatkozó esetét
	* Ha ez nem volt igaz, elindulok l-től p-vel. De l-t már megnéztem az imént, tehát merhetem igazából mindig a p->nextet nézni
		* p biztosan nem NULL, mert hiszen az az első ág volt, sőt az értéke is kisebb, mint q-é, lépésről lépésre kell ezeket vizsgálni p nextjére
		* Amikor megáll a ciklus, a "szokásos" két eset miatt tehette:
			* Vagy mert a lista végén van p (p->next == NULL)
			* Vagy mert p az utolsó kisebb elem (p->next != NULL és p->next->key >= q->key)
		* Ekkor tehát p és p->next közé szúrjuk az elemet, azaz előbb beállítjuk az elemünk nextjét p->nextre (akár NULL, akár nem), majd a már elmentett p->nextet felülírjuk q-val. Az fontos, hogy p itt sem lehet NULL, azt mindig a ciklus előző körében ellenőriztük
	* A kész megoldás:
	
		```
		insert(&l : E1*, q : E1*)
		    HA l == NULL v l->key >= q->key
                q->next = l
		        l = q
            KÜLÖNBEN
	            p = l
	            AMÍG p->next != NULL és p->next->key < q->key
                    p = p->next
                q->next = p->next
                p->next = q
		```
