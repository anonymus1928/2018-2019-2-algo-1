# Rendezett HL-k rendezettséget megtartó összefésülése (uniója)

* Az alapfeladatnak többféle változata ismeretes annak függvényében, hogy unióról, metszetről, különbségről (egyik vagy másik irányban), szimmetrikus differenciáról beszélünk, illetve, hogy mi legyen az inputok "sorsa" a függvény futásának végére. Valamint a műveletigényről is szoktak lenni elvárásaink. A halmaztulajdonság (minden elemből maximum egy van) is gyengíthető zsáktulajdonságra (multiset). A rendezettség viszont mindig alapelvárás az összefuttatós algoritmusvázhoz (röviden halmaznál: szigorúan monoton legyen, zsáknál: monoton legyen - a növekvő vagy csökkenő tulajdonság is megengedett, de minden input és output e szempontból azonos)
* Itt most legyen az a feladat, hogy f1 és f2 unióját állítsuk elő f1-be, f2 pedig ürüljön ki
* Az OOP tárgyból ismert háromágú elágazás lesz a megoldás lelke: vesszük a két lista "következő" elemeit, és megnézzük a rendezettség szerint melyik a következő a közös felsorolásban. Azzal kezdünk valamit, majd igény szerint egyikben, másikban, mindkettőben továbblépünk
* A műveletigény itt Ordó(|f1|+|f2|) lesz. A ciklusban végigmegyek addig, amíg még egyik sem fogyott el. Ez akkor lehet maximális, ha f2-ben csupa kisebb elem van, mint f1-ben, de az utolsó eleme nagyobb. Ekkor egyesével végigmegyünk f2 elemein az utolsó kivételével, emiatt nem ér véget f2, tehát futni fog még a ciklus. Most végigmegyek f1 elemein, és ekkor léptem összesen f2 méretényi - 1 + f1 méretényit, ami már nagyságrendben így is annyi, mint amit állítottunk, de a végén még jön q átfűzése, ami 1 lépésnek számít, tehát ez kereken a két lista hosszányi lépésszámot fog tekerni. Ennél lehet jobb futási eredmény, ha pl. vannak közös elemek, vagy f2 összes eleme kisebb, mint f1 összes eleme, ezért "csak" Ordó és nem Théta
* Azt azért láthatjuk, hogy minden esetben lineáris a műveletigény, csak rossz esetben a két lista hosszának összege szerinti lineáris, jó esetben a két lista hossza közül vett kisebb hossza szerinti lineáris, ami - mivel nem tudjuk a két lista hosszának egymáshoz képest vett viszonyát - lehet nagyságrendekkel jobb is
* Bár már többször tisztáztuk, de nem lehet elégszer elmondani, mert gyakori hiba: ha az egyik, vagy mindkét lista hossza 0, az nem számít helyes érvnek amellett, hogy ekkor a műveletigénynek is 0-nak, vagyis legalábbis konstansnak kellene lennie. A műveletigényeket mindig az input méretének függvényében mondjuk, azaz ha n=0 és ekkor a műveletigény kb. 0, az semmit nem jelent, hiszen ekkor lineáris is épp úgy lehet, mint köbös. Amikor a "legjobb" inputot keressük, akkor egy olyan struktúrát keresünk, ahol adott méret mellett, ahhoz képest a lehető legkevesebb műveletet kell végeznünk. Pl. egy rendezőalgoritmus esetében, ha az input eleve rendezett, akkor 0 cserét kell végezünk, míg ha nem az, akkor biztosan többet

```
union(f1 : E1*, f2 : E1*)
	pPrev = f1
	p = f1->next
	q = f2->next
	AMÍG p != NULL és q != NULL
		HA p->key < q->key
			pPrev = p
			p = p->next
		HA p->key > q->key
			pPrev->next = q
			q = q->next
			pPrev = pPrev->next
			pPrev->next = p  
		HA p->key = q->key
			pPrev = p
			p = p->next
			s = q
			q = q->next
			delete s
	HA q != NULL
		pPrev -> next = q
	f2->next = NULL
```

* Elindulunk egy-egy pointerrel a két listán
	* Kezdhetünk az fx->nextekkel, mert az első elem a fejelem, ami nem játszik
	* f1-en számontartjuk az előző pointert is (pPrev), mert ide fogunk beszúrni f2-ből ha úgy van (és általában úgy van)
* Jön a jól ismert három ágú elágazás
	* Ha az f1-beli a kisebb, csak továbblépünk
	* Ha az f2-beli az, akkor p előzője és p közé szúrjuk (hiszen p előzője már kisebb-egyenlő volt, mert az már az unió része). Tehát pPrev következője legyen ez az elem, léptessük tovább az f2 pointerét, q-t, hogy ne ragadjon be, pPrevet is állítsuk az előbbi q-ra (amit már továbbléptettünk persze), hiszen most már ez az elem a p előzője, majd végül ennek az elemnek a következőjét is állítsuk p-re
	* Ha egyenlő a két elem, akkor egyrészt azt csináljuk (első két sor), mint az első ágon - megtartjuk az f1-belit -, másreszt kidobjuk az f2-belit, de előbb továbbléptetjük a q pointert
* A végén egy dolog nem fordulhat elő: mindkét listában van még feldolgozatlan elem (ciklusfeltétel miatt)
	* Az lehet, hogy egyikben sincs, ekkor kész vagyunk
	* Az lehet, hogy f1-ben van, ekkor is, hiszen ők kizárásos alapon nagyobbak f2 elemeinél
	* És az is lehet, hogy f2 nem üres, ekkor szimplán csak pPrev utánra rakjuk f2 maradékrészét, ami q-val keződik (azért pPrev utánra, mert p már NULL, onnan tudjuk, hogy f1-en végigmentünk - ha a NULL p-t írnánk át, az nem szúrna a listába!)
* A q != NULL feltétellel szemantikailag ekvivalens a p == NULL feltétel, viszont ott ha q is NULL, akkor ez egy felesleges nullpointer-átláncolást jelent, azaz ez egy picit hatékonyabb
    * És mi van, ha nincs feltétel egyáltalán? Az hülyeség volna, mert ha q == NULL, de pPrev->next nem az, ami lehetséges, ha p nem NULL, akkor felülírjuk azt helytelenül NULL-lal
* A legeslegvégén még NULL-ra álítjuk f2 nextjét, hiszen ez továbbra is a hajdanvolt első elemére mutat, ami most "valahol" f1-ben van már, tehát a nextje egyáltalán nem biztos, hogy f2-beli (na meg ha a fejelem nextje null, az jelenti az üres listát és épp ez volt a feladat)
* Ez az "éselős" ciklusfeltételes megoldás, de létezik "vagyolós" is. Erre is fogunk majd látni példát