# Ciklikus listák (CL)

* Annyival tud többet a sima listánál, hogy az utolsó elem nextje nem NULL, hanem visszamutat az első elemre
	* Egy elemű lista esetében önmagára
	* Az üres listát itt is egy darab nullpointerrel adhatjuk meg
* Ha fejelemes, azaz HCL-ről beszélünk, akkor a fejelem is része körnek
	* Ilyenkor egyáltalán nincs nullpointer, hiszen a fejelem miatt a lista mutatója nem lehet NULL, a ciklikusság miatt pedig a lezáró elem nextje nem lehet NULL
* Ha kétirányú és ciklikus (C2L), akkor az első elem prevje az utolsó elemre mutat
    * Ekkor a listát alkotó elemek típusa E2C, ami annyiban tér el az E2-től, hogy a konstruktorában a next és a prev is önmagára inicializálódik, nem NULL-ra
    * Azaz egy egy elemű listát könnyen tudunk létrehozni, csak a kulcsot kell beállítani
* A non plus ultra, a H2CL (azaz fejelemes, kétirányú, ciklikus lista), itt szintén E2C a listaelemek típusa
    * H2CL esetén üres listát a new E2C kiadásával hozhatunk létre. Ez létrehoz egy definiálatlan tartalmú, mindkét irányból önmagára mutató elemet, azaz a fejelemet
* Egy ilyen listánál úgy tudjuk megállapítani, hogy végigjártuk-e az elemeit, ha teszteljük, a bejárás során (újra) elértük-e a lista pointere által mutatott elemet
