# Összehasonlító rendezések

## Beszúró rendezés HL-ra

* Angolul Insertion sort
* Hasonló a listafordítóshoz (csak itt rendezett beszúrást végzek - ami a műveletigényét is egy n-es szorzóval rontja ahhoz képest)
    * Kimentem az első elemet, beállítom üresre a listát, majd amíg az első elemből következő listaelemek nem fogynak el, rendezetten beszúrom őket a lassan előálló rendezett listába
* A ciklusban előbb továbblépek p-vel, majd beszúrom a p "előzőjét" a rendezett listába
* Mivel fejelemes, az f-et nem kell referencia szerint átadni

```
insertionSort(f : E1*)
	p = f->next
	f->next = NULL
	AMÍG p != NULL
		q = p
		p = p->next
		insert(f,q)
```

* Ahol, az insert függvény egy az egyben a fejelemes listára való már adott pointer beszúrása, az alábbiakban megismétlem:

```	
insert(f : E1*, q : E1*)
	p = f
	AMÍG p->next != NULL és p->next->key < q->key
		p = p->next
	q->next = p->next
	p->next = q
```

* Műveletigénye négyzetes, hiszen "kint" és "bent" is végigmegyünk potenciálisan az egész listán
