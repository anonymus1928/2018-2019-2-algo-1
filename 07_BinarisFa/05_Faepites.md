# Faépítés

## Kifejezésfa rekonstruálása
		
* Adott egy kifejezés, építsük fel a fát:
	* a / (b + c) * ( h - j + k ) ^ e
* Első lépés: zárójelezzük be teljesen:
	* ( ( a / ( b + c ) ) * ( ( ( h - j ) + k ) ^ e ) )
* Innen meg nyilván mindig a fő műveleti jelet a (rész-)fa gyökerének választva könnyen felépíthetjük (amiből pedig postorder bejárással könnyen megkapjuk a lengyelformát)

	```
             *
            / \_
           /    ^
          /\    / \
         a  +  +   e
           /\  /\
          b c  - k
               /\
              h  j
	```

## Faépítés lengyelformából

* Feltesszük, hogy csak binér műveleteink vannak
* Szülőpointerekkel most nem foglalkozunk
* Hasonlóan, mint az egyik kiértékelő algoritmusnál, itt is a jobb gyereket vesszük ki hamarabb a veremből, mint a balt
* Láncolt ábrázolásra írtam meg

```
faépítés(s : InStream) : Node*
    v = Stack()
    ch = s.read()
    AMÍG ch != EOF
        q = new Node
        q -> key = ch
        HA ch eleme OPERÁTOR
            q -> right = v.pop()
            q -> left = v.pop()
        v.push(q)
        ch = s.read()
    return v.pop()
```

## Általános bináris fa felépítése bejárásokból

* Feladat: adott egy nem feltétlen teljes, bináris fa
	* Ez volt a preorder bejárása: 12, 40, 25, 2, 3, 17, 5, 7, 11, 30, 1, 9, 6, 42
	* Ez volt az inorder bejárása: 3, 2, 17, 25, 40, 7, 5, 11, 12, 1, 6, 9, 42, 30
* Hogyan nézhetett ki a fa?

	```
             12
            /  \
           40    30
          / \    /
         25  5   1
        /   /\   \
       2   7  11   9
      / \         / \
     3   17      6  42
	```

* A preorderből tudjuk, hogy a 12 a gyökér, majd a 40 az ő egyik leszármazottja - azt már az inorderből tudjuk, hogy bal oldali, mert abban a 12-től balra találjuk!	
	
* In-post-párból is visszaalakítható hasonló módon
* Pre-postból nem, mert nem tudjuk a jobb-bal sorrendet, példa:
	```
        a    a
       /      \
      b        b
	```
* Mindkettő preorder bejárása a,b; postorder bejárása b,a
* Speciális fáknál elképzelhető, hogy kevesebből is rekonstruálhatók, pl. az előző alfejezetben láthattuk a kifejezésfát, amit az RPN-ből - ami az ő postfix bejárása - fel tudtunk építeni
