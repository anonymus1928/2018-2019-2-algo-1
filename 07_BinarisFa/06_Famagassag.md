# Famagasság

* Definíció szerint a leghosszabb ág hossza, avagy a gyökértől való leghosszabb előforduló távolság
	* Egy csúcsú fánál 0
	* Egy éllel rendelkező fánál 1
	* ÜRES fánál -1, az általánosság kedvéért
	* az alábbi fánál 4

	```
         *
        /\
       *  *
      /\  /\
     * *  * *
         /
         *
         \
         *
	```

	* Absztrakt szinten megadva, rekurzívan (fáknál ez igen gyakori fajta algoritmus)
	
	```
    magasság(t : BinTree) : Z
      HA t == ÜRES
        return -1
      KÜLÖNBEN
        return max(magasság(t.left()), magasság(t.right())) + 1
	```
