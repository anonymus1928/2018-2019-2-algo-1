# Bináris fák

* Nem lineáris adatszerkezet
* ADT-szinten (abstract data type, azaz megvalósítás-agnosztikus) a lenti ábrának megfelelően tekintünk rá, egy összefüggő, körmentes, irányítatlan gráfként; csomópontok és köztük húzott kapcsolatok halmazaként, egyfajta általánosított láncolt listaként, holott a reprezentációja lehet tömb-alapú is
* A csomópontok közül az egyik kitüntetett, ezt hívjuk gyökérelemnek és ezzel az elemmel azonosítjuk a fát, épp úgy, mint ahogy a láncolt listák első elemével azonosítottuk magát a listát
* A fákat jelentő változókat hagyományosan t-nek (tree) hívjuk, a listához hasonlóan a láncoltan ábrázolt fa típusa megegyezik a csomópont típusával
* Ha ADT-szinten beszélünk róla, akkor ahogy a listáról H2L meg hasonló neveken szóltunk, a fa típusneve BinTree lesz
* Ha a fának nincs csomópontja (gyökere se), akkor t == ÜRES, különben pedig t a gyökeret azonosítja, pl. alább t == a
* Régebbi anyagokban az ÜRES fát az Ω szimbólummal jelölhetik

```
    a
   / \
  b   c
 / \   \___
d   e      f
   / \    / \
  g   h  i   j
        /
       k
```

* A gyökeret hagyományosan legfelülre rajzoljuk, belőle pedig 0, 1 vagy 2 él indulhat ki
* Az élek "iránya" nem mindegy. Él balra-lefele és jobbra-lefele indulhat egy másik csomópont felé
* A csomópontok közé húzott élek szerint beszélhetünk közöttük az alábbi viszonyokról:
	* Bal gyerek (pl. a-nak b; e-nek g)
	* Jobb gyerek (pl. a-nak c; c-nek f; i-nek ÜRES)
	* Szülő (pl. a-nak ÜRES; e-nek b)
* Egy csúcs gyerek csúcsa gyakorlatilag a gyerek csúccsal, mint gyökérrel megadott fa, tehát a csúcs gyereke egyben részfája is. Ezért ha egy csúcsnak nincs bal vagy jobb gyereke, akkor azt is mondhatjuk, hogy az adott csúcs bal vagy jobb részfája az ÜRES fa
* Egy csúcs szülője akkor és csak akkor ÜRES, ha ő a gyökér
* Ha egy csúcs mindkét oldali részfája ÜRES, akkor ő egy levélcsúcs, különben köztes csúcs
* A csúcsokhoz az egymás közti viszonyokat jelző "pointereken" túl még más adattagokat is rendelhetünk, egy "címkét" mindenképpen: a fenti gráfon ezek a betűk
* Az ÜRES fát táblán egy áthúzott körrel jelölöm, épp úgy, mint a NULL pointert. Ennek ellenére ennek a kiolvasása az "üres fa", nem pedig a "nullpointer", mert most nem láncolt reprezentációról van szó, hanem a fa típus absztrakt megadásáról - más kérdés, hogy a láncolt reprezentációban az ÜRES fa megfelelője épp a NULL pointer lesz
* A fa mérete (n-nel jelöljük) a csúcsai számát jelenti. ÜRES mérete 0
* A fa magassága (jele h) a gyökértől való (egyik) legmesszebb levő csúcs távolsága. Az egy elemű fáé 0, a példában levő fáé 4; az ÜRES fáé -1
* A csúcsokhoz rendelhetünk szinteket, ami a magassággal analóg módon jön ki: a gyökér szintje 0, a legmesszebb levő elem szintje a magasság, a példában a g csúcs szintje 3
