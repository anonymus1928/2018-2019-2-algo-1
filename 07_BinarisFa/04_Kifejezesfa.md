# Kifejezésfa

* Adott egy valamilyen binér műveletekből álló összetett kifejezésből épített (értelemszerűen bináris) fa
* A köztes csúcsok műveletek, a bal és jobb részfák rendre a szülő bal és jobb operandusai, a levelek a változók vagy literálok

	```
             +
            / \
           *   /
          /\   /\
         +  -  e ^
        /\  /\   /\
       a  b c d  f g
	```
	
* Járjuk be a három megismert módszerrel:
	* Preorder: + * + a b - c d / e ^ f g
		* Megkaptuk a prefixes lengyelformát (PN)
	* Inorder: a + b * c - d + e / f ^ g
		* Sima infix jelölés, de vigyázat! Rosszul (sehogy) zárójelezve
	* Postorder: a b + c d - * e f g ^ / +
		* Fordított lengyelforma (RPN)
