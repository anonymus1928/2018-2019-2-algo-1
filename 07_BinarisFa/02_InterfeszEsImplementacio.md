# Bináris fák

## Interfész - műveletek

* t == ÜRES : L
    * Azaz minden fáról le tudjuk kérdezni egy logikai értékbe, üres-e
* t.left() : BinTree
    * Le tudjuk kérni a bal gyereket, vagy másképpen bal részfát (ami lehet üres is)
    * Üres fának nincs se gyereke, se szülője se címkéje, azaz ezekhez a nem üresség az előfeltétel (mintahogy NULL-nak sincs kulcsa a listák világában)
* t.right() : BinTree
* t.parent() : BinTree
    * Opcionális művelet, nem minden reprezentáció támogatja
* t.key() : T
    * Az a bizonyos címke
	
## Ábrázolások	

### Aritmetikai (azaz tömbös) ábrázolás

* A címkék sorfolytonosan a kimaradó elemek (és gyerekeik) kihagyásával -emiatt a gyereket/szülőt lekérő műveletek nagyon gyorsak
* A példa fájára: t = [a,b,c,d,e,NULL,f,NULL,NULL,g,h,NULL,NULL,i,j,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,k,NULL,NULL,NULL]
* A tömb elemei T típusúak, ahol is a T a kulcs (címke) típusa. Itt és most ez Character, a "NULL" jelentse az üres karaktert (nem nullpointert)
* A tömb mérete (ami nem összekeverendő az általa jelölt fa méretével) a következőképp alakul
	* 0. szint: 1 (gyökér)
	* 1. szint: 2
	* 2. szint: 4
	* stb.
* Azaz összesen ha h szintes a fa (0-tól kezdve), 2<sup>h+1</sup> - 1 elem kell a tömbbe, a példában 31
	* Ez a reprezentáció ritka, de mély fáknál nem éri meg
* Műveletek megvalósításai:
	* i.left() := 2i (nem szorzás, shiftelés, mert az olcsóbb)
	* i.right() := 2i + 1
	* i.parent() := alsóegészrész(i/2)
	* i.key() := t[i]	

### Láncolt ábrázolás

* Olyan elemtípusra (Node) mutató pointer, aminek adattagjai:
	* left : Node*
	* right : Node*
	* key : T
* A műveletek értelemszerűen, pl. t.key() := t->key - arra mindig figyeljünk oda, hogy a függvényszerű jelölés az ADT-szinten adott fáknál, míg a pointeres jelölés a láncolt ábrázolással megadott fáknál dívik	
* Ha a szülőt is számon akarjuk tartani, akkor a csúcs (és így a fa) típusa Node3, és még egy adattagja van a fentieken túl:
	* parent : Node3*
* Konstruktorból két féle van, mind Node-ra, mind Node3-ra
	* Üres: minden pointert NULL-ra inicializál, key definiálatlan marad
	* x : T paraméterű: minden pointert NULL-ra inicializál, key értéke x lesz
