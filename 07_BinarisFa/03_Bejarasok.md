# Bináris fa nevezetes bejárásai - rekurzív algoritmusok

* A gyökérből kiindulva bejárjuk az összes csomópontot. A bejárások közötti különbséget a gyökér "elhelyezkedése" jelenti a bal és jobb gyerekhez (részfához) képest
* A bal mindig meg fogja előzni a jobbot
* A bejárások szabályai rekurzívak, azaz preorder esetén a gyökér gyerekeire, és azok gyerekeire is alkalmazni kell a preorder sorrendet

## Preorder bejárás

* Dolgozzuk fel a fa elemeit rekurzívan, gyökér-bal-jobb sorrendben

	```
    preorder(t)
        HA t == ÜRES
            SKIP
        KÜLÖNBEN
            feldolgoz(t.key())
            preorder(t.left())
            preorder(t.right())
	```

	```
	    1
	   / \
	  2   3
	```

## Inorder bejárás

* Bal-gyökér-jobb sorrendben

	```
    inorder(t)
        HA t == ÜRES
            SKIP
        KÜLÖNBEN
            inorder(t.left())
            feldolgoz(t.key())
            inorder(t.right())
	```

	```
	    2
	   / \
	  1   3
	```

## Postorder bejárás

* Bal-jobb-gyökér sorrendben

	```
    postorder(t)
        HA t == ÜRES
            SKIP
        KÜLÖNBEN
            postorder(t.left())
            postorder(t.right())
            feldolgoz(t.key())
	```

	```
	    3
	   / \
	  1   2
	```

## Továbbiak

* Nevezetes bejárás még a szintfolytonos bejárás, ez a konzultációra marad
* A fenti három bejárás mindegyike megírható iteratív módon is (azaz nem rekurzióval, hanem ciklussal)
	
* A példa fa bejárásai:
	* Preorder: a,b,d,e,g,h,c,f,i,k,j
	* Inorder: d,b,g,e,h,a,c,k,i,f,j
	* Postorder: d,g,h,e,b,k,i,j,f,c,a
