# Sor

* A sor egy FIFO adatszerkezet (összehasonlításul a verem LIFO volt)
	* First in first out: amit először beraktunk (legrégebben), az kerül ki elsőnek, azaz úgy lehet felfogni, mint aminek a végére pakolok és az elejéből szedek ki

## Műveletei

* Queue() - konstruktor
* isEmpty() : L, isEmpty(&l : L)
* isFull() : L, isFull(&l : L) - technikailag lehetséges csak
* add(x : T) - a végére rakja x-et
* rem() : T, rem(&x : T), rem() - kiveszi az első elemet (és visszatér vele)
* first() : T, first(&x : T) - visszatér az első elemmel, de nem veszi ki
