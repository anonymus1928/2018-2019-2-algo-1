# Dadogós

* Döntsük el egy stringről, hogy dadogós-e (azaz a fele után megismétlődik tartalma)
	* Legyen a bemenet streamként adva, különben nem lenne nehéz dolgunk, ha ismernénk a méretét és tudnánk indexelni

	```
	dadogós(s : InStream) : L
		s1 = Queue()
		s2 = Queue()
		hossz = 0
		ch = s.read()
		AMÍG ch != EOF
			s1.add(ch)
			hossz = hossz+1
			ch = s.read()
		HA 2|hossz
			n = hossz/2
			AMÍG n>0
				s2.add(s1.rem())
				n = n-1
			AMÍG !s1.isEmpty()
				HA s1.rem() != s2.rem()
					return false
			return true
		KÜLÖNBEN
			return false
	```
	
	* Bepakoljuk a string betűit egy sorba; közben számoljuk a hosszát
	* Ha nem páros a hossz, nem dadogós (nem tudjuk félbevágni a középső betűt)
	* Ha páros, a felét berakjuk egy másik sorba, majd a két sort karakterenként összehasonlítjuk (hasonló a feladat az első zh konzultációjakor vizsgált palindromos feladathoz, csak itt nem kellett inverziót rakni a stringbe)
		* Fontos, hogy itt feltehetjük a két sor kialakítása miatt, hogy a két sor pont egyszerre fog elfogyni. Ha annyi lenne a feladat, hogy hasonlítsunk össze két sort, ezeket a vizsgálatokat még el kellene végezni!