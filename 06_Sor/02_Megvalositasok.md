# Sorok

## Ábrázolások

### Tömbös

* a[1..n]; se eleme [1..n] (sor eleje); sv eleme [1..n] (sor vége) lehetne, de ilyenkor kellene egy plusz adattag, mert ha sv = se-1, azzal az üres sort éppúgy lehetne reprezentálni, mint a tele sort...
* Ezért inkább
	* a[1..n]
	* se eleme [1..n]
	* db eleme [0..n]
* Néhány metódus implementációja

	```
	Queue::Queue()
	  se = 1
	  db = 0
	```

	```
	Queue::add(x : T)
	  HA db == n
		HIBA
	  KÜLÖNBEN
		HA se + db <= n
		  a[se+db] = x
		KÜLÖNBEN
		  a[se+db-n] = x
		db = db+1
	```

	* A végére rakok, kivéve ha túlcsordulna, mert akkor az elejétől folytatom, amíg ott van hely
	* Kitérő: a gyakorlatban a tömbös reprezentációk gyakran olyanok, hogy a kapacitás betelése esetén nem hibajelzéssel térnek vissza, hanem megnövelik a kapacitást (ami egy teljesen természetes dolog, hiszen a kapacitás (n) nem is látható kívülről és az interfész alapján nem is szabadna lennie ilyennek). Ezt egyéb más módok mellett általában egy nagyobb mögöttes tömb újraallokálásával és az eddigi tömb egyesével való átmásolásával lehet elérni, ami azt eredményezi, hogy az amúgy konstans műveletigényű műveletek néha lineárisak lesznek, de ez nem az inputtól, hanem az adatszerkezet pillanatnyi belső állapotától függ. Ezt hívjuk "amortizált konstans" műveletigénynek

	```
	Queue::rem(&x : T)
		HA db == 0
			HIBA
		KÜLÖNBEN
			x = a[se]
			db = db-1
			HA se == n
				se = 1
			KÜLÖNBEN
				se = se+1
	```

	* Az előző inverze. Értelemszerűen a db csökken, de ez nem elég, mert akkor a "végéről" vennénk el elemet, ahhoz hogy az első essen ki, a sor eleje változót növelni kell (modulóban értve)

### Láncolt implementáció		

* se eleme E1*
* sv eleme E1*
	* És mindezt úgy, hogy van egy a fejelemhez hasonló helyőrző, amire kezdetben mutat mindkét pointer, majd utána mindig az sv fog csak erre: ez lesz annak a helye, ahova majd beszúrok, hivatalosan ez nem a lista része. Ezt hívjuk a fejelem mintájára végelemnek
* Műveletek:

	```
	Queue::Queue()
		sv = new E1
		se = sv
	```

	```
	Queue::add(x : T)
		sv->key = x
		sv->next = new E1
		sv = sv->next
	```
	
	* Kitöltöm a már talonban levő sv-t, beszúrok mögé egy új sv-t, aminek a mutatója helyesen NULL-ra inicializálódik, mint ahogy egy tisztességes végelemhez illik (a kétirányú fejelemes listáknál is NULL volt a fejelem visszapointere)

	```
	Queue::rem() : T
		HA se == sv
			HIBA
		KÜLÖNBEN
			x = se->key
			p = se
			se = se->next
			delete p
			return x
	```
	
	* A hiba esete az üres lista; itt: csak ha végelem van, azaz az a szituáció, amit a konstruktor hozott össze
	* Különben ez a lista elejéről való törlés minősített esete
* Opcionálisan a láncolt reprezentáció is kiegészíthető egy hossz attribútummal, és a vele visszatérő hossz() függvénnyel
	* Lehet nem végelemes listával is megadni, ekkor se és sv is lehet NULL