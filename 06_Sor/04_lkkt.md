# Legkisebb közös többszörös

* Adott két sor, mindkettőben egy egynél nagyobb természetes szám prímtényezős felbontása növekvő sorrendben (lehetnek azonos elemek). Adjuk meg a legkisebb közös többszöröst hasonló formában. A két sor tartalma változhat a futás alatt
* Például
	* s1 = <2, 2, 5, 7, 7>
	* s2 = <3, 5, 5, 13>
	* lkkt = <2, 2, 3, 5, 5, 7, 7, 13>
	* És amúgy: lnko = <5>
* Ötlet: összefuttatás
	* Két féle alapalakja van, az unió- és a metszet-szerű
* Mi van akkor, ha elvárás, hogy az egyik sor maradjon érintetlen (immutable)?
	* Berakok egy extremális értéket az elején ebbe a sorba, és mindig amikor kiveszek elemet onnan, ugyanazt be is rakom (a végére), az üresség-tesztnél pedig az extremális érték meglétét tesztelem. A végén ezt még kiveszem. Így végülis a bemenet nem lesz konstans, mert a futás közben változik a tartalma, de biztosan ugyanaz lesz a tartalma az elején, mint a végén
* Ez most olyan unió-alapú változat lesz, ahol az inputok elfogynak

	```
	lkktUniosan(s1 : Queue, s2 : Queue) : Queue
	  s3 = Queue()
	  AMÍG !s1.isEmpty() vagy !s2.isEmpty()
	    HA s2.isEmpty() vagy (!s1.isEmpty() és s1.first() < s2.first())
	      s3.add(s1.rem())
	    HA s1.isEmpty() vagy (!s2.isEmpty() és s2.first() < s1.first())
	      s3.add(s2.rem())
	    HA !s1.isEmpty() és !s2.isEmpty() és s1.first() == s2.first()
	      s3.add(s1.rem())
	      s2.rem()
	  return s3
	```
	
	* OOP tárgyból ismert összefuttatás: addig megyek, amíg legalább az egyik nem üres, megnézem melyik "jön" épp a rendezés szerint, azt feldolgozom. Ha mindkettő, akkor nem felejtem el bár csak egyszer feldolgozni, de a másikból is kivenni - ez az unió alapalakja, és az lkkt is egy uniózás végső soron
	    * Az egyes ágak magyarázata: s1 eleme jön a felsorolásban, ha s2 üres. Emellett akkor is, ha egyik sem üres, és s1 első eleme a kisebb. Viszont a rövidzár miatt, ha az s2.isEmpty()-n továbbmentünk, csak azt állíthatjuk bizton, hogy s2 nem üres, de azt nem hogy s1 sem - amit a ciklusfeltétel megengedne. Ezért kell s1 nemüresség-ellenőrzése, és ezért nem kell s2-re ugyanez
	* Az lnko inkább metszetes lenne, ahhoz a következő alak illik jobban, de uniózásra is lehet metszet alakú összefésülést használni, ahogy a lenti példán láthatjuk:

	```
	lkktMetszetesen(s1 : Queue, s2 : Queue) : Queue
		s3 = Queue()
		AMÍG !s1.isEmpty() és !s2.isEmpty()
			HA s1.first() < s2.first()
				s3.add(s1.rem())
			HA s2.first() < s1.first()
				s3.add(s2.rem())
			HA s1.first() == s2.first()
				s3.add(s1.rem())
				s2.rem()
		AMÍG !s1.isEmpty()
			s3.add(s1.rem())
		AMÍG !s2.isEmpty()
			s3.add(s2.rem())
	return s3
	```

	* Itt most a metszetet soroljuk fel. Addig megyünk, míg igaz, hogy még mindkettőben van elem - így egyszerűsödnek a belső feltételek is!
	* Cserébe a végén előfordulhat hogy egyikben vagy másikban van még elem (mindkettő nem lehet), ezt így két egymástól független while-ciklussal oldottam most meg (max. csak az egyik fog belépni)
    * Azért hívom a fentit uniósnak, mert a ciklusfeltételben "vagy" van, azaz addig fut a ciklus, amíg legalább az egyik input nem üres. Ellenben, a "metszetes" alakban "és" van, ott addig fut, amíg egyik sem az (azaz, amíg van esély, hogy a metszetből soroljunk fel egy elemet). Láthatjuk, ha unió jellegű feladatot végzünk metszetes alakkal, ki kell egészítenünk még egy-két ciklussal, ami végigjárja a kimaradt elemeket