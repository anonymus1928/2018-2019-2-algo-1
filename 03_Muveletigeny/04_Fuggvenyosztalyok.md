# Függvények osztályozása műveletigény szerint

## Példák az eddig tanultak alapján

* Megtanultuk, hogy a műveletigény-függvényeket ekvivalenciaosztályokba sorolhatjuk
    * Láttunk szabályokat, amelyek segítségével a besorolást "ránézésre" megoldhatjuk
* Az osztályokat jelölhetjük a legegyszerűbb reprezentánsukkal
    * Pl. azt az osztályt, amibe az n<sup>3</sup> + 3 * n<sup>2</sup> - 420 * n tartozik, jelöljük majd mindig a legegyszerűbb tagjával: Θ(n<sup>3</sup>)-bel
* Soroljuk be az eddig vizsgált függvényeket az osztályokba:
	* Buborékrendezés:
	    * mÖsszehasonlítás<sub>BS</sub>(n) ~ MÖsszehasonlítás<sub>BS</sub>(n) ~ AÖsszehasonlítás<sub>BS</sub>(n) eleme Θ(n<sup>2</sup>)
	    * A cserére csak Ο(n<sup>2</sup>), mivel az mCsere<sub>BS</sub>(n) nagyságrendekkel kisebb, mint a többi
	* Hanoi tornyai:
	    * Átrakás<sub>HT</sub>(n) eleme Θ(2<sup>n</sup>)
	* Polinomszámolósok:
		* Összeadás<sub>Bármelyik</sub>(n) eleme Θ(n)
		* Szorzás:
		    * A naiv megoldásnál Θ(n<sup>2</sup>), ebből következik az Ο és az Ω is
		    * A rekurzív és a Horner-féle megoldásnál Θ(n)

## Tipikus nagyságrendek, nevezetes függvényosztályok

* Az alábbi függvényosztályok csökkenő sorban vannak feltüntetve, azaz ami feljebb van, az mind eleme ω(ami alatta van), és fordítva: ami lejjebb van az mind eleme ο(ami fölötte van)
	* n! - faktoriális műveletigény - utazóügynök probléma (azaz optimális Hamilton-kör keresés - amúgy ezt "optimalizálhatjuk" exponenciálisra)
	* 2<sup>n</sup> - exponenciális műveletigény - Hanoi tornyai
	* n<sup>3</sup> - köbös műveletigény - mátrixszorzás
	* n<sup>2</sup> - négyzetes (kvadratikus) műveletigény - buborékrendezés
	* n*log(n) - kvázilineáris műveletigény - kupacrendezés
	* n - lineáris műveletigény - a programozási tételek
	* gyök(n) - gyökös műveletigény - prímteszt
	* log(n) - logaritmikus műveletigény - logaritmusos (avagy bináris) keresés
	* 1 - konstans műveletigény - veremből kivétel, belerakás, tömbindexelés
* Megjegyzés: kicsi n-re elképzelhető, hogy egy lejjebb lévő nagyobb, mint egy feljebb lévő (gyök vs. négyzet), de kicsi n-re minden nagyon gyors. A nagy n-eknél viszont biztosan a feljebb lévők a nagyobb, ráadásul az olló egyre nagyobb mértékben nyílik
* Megjegyzés: Természetesen végtelen sok nagyságrend van, amiből még néhányat könnyedén elhelyezhetünk, látjuk, hogy az n<sup>2</sup>-nél nagyobb az n<sup>3</sup>, innen nyilván annál nagyobb az n<sup>4</sup>, stb. És ráadásul tört kitevők is lehetnek. Általánosan megállapítható (ezt be is bizonyítottuk), hogy n alappal minél nagyobb a kitevő, annál nagyobb a nagyságrend. De ezek mind a 2<sup>n</sup> alatt lesznek, és mind az n*log(n) fölött, amennyiben a kitevő nagyobb, mint 1. Ha a kitevő 1, akkor visszakapjuk a lineáris esetet, ha pedig a kitevő kisebb, mint 1, akkor a gyök(n) környékén mondható el a fenti: gyök(n) = n<sup>1/2</sup> nyilván nagyobb nagyságrend, mint mondjuk n<sup>1/3</sup>
* Megjegyzés: Az f eleme ω(g) viszonyt (azaz, hogy az f nagyságrendileg nagyobb, mint a g) jelölhetjük így is: f ≻ g (ez egy "hajlított" kacsacsőr-jel, nagyobb betűméretnél látszik rendesen)
